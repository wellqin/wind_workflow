package com.bcx.wind.workflow.test.core.submit;

import com.bcx.wind.workflow.pojo.Wind;
import org.junit.Test;

public class SubmitMoniterTest extends SubmitBase {

    @Test
    public void submitTest(){
        Wind wind = monitor();
        assert wind != null;
        commit();
    }
}
