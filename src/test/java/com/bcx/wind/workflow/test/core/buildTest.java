package com.bcx.wind.workflow.test.core;

import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.pojo.WindUser;
import com.bcx.wind.workflow.pojo.variable.BuildVariable;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.ResourceHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;

/**
 * 创建流程 创建一个Context.xml模型的流程实例，进入第一个edit节点
 *
 * @author zhanglei
 */
public class buildTest extends BaseTest {

    private String processId;

    @Before
    public void before() throws IOException {
        InputStream stream = ResourceHelper.getResourceAsStream("process/Context.xml");
        WindProcess process = engine().repositoryService().deploy(stream,"DMS");
        engine().repositoryService().release(process.getId());
        this.processId = process.getId();
    }

    @Test
    public void buildOne(){
        WindUser user = new WindUser()
                .setUserId("1001")
                .setUserName("李四");
        BuildVariable variable = new BuildVariable()
                .setProcessId(this.processId)
                .setBusinessId(ObjectHelper.primaryKey())
                .setUser(user)
                .setSystem("DMS")
                .setExpireTime(new Timestamp(TimeHelper.getTime("2019-04-29 12:21:31")))
                .setOrderMsg("李四提交的请假审批");
        Wind wind = engine().windCoreService().buildByVariable(variable);
        System.out.println(wind);
        commit();
    }
}
