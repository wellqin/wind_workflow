package com.bcx.wind.workflow.test.support;

import com.bcx.wind.workflow.support.TimeHelper;
import org.junit.Test;

/**
 * 时间处理测试类
 *
 * @author zhanglei
 */
public class TimeHelperTest  {

    private static final String FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static final String DAY_FORMAT = "yyyy-MM-dd";

    @Test
    public void timeHelperTest(){
        String nowTime = TimeHelper.getNowTime();

        String now =  TimeHelper.getNow();

        TimeHelper.getNowTime(FORMAT);

        TimeHelper.getTime(System.currentTimeMillis(),FORMAT);

        assert "2018-02-03 12:21:31".equals(TimeHelper.addLongTime("2018-02-03 12:21:21",1000*10,FORMAT));

        assert TimeHelper.betweenSeconds( "2018-02-03 12:21:31", "2018-02-03 12:21:41",FORMAT) == 10;

        assert  TimeHelper.betweenDays("2018-02-03","2018-02-09",DAY_FORMAT) == 7;

        assert  TimeHelper.getWeek( "2019-03-11 12:21:31",FORMAT) == 1;

        assert TimeHelper.betweenWorkDays("2019-02-03 12:21:31","2019-02-23 12:21:31",FORMAT).size() == 15;

    }
}
