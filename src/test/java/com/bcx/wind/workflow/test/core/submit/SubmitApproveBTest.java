package com.bcx.wind.workflow.test.core.submit;

import com.bcx.wind.workflow.pojo.Wind;
import org.junit.Test;

public class SubmitApproveBTest extends SubmitBase {

    @Test
    public void submitTest(){
        Wind wind = approveB();
        assert wind != null;
        commit();;
    }
}
