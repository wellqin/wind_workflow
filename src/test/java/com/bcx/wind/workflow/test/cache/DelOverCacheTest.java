package com.bcx.wind.workflow.test.cache;

import com.bcx.wind.workflow.cache.Cache;
import com.bcx.wind.workflow.cache.WindCache;
import com.bcx.wind.workflow.cache.imp.DelOverCache;
import org.junit.Test;

/**
 * 控制大小，删除最不常用数据测试
 *
 * @author zhanglei
 */
public class DelOverCacheTest {

    @Test
    public void delOCacheTest(){
        Cache cache = new DelOverCache(new WindCache(),4);
        cache.put("1001","james");
        cache.put("1002","tom");
        cache.put("1003","hir");
        cache.put("1004","jack");

        //使用了3个数据
        cache.get("1001");
        cache.get("1002");
        cache.get("1003");

        cache.put("1005","rose");
        //由于1004数据没有用，那么1004数据应该被清除

        assert cache.get("1004") == null;
    }
}
