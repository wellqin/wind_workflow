package com.bcx.wind.workflow.test.core.submit;

import com.bcx.wind.workflow.pojo.Wind;
import org.junit.Test;

public class SubmitManagerTest extends SubmitBase {

    @Test
    public void submitTest(){
        Wind wind = manager();
        assert wind!=null;
        commit();
    }
}
