package com.bcx.wind.workflow.test.access;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.core.constant.TaskLevel;
import com.bcx.wind.workflow.entity.WindActHistory;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

/**
 * 执行历史履历测试
 *
 * @author zhanglei
 */
public class ActHistoryTest extends BaseTest {

    private WindActHistory insert(){
        WindActHistory history = new WindActHistory()
                .setId(ObjectHelper.primaryKey())
                .setTaskId(ObjectHelper.primaryKey())
                .setTaskName("approve")
                .setTaskDisplayName("审核")
                .setProcessId(ObjectHelper.primaryKey())
                .setOrderId(ObjectHelper.primaryKey())
                .setProcessName("holiday_apply")
                .setProcessDisplayName("请假申请")
                .setOperate("提交")
                .setSuggest("同意审核")
                .setApproveTime(TimeHelper.nowDate())
                .setActorId("10002")
                .setActorName("李四")
                .setCreateTime(TimeHelper.nowDate())
                .setApproveUserVariable("[]")
                .setTaskType("all")
                .setSystem("DMS")
                .setSubmitUserVariable("{}")
                .setTaskLevel(TaskLevel.main.name());
        int ret = access().insertActiveHistory(history);
        assert ret == 1;
        return history;
    }


    @Test
    public void insertTest(){
       insert();
       commit();
    }

    @Test
    public void updateTest(){
        WindActHistory history =  insert();
        history.setProcessName("abcd");
        int ret = access().updateActiveHistory(history);
        assert ret == 1;
        commit();
    }

    @Test
    public void queryOne(){
        WindActHistory history = insert();
        history = access().getActiveHistoryById(history.getId());
        assert history != null;
        commit();
    }

    @Test
    public void query(){
        QueryFilter filter = new QueryFilter()
                .setProcessName("holiday_apply")
                .setTaskActorId(new String[]{"admin","10001","10002"})
                .setCreateTimeStart(new Timestamp(TimeHelper.getTime("2019-03-13 13:31:17")));

        List<WindActHistory> histories = access().selectActiveHistoryList(filter);
        System.out.println(histories.size());
        commit();
    }

    @Test
    public void queryPage(){
        WindPage<WindActHistory> page = new WindPage<>();
        page.setPageSize(3);

        QueryFilter filter = new QueryFilter()
                .setProcessName("holiday_apply")
                .setTaskActorId(new String[]{"admin","10001","10002"})
                .setCreateTimeStart(new Timestamp(TimeHelper.getTime("2019-03-13 13:31:17")));

        List<WindActHistory> result = access().selectActiveHistoryList(filter,page);
        System.out.println(result.size());
        System.out.println(page.getCount());
        commit();
    }

    @Test
    public void deleteOne(){
        WindActHistory actHistory = insert();

        int ret = access().removeActiveHistoryById(actHistory.getId());
        assert  ret == 1;
        commit();
    }

    @Test
    public void deleteList(){
        WindActHistory actHistory = insert();
        WindActHistory actHistory1 = insert();

        List<String> ids = new LinkedList<>();
        ids.add(actHistory.getId());
        ids.add(actHistory1.getId());

        int ret = access().removeActiveHistoryByIds(ids);
        assert ret == 2;
        commit();
    }
}
