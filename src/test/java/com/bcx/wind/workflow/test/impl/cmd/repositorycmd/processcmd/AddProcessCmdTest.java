package com.bcx.wind.workflow.test.impl.cmd.repositorycmd.processcmd;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.support.ResourceHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 添加流程定义测试
 *
 * @author zhanglei
 */
public class AddProcessCmdTest extends BaseTest {

    protected WindProcess  addProcess(){
        InputStream stream = null;
        try {
            stream = ResourceHelper.getResourceAsStream("process/Context.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        WindProcess process = engine().repositoryService().deploy(stream,"DMS");
        assert process != null;
        return process;
    }


    @Test
    public void  addProcessTest() throws IOException {
        WindProcess process = addProcess();
        System.out.println(process);
        commit();
    }

    @Test
    public void queryListTest(){
        QueryFilter filter = new QueryFilter()
                .setSystem("DMS");
        List<WindProcess> processes =  engine().repositoryService().queryProcess(filter);
        System.out.println(processes.size());
        commit();
    }

    @Test
    public void queryListPageTest(){
        QueryFilter filter = new QueryFilter()
                .setSystem("DMS");
        WindPage<WindProcess> page = new WindPage<>();
        page.setPageSize(2);
        List<WindProcess> processes =  engine().repositoryService().queryProcessPage(filter,page);
        System.out.println(page.getCount());
        System.out.println(processes.size());
        commit();
    }

    @Test
    public void  removeProcessTest(){
        WindProcess process = addProcess();
        engine().repositoryService().recovery(process.getId());
        int ret = engine().repositoryService().removeByProcessId(process.getId());
        assert ret == 1;
        commit();
    }

    @Test
    public void releaseProcessTest(){
        WindProcess process = addProcess();
        int ret = engine().repositoryService().release(process.getId());

        assert ret == 1;
        commit();
    }

    @Test
    public void recoveryProcessTest(){
        WindProcess process = addProcess();
        int ret = engine().repositoryService().recovery(process.getId());
        assert ret == 1;
        commit();
    }

    @Test
    public void queryProcessId(){
        WindProcess process = addProcess();
        engine().repositoryService().release(process.getId());
        WindProcess windProcess = engine().repositoryService().queryById(process.getId());
        assert windProcess != null;
    }

    @Test
    public void queryMaxVersionTest(){
        WindProcess process = addProcess();
        engine().repositoryService().release(process.getId());
        WindProcess windProcess = engine().repositoryService().queryMaxVersion(process.getProcessName());
        assert windProcess != null;
    }
}
