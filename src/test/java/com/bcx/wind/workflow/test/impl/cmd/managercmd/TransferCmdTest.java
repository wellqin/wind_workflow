package com.bcx.wind.workflow.test.impl.cmd.managercmd;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.entity.WindTaskActor;
import com.bcx.wind.workflow.pojo.Task;
import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.pojo.WindUser;
import com.bcx.wind.workflow.test.core.submit.SubmitBase;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

public class TransferCmdTest  extends SubmitBase {

    @Test
    public void transferByTaskId(){
        //先执行提交
        Wind wind = monitor();
        assert  wind != null;

        //获取当前任务
        Task curTask = wind.getCurNode().get(0);

        //获取新任务 即班长任务
        List<Task> newTasks  = curTask.getSubmitTasks();

        //查询其中一个任务的审批人
        Task newTask = newTasks.get(0);
        String taskId = newTask.getWindTask().getId();
        QueryFilter filter = new QueryFilter()
                .setTaskId(taskId);
        List<WindTaskActor> windTaskActors = engine().getConfiguration().getAccess().selectTaskActorList(filter);

        //审批人
        String oldActor = windTaskActors.get(0).getActorId();


        //执行转办
        WindUser user = new WindUser()
                .setUserId("123456")
                .setUserName("东方不败");
        List<WindTask> result = this.engine().managerService().transferByTaskId(Collections.singletonList(taskId),oldActor,user,null);
        System.out.println(result.size());
        commit();
    }
}
