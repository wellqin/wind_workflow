package com.bcx.wind.workflow.test.core.submit;

import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.pojo.ApproveUser;
import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.pojo.WindUser;
import com.bcx.wind.workflow.pojo.variable.BuildVariable;
import com.bcx.wind.workflow.pojo.variable.SubmitVariable;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.ResourceHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Before;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SubmitBase  extends BaseTest {

    protected String orderId;

    @Before
    public void buildFlow() throws IOException {
        InputStream stream = ResourceHelper.getResourceAsStream("process/Context.xml");
        WindProcess process = engine().repositoryService().deploy(stream,"DMS");
        engine().repositoryService().release(process.getId());
        String processId = process.getId();
        WindUser user = new WindUser()
                .setUserId("1001")
                .setUserName("李四");
        BuildVariable variable = new BuildVariable()
                .setProcessId(processId)
                .setBusinessId(ObjectHelper.primaryKey())
                .setUser(user)
                .setSystem("DMS")
                .setExpireTime(new Timestamp(TimeHelper.getTime("2019-04-29 12:21:31")))
                .setOrderMsg("李四提交的请假审批");
        Wind wind = engine().windCoreService().buildByVariable(variable);
        this.orderId = wind.getWindOrder().getId();
        System.out.println("build success orderId: "+orderId);
        System.out.println(wind);
    }


    /**
     * 第一步
     * @return
     */
    private List<ApproveUser> routerEditApproveUsers(){
        List<ApproveUser> users =new LinkedList<>();
        ApproveUser user = new ApproveUser();
        List<WindUser> approveUsers = new LinkedList<>();
        approveUsers.add(new WindUser().setUserId("2001").setUserName("李四"));
        approveUsers.add(new WindUser().setUserId("2002").setUserName("王五"));
        user.setNodeId("routerEdit")
                .setExpireTime(new Timestamp(TimeHelper.getTime("2019-05-01 12:32:21")))
                .setUsers(approveUsers);
        users.add(user);
        return users;
    }


    protected Wind routerEdit(){
        //审批人
        List<ApproveUser> users = routerEditApproveUsers();
        //提交参数
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setUser(new WindUser().setUserId("10001").setUserName("张三"))
                .setApproveUsers(users)
                .setSystem("dms")
                .setSuggest("提交起草文件！");

        Wind wind = engine().windCoreService().submit(variable);
        System.out.println(wind.getWindOrder().getId());
        return wind;
    }


    /**
     * 第二步
     * @return
     */
    private List<ApproveUser>  routerApproveApproveUser(){
        List<ApproveUser> users =new LinkedList<>();
        ApproveUser user = new ApproveUser();
        List<WindUser> approveUsers = new LinkedList<>();
        approveUsers.add(new WindUser().setUserId("2003").setUserName("刘备"));
        user.setNodeId("routerApprove")
                .setExpireTime(new Timestamp(TimeHelper.getTime("2019-05-01 12:32:21")))
                .setUsers(approveUsers);
        users.add(user);
        return users;
    }

    protected Wind routerApprove(){
        routerEdit();
        List<ApproveUser> approveUsers = routerApproveApproveUser();
        //提交参数
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setUser(new WindUser().setUserId("2002").setUserName("王五"))
                .setApproveUsers(approveUsers)
                .setSystem("dms")
                .setSuggest("子节点起草提交！");

        Wind wind = engine().windCoreService().submit(variable);
        System.out.println(wind.getWindOrder().getId());
        return wind;
    }




    /**
     * 第三步
     * @return
     */
    private List<ApproveUser>  monitorApproveUser(){
        List<ApproveUser> users =new LinkedList<>();
        ApproveUser user = new ApproveUser();
        List<WindUser> approveUsers = new LinkedList<>();
        approveUsers.add(new WindUser().setUserId("2004").setUserName("张无忌"));
        approveUsers.add(new WindUser().setUserId("2005").setUserName("赵敏"));
        approveUsers.add(new WindUser().setUserId("2006").setUserName("周芷若"));
        user.setNodeId("monitor")
                .setExpireTime(new Timestamp(TimeHelper.getTime("2019-05-01 12:32:21")))
                .setUsers(approveUsers);

        ApproveUser user1 = new ApproveUser();
        List<WindUser> approveUsers1 = new LinkedList<>();
        approveUsers1.add(new WindUser().setUserId("2007").setUserName("杨逍"));
        user1.setNodeId("scribeTask")
                .setExpireTime(new Timestamp(TimeHelper.getTime("2019-05-01 12:32:21")))
                .setUsers(approveUsers1);

        users.add(user);
        users.add(user1);
        return users;
    }


    protected Wind monitor(){
        routerApprove();
        List<ApproveUser> approveUsers = monitorApproveUser();
        //提交参数
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setUser(new WindUser().setUserId("2003").setUserName("刘备"))
                .setApproveUsers(approveUsers)
                .setSystem("dms")
                .setSuggest("子节点审核提交！");

        Wind wind = engine().windCoreService().submit(variable);
        System.out.println(wind.getWindOrder().getId());
        return wind;
    }


    /**
     * 第三步
     * @return
     */
    private List<ApproveUser>  forkApproveUser(){
        List<ApproveUser> users =new LinkedList<>();
        ApproveUser user = new ApproveUser();
        List<WindUser> approveUsers = new LinkedList<>();
        approveUsers.add(new WindUser().setUserId("2008").setUserName("萧峰"));
        user.setNodeId("approveA")
                .setExpireTime(new Timestamp(TimeHelper.getTime("2019-05-01 12:32:21")))
                .setUsers(approveUsers);

        ApproveUser user1 = new ApproveUser();
        List<WindUser> approveUsers1 = new LinkedList<>();
        approveUsers1.add(new WindUser().setUserId("2009").setUserName("段誉"));
        user1.setNodeId("approveC")
                .setExpireTime(new Timestamp(TimeHelper.getTime("2019-05-01 12:32:21")))
                .setUsers(approveUsers1);

        users.add(user);
        users.add(user1);
        return users;
    }

    protected Wind approveAapproveC(){
        monitor();
        fork("2004","张无忌");
        fork("2005","赵敏");
        Wind wind = fork("2006","周芷若");
        return wind;
    }

    protected Wind approveA(){
        monitor();
        return fork("2004","张无忌");
    }


    private Wind fork(String userId,String name){
        List<ApproveUser> approveUsers = forkApproveUser();
        //提交参数
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setUser(new WindUser().setUserId(userId).setUserName(name))
                .setApproveUsers(approveUsers)
                .setSystem("dms")
                .setSuggest(name+"班长提交！");

        Wind wind = engine().windCoreService().submit(variable);
        System.out.println(wind.getWindOrder().getId());
        return wind;
    }



    /**
     * 第四步
     * @return
     */
    private List<ApproveUser>  approveBapproveUser(){
        List<ApproveUser> users =new LinkedList<>();
        ApproveUser user = new ApproveUser();
        List<WindUser> approveUsers = new LinkedList<>();
        approveUsers.add(new WindUser().setUserId("2010").setUserName("杨过"));
        user.setNodeId("approveB")
                .setExpireTime(new Timestamp(TimeHelper.getTime("2019-05-01 12:32:21")))
                .setUsers(approveUsers);
        users.add(user);
        return users;
    }


    protected Wind approveB(){
        approveAapproveC();
        List<ApproveUser> approveUsers = approveBapproveUser();
        //提交参数
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setUser(new WindUser().setUserId("2008").setUserName("萧峰"))
                .setApproveUsers(approveUsers)
                .setSystem("dms")
                .setSuggest("approveA提交！");

        Wind wind = engine().windCoreService().submit(variable);
        System.out.println(wind.getWindOrder().getId());
        return wind;
    }



    /**
     * 第五步
     * @return
     */
    protected List<ApproveUser>  miniterApproveUser(){
        List<ApproveUser> users =new LinkedList<>();
        ApproveUser user = new ApproveUser();
        List<WindUser> approveUsers = new LinkedList<>();
        approveUsers.add(new WindUser().setUserId("2011").setUserName("小龙女"));
        approveUsers.add(new WindUser().setUserId("2012").setUserName("郭靖"));
        user.setNodeId("minister")
                .setExpireTime(new Timestamp(TimeHelper.getTime("2019-05-01 12:32:21")))
                .setUsers(approveUsers);
        users.add(user);
        return users;
    }


    protected Wind miniter1(){
        approveB();
        List<ApproveUser> approveUsers = miniterApproveUser();
        //提交参数
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setUser(new WindUser().setUserId("2010").setUserName("杨过"))
                .setApproveUsers(approveUsers)
                .setSystem("dms")
                .setSuggest("approveB提交！");

        Wind wind = engine().windCoreService().submit(variable);
        System.out.println(wind.getWindOrder().getId());
        return wind;
    }


    /**
     * 第六步
     * @return
     */
    protected Wind miniter2(){
        miniter1();
        List<ApproveUser> approveUsers = miniterApproveUser();
        //提交参数
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setUser(new WindUser().setUserId("2009").setUserName("段誉"))
                .setApproveUsers(approveUsers)
                .setSystem("dms")
                .setSuggest("approveC提交！");

        Wind wind = engine().windCoreService().submit(variable);
        System.out.println(wind.getWindOrder().getId());
        return wind;
    }



    /**
     * 第五步
     * @return
     */
    private List<ApproveUser>  managerApproveUser(){
        List<ApproveUser> users =new LinkedList<>();
        ApproveUser user = new ApproveUser();
        List<WindUser> approveUsers = new LinkedList<>();
        approveUsers.add(new WindUser().setUserId("2013").setUserName("段正淳"));
        approveUsers.add(new WindUser().setUserId("2014").setUserName("李寻欢"));
        user.setNodeId("manager")
                .setExpireTime(new Timestamp(TimeHelper.getTime("2019-05-01 12:32:21")))
                .setUsers(approveUsers);
        users.add(user);
        return users;
    }


    protected Wind manager(){
        miniter2();
        managerSubmit("2011","小龙女");
        return managerSubmit("2012","郭靖");
    }


    protected Wind managerSubmit(String userId,String userName){
        List<ApproveUser> approveUsers = managerApproveUser();
        //提交参数
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setUser(new WindUser().setUserId(userId).setUserName(userName))
                .setApproveUsers(approveUsers)
                .setSubmitPath(Collections.singletonMap("minister",Collections.singletonList("disc_path2")))
                .setSystem("dms")
                .setSuggest("部长提交！");

        Wind wind = engine().windCoreService().submit(variable);
        System.out.println(wind.getWindOrder().getId());
        return wind;
    }



    protected Wind endSubmit(){
        manager();
        //提交参数
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setUser(new WindUser().setUserId("2013").setUserName("段正淳"))
                .setSystem("dms")
                .setSuggest("总总理提交！");

        Wind wind = engine().windCoreService().submit(variable);
        System.out.println(wind.getWindOrder().getId());
        return wind;
    }


    protected Wind end1Submit(){
        endSubmit();
        //提交参数
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setUser(new WindUser().setUserId("2014").setUserName("李寻欢"))
                .setSystem("dms")
                .setSuggest("总总理1提交！");

        Wind wind = engine().windCoreService().submit(variable);
        System.out.println(wind.getWindOrder().getId());
        return wind;
    }

}
