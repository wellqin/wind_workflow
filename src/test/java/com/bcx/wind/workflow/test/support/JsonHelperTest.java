package com.bcx.wind.workflow.test.support;

import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.support.JsonHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import org.junit.Test;

import java.util.Map;

/**
 * json处理测试类
 *
 * @author zhanglei
 */
public class JsonHelperTest  {

    @Test
    public void jsonHelperTest(){
        WindTask task = new WindTask()
                .setId("10001")
                .setTaskName("approveUser")
                .setTaskType("ordinary")
                .setCreateTime(TimeHelper.nowDate())
                .setApproveCount(2);

        String json = JsonHelper.toJson(task);
        Map<String,Object> jsonObj = JsonHelper.jsonToMap(json);
        WindTask windTask = JsonHelper.coverObject(jsonObj,WindTask.class);
        jsonObj = JsonHelper.objectToMap(windTask);
        windTask = JsonHelper.mapToObject(jsonObj,WindTask.class);
        json = JsonHelper.toJson(windTask);
        windTask = JsonHelper.parseJson(json,WindTask.class);

        assert windTask.getId().equals("10001");

        String value = "process_display_name";
        assert JsonHelper.toHump(value).equals("processDisplayName");
        String value1 = "processDisplayName";
        assert JsonHelper.toUnderLineField(value1).equals("process_display_name");
    }
}
