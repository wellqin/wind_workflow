package com.bcx.wind.workflow.test.impl.cmd.runtimecmd;

import com.bcx.wind.workflow.core.constant.OrderStatus;
import com.bcx.wind.workflow.core.constant.TaskLevel;
import com.bcx.wind.workflow.core.constant.TaskStatus;
import com.bcx.wind.workflow.core.constant.TaskType;
import com.bcx.wind.workflow.entity.WindActHistory;
import com.bcx.wind.workflow.entity.WindHistOrder;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.pojo.WindUser;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import static com.bcx.wind.workflow.core.constant.Constant.ARRAY_NULL;
import static com.bcx.wind.workflow.core.constant.Constant.JSON_NULL;

/**
 * 运行时服务测试
 *
 * @author zhanglei
 */
public class RuntimeServiceTest extends BaseTest {

    private WindTask createTask(){
        WindTask task = new WindTask()
                .setTaskName("edit")
                .setDisplayName("编辑")
                .setTaskType(TaskType.ordinary.name())
                .setExpireTime(new Timestamp(TimeHelper.getTime("2022-01-02 12:12:34")))
                .setStatus(TaskStatus.run.name())
                .setOrderId(ObjectHelper.primaryKey())
                .setProcessId(ObjectHelper.primaryKey())
                .setPosition("/location/demo")
                .setTaskLevel(TaskLevel.main.name());
        return engine().runtimeService().createNewTask(task);
    }

    @Test
    public void createTaskTest(){
        createTask();
        commit();
    }

    @Test
    public void updateTaskTest(){
        WindTask task = createTask();
        task.setPosition("/abc/更新");
        engine().runtimeService().updateTask(task);
        commit();
    }

    @Test
    public void addTaskVariableTest(){
        WindTask task = createTask();
        engine().runtimeService().addTaskVariable(task.getId(),"approveUser","1001");
        commit();
    }

    @Test
    public void removeTaskTest(){
        WindTask task = createTask();
        engine().runtimeService().removeTask(task.getId());
        commit();
    }

    @Test
    public void removeTaskByOrderId(){
        WindTask task = createTask();
        engine().runtimeService().removeTaskByOrderId(task.getOrderId());
        commit();
    }

    @Test
    public void addTaskActor(){
        WindTask task = createTask();
        List<String> users = new LinkedList<>();
        users.add("10001");
        users.add("10002");
        engine().runtimeService().addTaskActor(task.getId(),users);
        commit();
    }

    @Test
    public void removeTaskActor(){
        WindTask task = createTask();
        List<String> users = new LinkedList<>();
        users.add("10001");
        users.add("10002");
        engine().runtimeService().addTaskActor(task.getId(),users);
        engine().runtimeService().removeTaskActor(task.getId(),"10001");
        commit();

    }

    private WindOrder createOrder(){
        WindOrder order = new WindOrder()
                .setProcessId(ObjectHelper.primaryKey())
                .setStatus(OrderStatus.run.name())
                .setCreateUser("10001")
                .setBusinessId(ObjectHelper.primaryKey())
                .setExpireTime(new Timestamp(TimeHelper.getTime("2022-01-02 12:12:34")));
        return engine().runtimeService().createOrder(order);
    }

    @Test
    public void createOrderTest(){
        createOrder();
        commit();
    }

    @Test
    public void updateOrderTest(){
        WindOrder order = createOrder();
        order.setFinishTime(TimeHelper.nowDate());
        engine().runtimeService().updateOrder(order);
        commit();
    }

    @Test
    public void addOrderVariableTest(){
        WindOrder order = createOrder();
        engine().runtimeService().addOrderVariable(order.getId(),
                "userId","1001");
        commit();
    }



    @Test
    public void removeOrderTest(){
        WindOrder order = createOrder();
        engine.runtimeService().removeOrder(order.getId());
        commit();
    }

    @Test
    public void createHistOrderTest(){
        WindHistOrder order = new WindHistOrder()
                .setId(ObjectHelper.primaryKey())
                .setProcessId(ObjectHelper.primaryKey())
                .setStatus(OrderStatus.complete.name())
                .setCreateUser("1001")
                .setCreateTime(TimeHelper.nowDate())
                .setExpireTime(TimeHelper.nowDate())
                .setVersion(3)
                .setVariable(JSON_NULL)
                .setData("测试内容")
                .setBusinessId(ObjectHelper.primaryKey())
                .setSystem("DMS")
                .setFinishTime(TimeHelper.nowDate());
        engine().runtimeService().createHistOrder(order);
        commit();
    }

    private WindActHistory actHistory(){
        return new WindActHistory()
                .setId(ObjectHelper.primaryKey())
                .setTaskId(ObjectHelper.primaryKey())
                .setTaskName("approve")
                .setTaskDisplayName("审核")
                .setProcessId(ObjectHelper.primaryKey())
                .setOrderId(ObjectHelper.primaryKey())
                .setProcessName("holiday")
                .setProcessDisplayName("请假流程")
                .setCreateTime(TimeHelper.nowDate())
                .setApproveUserVariable(ARRAY_NULL)
                .setTaskType("普通");
    }

    @Test
    public void addActHistoryTest(){
        WindActHistory history = actHistory();
        engine().runtimeService().addActHistory(history);
        commit();
    }

    @Test
    public void updateActHistoryTest(){
        WindActHistory actHistory = actHistory();
        engine().runtimeService().addActHistory(actHistory);
        actHistory.setOperate("提交")
                .setSuggest("同意")
                .setApproveTime(TimeHelper.nowDate())
                .setActorId("1002")
                .setActorName("李四")
                .setSystem("DMS")
                .setSubmitUserVariable(JSON_NULL);
        engine().runtimeService().updateActHistory(actHistory);
        commit();
    }

    @Test
    public void removeActHistoryTest(){
        WindActHistory actHistory = actHistory();
        engine().runtimeService().removeActHistory(actHistory.getOrderId());
        commit();
    }

    @Test
    public void addEndHistoryTest(){
        WindActHistory actHistory = actHistory();
        actHistory.setOperate("提交")
                .setSuggest("同意")
                .setApproveTime(TimeHelper.nowDate())
                .setActorId("1002")
                .setActorName("李四")
                .setSystem("DMS")
                .setSubmitUserVariable(JSON_NULL);
        List<WindActHistory> actHistories = new LinkedList<>();
        actHistories.add(actHistory);
        engine().runtimeService().addEndHistory(actHistories);
        commit();
    }


}
