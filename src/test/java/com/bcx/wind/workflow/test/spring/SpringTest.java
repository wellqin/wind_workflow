package com.bcx.wind.workflow.test.spring;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Spring 结合测试
 *
 * @author zhanglei
 */
public class SpringTest {

    @Test
    public void springTest(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringBean.class);
        SpringService springService = context.getBean(SpringService.class);
        springService.add();
        System.out.println();
    }
}
