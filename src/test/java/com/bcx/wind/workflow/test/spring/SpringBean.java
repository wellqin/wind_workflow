package com.bcx.wind.workflow.test.spring;

import com.bcx.wind.workflow.WindEngine;
import com.bcx.wind.workflow.core.constant.WindDB;
import com.bcx.wind.workflow.db.datasource.WindDataSource;
import com.bcx.wind.workflow.imp.WindEngineBuilder;
import com.bcx.wind.workflow.imp.service.*;
import org.junit.Before;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

import static com.bcx.wind.workflow.test.BaseTest.*;

@ComponentScan("com.bcx.wind.workflow.test.spring")
@EnableTransactionManagement
public class SpringBean {

    @Bean
    public DataSource dataSource(){
         return   new WindDataSource(DB_URL,
                 DRIVER_CLASS,
                 USER_NAME,
                 PASSWORD)
                 .setMinSize(1);
    }

    @Bean
    public PlatformTransactionManager platformTransactionManager(DataSource dataSource){
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    public WindEngine windEngine(DataSource dataSource){
        return WindEngineBuilder.getInstance().buildDataSource(dataSource)
                .buildDbType(WindDB.SPRING)
                .buildEngine();
    }

    @Bean
    public RuntimeService runtimeService(WindEngine engine){
        return engine.runtimeService();
    }

    @Bean
    public RepositoryService repositoryService(WindEngine engine){
        return engine.repositoryService();
    }

    @Bean
    public ManagerService managerService(WindEngine engine){
        return engine.managerService();
    }

    @Bean
    public HistoryService historyService(WindEngine engine){
        return engine.historyService();
    }

    @Bean
    public WindCoreService windCoreService(WindEngine engine){
        return engine.windCoreService();
    }


}
