package com.bcx.wind.workflow.test.impl.cmd.managercmd;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.entity.WindTaskActor;
import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.test.core.submit.SubmitBase;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

public class ReduceApproveUserTest  extends SubmitBase {

    @Test
    public void  reduceTest(){
        Wind wind = approveA();

        assert  wind != null;

        String orderId = wind.getWindOrder().getId();
        QueryFilter f = new QueryFilter()
                .setOrderId(orderId);
        List<WindTask> list = this.engine().getConfiguration().getAccess().selectTaskInstanceList(f);
        String taskId = list.get(0).getId();

        QueryFilter filter = new QueryFilter()
                .setTaskId(taskId);
        List<WindTaskActor> windTaskActors = engine().getConfiguration().getAccess().selectTaskActorList(filter);

        //审批人
        String oldActor = windTaskActors.get(0).getActorId();

        //执行去除
        WindTask windTask = engine().managerService().reduceApproveUser(taskId,Collections.singletonList(oldActor),null);
        assert windTask != null;
        commit();
    }
}
