package com.bcx.wind.workflow.test.access;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindHistOrder;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.util.List;

/**
 * 历史实例测试
 *
 * @author zhanglei
 */
public class HistoryOrderTest extends BaseTest {


    private WindHistOrder insert(){
        WindHistOrder order = new WindHistOrder()
                .setId(ObjectHelper.primaryKey())
                .setProcessId(ObjectHelper.primaryKey())
                .setStatus("run")
                .setCreateUser("10001")
                .setCreateTime(TimeHelper.nowDate())
                .setExpireTime(TimeHelper.nowDate())
                .setParentId(ObjectHelper.primaryKey())
                .setVersion(1)
                .setVariable("{}")
                .setData("请假相关流程呢")
                .setSystem("DMS")
                .setFinishTime(null)
                .setBusinessId(ObjectHelper.primaryKey());
        int ret = access().insertWindHistOrder(order);
        assert ret == 1;
        return order;
    }

    @Test
    public void insertTest(){
        insert();
        commit();;
    }

    @Test
    public void updateTest(){
        WindHistOrder order = insert();
        order.setFinishTime(TimeHelper.nowDate());
        int ret = access().updateWindHistOrder(order);
        assert ret == 1;
        commit();
    }

    @Test
    public void deleteOne(){
        WindHistOrder order = insert();
        int ret = access().removeWindHistOrderById(order.getId());
        assert ret == 1;
        commit();
    }

    @Test
    public void queryOne(){
        WindHistOrder order = insert();
        order = access().getWindHistOrderById(order.getId());
        assert order != null;
        commit();
    }

    @Test
    public void query(){
        QueryFilter filter = new QueryFilter()
                .setSystem("DMS")
                .setOverTime(true);
        List<WindHistOrder> histOrders =  access().selectWindHistOrderList(filter);
        System.out.println(histOrders.size());
    }

    @Test
    public void queryPage(){
        QueryFilter filter = new QueryFilter()
                .setSystem("DMS")
                .setOverTime(true);

        WindPage<WindHistOrder>  page = new WindPage<>();
        page.setPageSize(4);
        List<WindHistOrder> orders = access().selectWindHistOrderList(filter,page);
        System.out.println(orders.size());
        System.out.println(page.getCount());
    }
}
