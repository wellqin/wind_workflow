package com.bcx.wind.workflow.test.core.submit;

import com.bcx.wind.workflow.pojo.Wind;
import org.junit.Test;

public class SubmitEnd1Test extends SubmitBase {

    @Test
    public void submitTest(){
        Wind wind = end1Submit();
        assert wind != null;
        commit();
    }
}
