package com.bcx.wind.workflow.test.impl.cmd.managercmd;

import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.test.core.submit.SubmitBase;
import org.junit.Test;

public class HangupCmdTest extends SubmitBase {

    @Test
    public void  hangupCmdTest(){
        Wind wind = monitor();
        assert  wind != null;

        String orderId = wind.getWindOrder().getId();
        int ret = this.engine().managerService().hangUpOrderByOrderId(orderId);

        //恢复
        this.engine().managerService().recoveryOrderByOrderId(orderId);
        assert  ret != 0;
        commit();
    }

}
