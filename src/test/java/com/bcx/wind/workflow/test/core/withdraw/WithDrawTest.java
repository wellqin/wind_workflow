package com.bcx.wind.workflow.test.core.withdraw;

import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.pojo.variable.WithDrawVariable;
import com.bcx.wind.workflow.test.core.submit.SubmitBase;
import org.junit.Test;

public class WithDrawTest extends SubmitBase {

    @Test
    public void withDrawTest(){
        routerApprove();

        WithDrawVariable variable = new WithDrawVariable()
                .setOrderId(orderId);

        Wind wind = engine.windCoreService().withDraw(variable);
        assert wind != null;
        commit();
    }
}
