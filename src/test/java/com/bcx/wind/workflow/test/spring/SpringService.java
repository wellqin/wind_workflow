package com.bcx.wind.workflow.test.spring;

import com.bcx.wind.workflow.WindEngine;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.imp.service.HistoryService;
import com.bcx.wind.workflow.imp.service.ManagerService;
import com.bcx.wind.workflow.imp.service.RepositoryService;
import com.bcx.wind.workflow.imp.service.RuntimeService;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.ResourceHelper;
import com.bcx.wind.workflow.support.StreamHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.net.ResponseCache;

/**
 * Spring结合测试
 *
 * @author zhanglei
 */

@Component
@Transactional
public class SpringService {

    @Resource
    private WindEngine engine;

    @Resource
    private RuntimeService runtimeService;

    @Resource
    private HistoryService historyService;

    @Resource
    private RepositoryService repositoryService;

    @Resource
    private ManagerService managerService;

    public  void add(){
//        InputStream stream = null;
//        try {
//            stream = ResourceHelper.getResourceAsStream("process/Context.dtd");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        WindProcess process = new WindProcess()
//                .setId(ObjectHelper.primaryKey())
//                .setProcessName("template")
//                .setDisplayName("模板提交")
//                .setStatus("0")
//                .setCreateTime(TimeHelper.nowDate())
//                .setContent(StreamHelper.getByte(stream))
//                .setParentId(ObjectHelper.primaryKey())
//                .setSystem("DMS")
//                .setVersion(1);
//
//        int result = engine.getConfiguration().getAccess().addProcess(process);
//        System.out.println(result);

        WindOrder order = new WindOrder()
                .setId(ObjectHelper.primaryKey())
                .setProcessId(ObjectHelper.primaryKey())
                .setStatus("run")
                .setCreateUser("10001")
                .setCreateTime(TimeHelper.nowDate())
                .setExpireTime(TimeHelper.nowDate())
                .setParentId(ObjectHelper.primaryKey())
                .setVersion(1)
                .setVariable("{}")
                .setData("请假相关流程呢")
                .setSystem("DMS")
                .setFinishTime(null)
                .setBusinessId(ObjectHelper.primaryKey());
        int ret = engine.getConfiguration().getAccess().insertOrderInstance(order);
        assert ret == 1;
    }
}
