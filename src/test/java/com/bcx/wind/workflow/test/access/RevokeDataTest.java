package com.bcx.wind.workflow.test.access;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.entity.WindRevokeData;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.util.List;

/**
 * 撤销暂存测试
 *
 * @author zhanglei
 */
public class RevokeDataTest extends BaseTest {

    private WindRevokeData insert(){
        WindRevokeData revokeData = new WindRevokeData()
                .setId(ObjectHelper.primaryKey())
                .setOrderId(ObjectHelper.primaryKey())
                .setProcessId(ObjectHelper.primaryKey())
                .setProcessDisplayName("测试模型")
                .setTaskName("edit")
                .setTaskDisplayName("编辑节点")
                .setActorId("10001")
                .setOperate("提交")
                .setSuggest("测试建议")
                .setOperateTime(TimeHelper.nowDate())
                .setWindOrder("[]")
                .setWindTask("[]")
                .setWindTaskActor("[]")
                .setCreateTime(TimeHelper.nowDate());

        int ret = access().insertWindRevokeData(revokeData);
        assert ret == 1;
        return revokeData;
    }

    @Test
    public void insertTest(){
        insert();
        commit();
    }

    @Test
    public void removeRevoke(){
        WindRevokeData revokeData = insert();
        int ret = access().deleteWindRevokeDataByOrderId(revokeData.getOrderId());
        assert ret == 1;
        commit();;
    }

    @Test
    public void queryByOrderIdOrActorId(){
        WindRevokeData revokeData = insert();
        List<WindRevokeData> revokeDatas = access().selectRevokeByActor(revokeData.getOrderId(),revokeData.getActorId());
        System.out.println(revokeDatas.size());
    }

    @Test
    public void query(){
        QueryFilter filter = new QueryFilter()
                .setTaskName("edit");

        List<WindRevokeData> datas = access().selectRevokeData(filter);
        System.out.println(datas.size());
    }
}
