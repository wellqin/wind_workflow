package com.bcx.wind.workflow.test.impl.cmd.managercmd;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.entity.WindTaskActor;
import com.bcx.wind.workflow.pojo.Task;
import com.bcx.wind.workflow.pojo.Wind;
import com.bcx.wind.workflow.pojo.WindUser;
import com.bcx.wind.workflow.test.core.submit.SubmitBase;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;


public class CreateChildTaskTest extends SubmitBase {

    @Test
    public void createChildTaskTest(){
        //班长提交
        Wind wind = monitor();

        //获取当前任务
        Task curTask = wind.getCurNode().get(0);

        //获取新任务 即班长任务
        List<Task> newTasks  = curTask.getSubmitTasks();

        //查询其中一个任务的审批人
        Task newTask = newTasks.get(0);
        String taskId = newTask.getWindTask().getId();


        List<WindUser> windUsers = new LinkedList<>();
        WindUser user = new WindUser().setUserId("B1003").setUserName("B张三");
        WindUser user1  = new WindUser().setUserId("B1004");
        windUsers.add(user);
        windUsers.add(user1);

        //添加子任务
        List<WindTask> windTasks = engine().managerService().addChildTask(taskId,windUsers);
        System.out.println(windTasks.size());

        //完成任务
        for(WindTask task : windTasks){
            QueryFilter filter = new QueryFilter()
                    .setTaskId(task.getId());
            List<WindTaskActor> actors = engine().getConfiguration().getAccess().selectTaskActorList(filter);
            String actorId = actors.get(0).getActorId();

            this.engine().managerService().completeChildTask(task.getId(),new WindUser().setUserId(actorId),"完成任务成");
        }

        commit();
    }
}
