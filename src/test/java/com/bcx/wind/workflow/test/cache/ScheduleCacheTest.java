package com.bcx.wind.workflow.test.cache;

import com.bcx.wind.workflow.cache.Cache;
import com.bcx.wind.workflow.cache.WindCache;
import com.bcx.wind.workflow.cache.imp.ScheduleCache;
import org.junit.Test;

/**
 * 定时清理缓存器测试
 *
 * @author zhanglei
 */
public class ScheduleCacheTest {

    @Test
    public void scheduleCacheTest() throws InterruptedException {

        Cache cache = new ScheduleCache(new WindCache(),2000L);

        cache.put("10001","james");
        Thread.sleep(2500);

        assert cache.get("10001") == null;
    }
}
