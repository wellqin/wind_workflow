package com.bcx.wind.workflow.test.cache;

import com.bcx.wind.workflow.cache.Cache;
import com.bcx.wind.workflow.cache.WindCache;
import org.junit.Test;

/**
 * @author zhanglei
 */
public class WindCacheTest {

    @Test
    public void windCacheTest(){
        Cache cache = new WindCache();

        cache.put("id","10001");
        cache.put("name","james");
        assert cache.size()==2;

        cache.removeKey("id");
        assert cache.size()==1;

        Object value = cache.get("name");
        assert value.equals("james");
        assert cache.contain("name");

        cache.clear();
        assert cache.size()==0;
    }
}
