package com.bcx.wind.workflow.test.access;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.entity.WindProcessConfig;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import com.bcx.wind.workflow.test.BaseTest;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * 流程配置测试
 *
 * @author zhanglei
 */
public class ProcessConfigTest extends BaseTest {

    private WindProcessConfig insert(){
        WindProcessConfig config = new WindProcessConfig()
                .setId(ObjectHelper.primaryKey())
                .setProcessId(ObjectHelper.primaryKey())
                .setProcessName("holiday")
                .setConfigName("起草配置")
                .setNodeId("edit")
                .setCondition("[]")
                .setNodeConfig("{}")
                .setApproveUser("[]")
                .setBusinessConfig("{}")
                .setSort(1)
                .setLevel(2)
                .setCreateTime(TimeHelper.nowDate())
                .setLogic("and");
        int ret = access().insertProcessConfig(config);
        assert  ret == 1;
        return config;
    }

    @Test
    public void insertTest(){
        insert();
        commit();;
    }

    @Test
    public void updateTest(){
        WindProcessConfig config = insert();
        config.setConfigName("测试更新名称");
        int ret = access().updateProcessConfig(config);
        assert ret == 1;
        commit();
    }

    @Test
    public void  queryOne(){
        WindProcessConfig config = insert();
        config = access().getProcessConfigById(config.getId());
        assert config != null;
        commit();
    }

    @Test
    public void removeByProcessId(){
        WindProcessConfig config = insert();
        int ret  = access().removeProcessConfigByProcessId(config.getProcessId());
        assert ret == 1;
        commit();
    }

    @Test
    public void removeOne(){
        WindProcessConfig config = insert();
        int ret = access().removeProcessConfigById(config.getId());
        assert ret == 1;
        commit();
    }

    @Test
    public void queryList(){
        QueryFilter filter = new QueryFilter()
                .setNodeId("edit");

        List<WindProcessConfig>  configs = access().selectProcessConfigList(filter);
        System.out.println(configs.size());
    }

    @Test
    public void  updateSort(){
        WindProcessConfig config = insert();
        List<String> ids = new LinkedList<>();
        ids.add(config.getId());
        int ret = access().updateSort(ids,1);
        assert  ret == 1;
        commit();
    }
}
