package com.bcx.wind.workflow.test.cache;

import com.bcx.wind.workflow.cache.Cache;
import com.bcx.wind.workflow.cache.WindCache;
import com.bcx.wind.workflow.cache.imp.DelFirstCache;
import org.junit.Test;

/**
 * 指定大小，删除最先值缓存测试
 *
 * @author zhanglei
 */
public class DelFirstCacheTest {

    @Test
    public void delfCacheTest(){
        Cache cache = new DelFirstCache(new WindCache(),4);

        cache.put("1001","james");
        cache.put("1002","tom");
        cache.put("1003","hir");
        cache.put("1004","jack");
        cache.put("1005","rose");

        assert cache.get("1001") == null;

    }
}
