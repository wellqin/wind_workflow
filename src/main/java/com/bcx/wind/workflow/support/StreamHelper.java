package com.bcx.wind.workflow.support;

import com.bcx.wind.workflow.exception.WorkflowException;

import java.io.*;

/**
 * 流处理工具
 *
 * @author zhanglei
 */
public final class StreamHelper {

    /**
     * 将流转换成二进制
     *
     * @param stream  流
     * @return         二进制
     */
    public static byte[]  getByte(InputStream stream){
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] bytes = new byte[1024];

            int num = stream.read(bytes);
            while(num != -1){
                outputStream.write(bytes,0,num);
                num = stream.read(bytes);
            }

            outputStream.flush();
            return outputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            throw new WorkflowException(e.getMessage());
        }finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getStr(InputStream stream){
        byte[] bytes = getByte(stream);
        return byteToStr(bytes);
    }

    public static String byteToStr(byte[] content){

        try {
            return new String(content,"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new WorkflowException(e.getMessage());
        }
    }

    public static InputStream strToStream(String context){
        try {
            new ByteArrayInputStream(context.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
