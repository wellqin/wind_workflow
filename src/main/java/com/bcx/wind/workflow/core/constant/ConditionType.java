package com.bcx.wind.workflow.core.constant;

/**
 * 流程定义配置匹配规则类型
 *
 * @author zhanglei
 */
public enum  ConditionType {
    /**
     * 字符串等于
     */
    eq,
    /**
     * 字符串不等于
     */
    ueq,
    /**
     * 字符串相似
     */
    lk,
    /**
     * 不相似
     */
    ulk,
    /**
     * 包含
     */
    in,
    /**
     * 不包含
     */
    uin,
    /**
     * 以什么开始
     */
    st,
    /**
     * 以什么结束
     */
    end,
    /**
     * 正则表达式
     */
    rg,
    /**
     * 正则表达式 反
     */
    irg,
    /**
     * 数字等于
     */
    neq,
    /**
     * 数字不等于
     */
    nueq,
    /**
     * 数字小于
     */
    nlt,
    /**
     * 数字小于等于
     */
    nelt,
    /**
     * 数字大于
     */
    ngt,
    /**
     * 数字大于等于
     */
    negt;




}
