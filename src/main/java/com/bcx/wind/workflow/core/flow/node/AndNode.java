package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.constant.NodeType;

/**
 * 并且分支节点
 *
 * @author zhanglei
 */
public class AndNode extends BaseNode {

    public AndNode(String nodeId, String nodeName) {
        super(nodeId, nodeName);
        this.nodeType = NodeType.AND;
    }

    @Override
    public void executor() {
        next(null);
    }

    public void build(){
        //构建路线
        buildPaths(now);
        //构建节点指针
        createNodePointer();
    }
}
