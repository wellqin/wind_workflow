package com.bcx.wind.workflow.core.constant;

/**
 * 逻辑and  or
 *
 * @author zhanglei
 */
public enum  Logic {

    /**
     * 并且
     */
    AND("and"),
    /**
     * 或者
     */
    OR("or");

    private String value;

    private Logic(String value){
        this.value = value;
    }

    public String value(){
        return this.value;
    }
}
