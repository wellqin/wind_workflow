package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.constant.NodeType;
import com.bcx.wind.workflow.core.flow.TaskModel;
import com.bcx.wind.workflow.interceptor.CustomInterceptor;

/**
 * 自定义节点
 *
 * @author zhanglei
 */
public class Custom extends BaseNode implements TaskModel {

    /**
     * 是否拦截
     */
    private boolean interceptor;

    public Custom(String nodeId, String nodeName) {
        super(nodeId, nodeName);
        this.nodeType = NodeType.CUSTOM;
    }

    @Override
    public void executor() {
        CustomInterceptor customInterceptor = this.commandContext().getConfiguration().getCustomInterceptor();
        if(interceptor && customInterceptor != null){
            customInterceptor.execute(this.wind(),this.commandContext());
        }
        next(null);
    }

    public void build(){
        //构建路线
        buildPaths(now);
        //构建节点指针
        createNodePointer();
    }

    public boolean isInterceptor() {
        return interceptor;
    }

    public void setInterceptor(boolean interceptor) {
        this.interceptor = interceptor;
    }
}
