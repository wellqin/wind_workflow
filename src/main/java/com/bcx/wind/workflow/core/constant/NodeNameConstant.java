package com.bcx.wind.workflow.core.constant;

/**
 * 节点名称常量
 *
 * @author zhanglei
 */
public class NodeNameConstant {

    public static final String START = "start";

    public static final String END = "end";

    public static final String DISC = "disc";

    public static final String TASK = "task";

    public static final String SCRIBE = "scribe";

    public static final String SCRIBE_TASK = "scribeTask";

    public static final String CUSTOM = "custom";

    public static final String AND = "and";

    public static final String OR = "or";

    public static final String DYNA = "dyna";

    public static final String AND_JOIN = "andJoin";

    public static final String OR_JOIN = "orJoin";

    public static final String DYNA_JOIN = "dynaJoin";

    public static final String ROUTER = "router";

    public static final String PROCESS = "process";

    public static final String PATH = "path";

    public static final String ASSIGNEE = "assignee";


}
