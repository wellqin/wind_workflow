package com.bcx.wind.workflow.core.flow.check;

import com.bcx.wind.workflow.support.ObjectHelper;

/**
 * 动态分支校验缓存
 *
 * @author zhanglei
 */
public class DynaCache {

    private ThreadLocal<Integer>  inDyna = new ThreadLocal<>();

    private DynaCache(){}


    private static class DynaCacheInstance{
        private DynaCacheInstance(){}
        private static DynaCache dynaCache = new DynaCache();
    }

    public static DynaCache getInstance(){
        return DynaCache.DynaCacheInstance.dynaCache;
    }

    public boolean inDyna(){
        Integer integer =  this.inDyna.get();
        return !ObjectHelper.isEmpty(integer) && integer==1;
    }

    public Integer  get(){
        return  inDyna.get();
    }

    public void  inDyna(int inOr){
        this.inDyna.set(inOr);
    }

    public void remove(){
        this.inDyna.remove();
    }
}
