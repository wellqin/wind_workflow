package com.bcx.wind.workflow.core.flow.handler;

import com.bcx.wind.workflow.core.constant.WorkflowOperate;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.pojo.ApproveUser;
import com.bcx.wind.workflow.pojo.StrandUser;
import com.bcx.wind.workflow.pojo.Task;
import com.bcx.wind.workflow.pojo.WindUser;
import com.bcx.wind.workflow.support.JsonHelper;
import com.bcx.wind.workflow.support.ObjectHelper;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.bcx.wind.workflow.core.constant.OrderVariableConstant.STRAND_APPROVE_USER;

/**
 * 串签handler
 *
 * @author zhanglei
 */
public class StrandHandler extends BaseExecuteHandler{

    /**
     * 所有需要审批的任务数量
     */
    private int allTaskCount = 1;


    public StrandHandler(CommandContext commandContext, Task task) {
        super(commandContext, task);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void commandExecute() {
        setApproveUser();
        //添加当前任务审批人数量，审批状态。
        addTaskApproveCount();
    }

    @Override
    public int allTaskCount() {
        return this.allTaskCount;
    }


    /**
     * 设置下个串签任务的审批人
     */
    @SuppressWarnings("unchecked")
    private void  setApproveUser(){
        Object strandObj = this.getOrderValue(STRAND_APPROVE_USER);
        //串签中的下一个审批审批人必须要在提交时已经配置在流程变量中，如果没有保存，则直接提交到后续任务
        if(strandObj != null){
            //如果存在，则将审批人取出来放到工作流审批人中
            Map<String,Object> strandMap = JsonHelper.objectToMap(strandObj);
            Object strandUserObj =  strandMap.get(this.task.getNodeId());

            if(strandUserObj != null){
                List<StrandUser> strandUsers = JsonHelper.coverObject(strandUserObj,List.class,StrandUser.class);
                if(!ObjectHelper.isEmpty(strandUsers)){
                    this.allTaskCount = strandUsers.size();
                    if(WorkflowOperate.submit.equals(this.wind().getOperate())) {
                        StrandUser user = strandUsers.stream().min(Comparator.comparingInt(StrandUser::getSort))
                                .orElseThrow(()->WindError.error("strand user data is error!"));
                        //过滤保存
                        strandUsers = strandUsers.stream().filter(strand->
                            strand.getSort() != user.getSort()
                        ).collect(Collectors.toList());

                        //合并审批人
                        mergeUser();
                        //设置新任务的审批人
                        WindUser windUser = user.getUser();
                        ApproveUser approveUser = new ApproveUser()
                                .setNodeId(this.task.getNodeId())
                                .setUsers(Collections.singletonList(windUser));
                        this.wind().setApproveUsers(Collections.singletonList(approveUser));

                        //删除串签审批人变量
                        strandMap.put(this.task.getNodeId(),strandUsers);
                        Map<String,Object> variable = this.wind().getWindOrder().variable();
                        variable.put(STRAND_APPROVE_USER,strandMap);
                        this.wind().getWindOrder().setVariable(JsonHelper.toJson(variable));
                    }
                }
            }
        }
    }

}
