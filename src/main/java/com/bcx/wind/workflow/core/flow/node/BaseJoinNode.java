package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.constant.TaskType;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.support.ObjectHelper;

import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseJoinNode extends BaseNode{


    BaseJoinNode(String nodeId, String nodeName) {
        super(nodeId, nodeName);
    }


    /**
     * 过滤掉不需要审批的任务，比如任务类型为订阅任务等
     */
    List<WindTask> filterNotMustApprove(){
        List<WindTask> tasks = this.wind().getAllTasks();
        return tasks.stream().filter(t->{
            if(TaskType.scribe.name().equals(t.getTaskType())){
                return false;
            }
            return !t.getTaskName().equals(task().getNodeId());
        }).collect(Collectors.toList());
    }

    /**
     * 删除订阅任务  暂时不用
     * @param scribeTasks   订阅任务
     */
    private void removeScribeTask(List<WindTask> scribeTasks){
        //执行到后续任务，删除订阅任务
        if(!ObjectHelper.isEmpty(scribeTasks)){
            List<String> ids = scribeTasks.stream().map(WindTask::getId).collect(Collectors.toList());
            this.commandContext().access().removeTaskActorByTaskIds(ids);
            this.commandContext().access().removeTaskByTaskIds(ids);
        }
    }

}
