package com.bcx.wind.workflow.core.script;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

/**
 * script  js
 *
 * @author zhanglei
 */
public class FlowScriptEngine extends ScriptEngineManager {

    private static final String SCRIPT_NAME = "js";

    private FlowScriptEngine(){}

    public static FlowScriptEngine getInstance(){
        return new FlowScriptEngine();
    }

    public static ScriptEngine  getEngine(){
        return getInstance().getEngineByName(SCRIPT_NAME);
    }

}
