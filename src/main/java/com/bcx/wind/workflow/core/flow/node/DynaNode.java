package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.constant.NodeType;

import java.util.List;

/**
 * 动态分支
 *
 * @author zhanglei
 */
public class DynaNode extends BaseNode{

    public DynaNode(String nodeId, String nodeName) {
        super(nodeId, nodeName);
        this.nodeType = NodeType.DYNA;
    }

    public void build(){
        //构建路线
        buildPaths(now);
        //构建节点指针
        createNodePointer();
    }

    @Override
    public void executor() {
        List<String> paths = this.wind().getSubmitPath().get(this.task().getNodeId());
        if(paths != null) {
            for(String path : paths) {
                next(path);
            }
            return;
        }
        next(null);
    }
}
