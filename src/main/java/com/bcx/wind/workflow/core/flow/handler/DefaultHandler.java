package com.bcx.wind.workflow.core.flow.handler;

import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.pojo.Task;

/**
 * 默认handler
 *
 * @author zhanglei
 */
public class DefaultHandler extends BaseExecuteHandler {


    public DefaultHandler(CommandContext commandContext, Task task) {
        super(commandContext, task);
    }

    @Override
    public void commandExecute() {
        //添加会签任务需要完成的最少提交数量
        addTaskApproveCount();
    }

    @Override
    public int allTaskCount() {
        //默认只需要审批一个就可以
        return 1;
    }
}
