package com.bcx.wind.workflow.core.constant;

/**
 * 工作流节点配置常量
 *
 * @author zhanglei
 */
public class NodeConfigConstant {

    /**
     * 提交路线
     */
    public static final String SUBMIT_LINE = "submitLine";

    /**
     * 退回节点
     */
    public static final String RETURN_NODE = "returnNode";

    /**
     * 任务内容
     */
    public static final String TASK_CONTENT = "taskContent";

    /**
     * 任务地址
     */
    public static final String TASK_LINK = "taskLink";

    /**
     * 会签分支审批人合并配置 1合并 -1或不设置表示不和并  默认不合并
     */
    public static final String JOINTLY_APPROVE_USER_SET = "jointlyApproveUserSet";

    /**
     * 转办任务次数配置 设置该设置后，在转办任务达到设置上限时，则不允许进行转办   默认可以转办 （数字）
     */
    public static final String TRANSFER_TIME_SET = "transferTimeSet";

    /**
     * 会签通过率设置  float 小数 0 - 1之间
     */
    public static final String JOINTLY_APPROVE_TIME_SET = "jointlyApproveTimeSet";

    /**
     * 会签最少审批人次   数字 int
     */
    public static final String JOINTLY_APPROVE_USER_MIN_COUNT =  "jointlyApproveUserMinCount";


    /**
     * 任务类型  普通ordinary  会签jointly  串签strand
     */
    public static final String TASK_TYPE = "taskType";
}
