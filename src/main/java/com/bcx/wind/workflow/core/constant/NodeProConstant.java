package com.bcx.wind.workflow.core.constant;

/**
 * 节点属性名称
 *
 * @author zhanglei
 */
public class NodeProConstant {

    public static final String JOINTLY = "jointly";

    public static final String STRAND = "strand";

    public static final String TO = "to";

    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String DISPLAY_NAME = "displayName";

    public static final String EXPR = "expr";

    public static final String INTERCEPTOR  = "interceptor";

    public static final String BLOCK = "block";

    public static final String ASSIGNEE_USER = "assigneeUser";

    public static final String USER_ID = "userId";

    public static final String USER_NAME = "userName";

    public static final String TRUE = "true";

    public static final String FALSE = "false";
}
