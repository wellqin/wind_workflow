package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.constant.NodeType;
import com.bcx.wind.workflow.core.constant.OrderStatus;
import com.bcx.wind.workflow.core.constant.OrderType;
import com.bcx.wind.workflow.core.flow.NodeModel;
import com.bcx.wind.workflow.core.flow.check.NodeCache;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.handler.TaskHandler;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static com.bcx.wind.workflow.core.constant.Constant.JSON_NULL;
import static com.bcx.wind.workflow.core.constant.NodeNameConstant.*;
import static com.bcx.wind.workflow.core.constant.NodeProConstant.*;

/**
 *  子流程节点
 *
 * @author zhanglei
 */
public class RouterNode extends BaseNode {

    /**
     * 子流程起始
     */
    private NodeModel childNodeModel;

    /**
     * 所有任务节点
     */
    private List<NodeModel> allTaskNodes = new LinkedList<>();

    /**
     * 所有节点
     */
    protected List<NodeModel>  allNodes = new LinkedList<>();

    /**
     * 第一个任务节点
     */
    private NodeModel firstTaskNode;


    /**
     * 是否阻塞  默认阻塞
     */
    private boolean block = true;


    /**
     * 最后一个任务节点
     */
    private List<NodeModel>  lastTaskNode = new LinkedList<>();

    public RouterNode(String nodeId, String nodeName) {
        super(nodeId, nodeName);
        this.nodeType = NodeType.ROUTER;
    }


    @Override
    public void executor() {
        if(this.block) {
            //创建子流程实例
            createChildOrder();
            NodeModel firstTask = getFirstTaskNode();
            new TaskHandler(this.commandContext(),firstTask,this.task()).execute();
        }else{
            //创建子流程实例
            createChildOrder();
            NodeModel firstTask = getFirstTaskNode();
            new TaskHandler(this.commandContext(),firstTask,this.task()).execute();
            //继续执行，不用等待子流程执行
            next(null);
        }
    }



    private void  createChildOrder(){
        WindOrder childOrder = new WindOrder()
                .setId(ObjectHelper.primaryKey())
                .setProcessId(this.wind().getWindProcess().getId())
                .setStatus(OrderStatus.run.name())
                .setType(OrderType.child.name())
                .setCreateTime(TimeHelper.nowDate())
                .setCreateUser(user().proxyUserId())
                .setParentId(wind().getWindOrder().getId())
                .setVersion(1)
                .setVariable(JSON_NULL)
                .setBusinessId(wind().getBusinessId())
                .setSystem(wind().getSystem());
        this.commandContext().access().insertOrderInstance(childOrder);
    }





    public void build(){
        //构建路线
        buildPaths(now);
        //构建节点指针
        createNodePointer();
        //构建子流程中的流程数据！
        buildRouter();
    }

    public NodeModel getNodeModel() {
        return childNodeModel;
    }

    public NodeModel getNodeModel(String nodeName){
        for(NodeModel nodeModel : allNodes){
            if(nodeName.equals(nodeModel.nodeId())){
                return nodeModel;
            }
        }
        return null;
    }

    public TaskNode getTaskNode(String nodeName){
        for(NodeModel nodeModel : allTaskNodes){
            if(nodeName.equals(nodeModel.nodeId())){
                return (TaskNode) nodeModel;
            }
        }
        return null;
    }

    public <T>T getNodeModel(String nodeName,Class<T> clazz){
        NodeModel nodeModel = getNodeModel(nodeName);
        if(!ObjectHelper.isEmpty(nodeModel)){
            return (T)nodeModel;
        }
        return null;
    }




    /**
     * 构建子流程  创建childNodeModel
     */
    private void buildRouter(){
        NodeList nodeList = now.getChildNodes();

        Element startElement = null;
        for(int i=0 ; i<nodeList.getLength() ; i++){
            Node node = nodeList.item(i);
            if(node instanceof Element){
                if(node.getNodeName().equals(NodeType.START.value())) {
                    startElement = (Element) node;
                }
            }
        }

        this.childNodeModel = initRouterStartNode(startElement);
        enhanceProcessModel();
    }


    private StartNode  initRouterStartNode(Element startElement){
        String name = ((Element) startElement).getAttribute(NAME);
        String displayName = ((Element) startElement).getAttribute(DISPLAY_NAME);
        StartNode startNode = new StartNode(name,displayName);

        NodeCache.getInstance().put(name,startNode);

        startNode.setElement(now);
        startNode.setParentNode(this);
        startNode.inRouter(true);
        startNode.setNow((Element) startElement);
        startNode.build();
        return startNode;
    }

    @Override
    public Element getNow() {
        return now;
    }

    @Override
    public void setNow(Element now) {
        this.now = now;
    }

    public NodeModel getChildNodeModel() {
        return childNodeModel;
    }

    public void setChildNodeModel(NodeModel childNodeModel) {
        this.childNodeModel = childNodeModel;
    }

    public List<NodeModel> getAllTaskNodes() {
        return allTaskNodes;
    }

    public void setAllTaskNodes(List<NodeModel> allTaskNodes) {
        this.allTaskNodes = allTaskNodes;
    }

    public List<NodeModel> getAllNodes() {
        return allNodes;
    }

    public void setAllNodes(List<NodeModel> allNodes) {
        this.allNodes = allNodes;
    }

    public NodeModel getFirstTaskNode() {
        return firstTaskNode;
    }

    public void setFirstTaskNode(NodeModel firstTaskNode) {
        this.firstTaskNode = firstTaskNode;
    }

    public List<NodeModel> getLastTaskNode() {
        return lastTaskNode;
    }

    public void setLastTaskNode(List<NodeModel> lastTaskNode) {
        this.lastTaskNode = lastTaskNode;
    }

    public boolean isBlock() {
        return block;
    }

    public void setBlock(boolean block) {
        this.block = block;
    }

    /**
     * 增强模型
     */
    public void enhanceProcessModel(){
        buildAllNodes(this.childNodeModel);
        buildAllTaskNodes(this.childNodeModel);
        buildFirstTaskNode();
        //设置前置任务节点
        for(NodeModel node : this.allNodes){
            createPreTaskNodes(node,node);
        }
        buildLastTaskNodes();
    }

    private void buildAllNodes(NodeModel nodeModel){
        if(!this.allNodes.contains(nodeModel)){
            this.allNodes.add(nodeModel);
        }
        List<NodeModel> nextNodes = nodeModel.next();
        if(!ObjectHelper.isEmpty(nextNodes)){
            for(NodeModel node : nextNodes){
                buildAllNodes(node);
            }
        }
    }


    private void buildAllTaskNodes(NodeModel nodeModel){
        if(TASK.equals(nodeModel.nodeType().value())){
            if(!allTaskNodes.contains(nodeModel)) {
                this.allTaskNodes.add(nodeModel);
            }
        }
        List<NodeModel> nextNodes = nodeModel.nextTask();
        if(!ObjectHelper.isEmpty(nextNodes)){
            for(NodeModel node : nextNodes){
                buildAllTaskNodes(node);
            }
        }
    }


    private void buildFirstTaskNode(){
        this.firstTaskNode = childNodeModel.nextTask().get(0);
    }


    private void buildLastTaskNodes(){
        if(!ObjectHelper.isEmpty(this.allNodes)){
            List<NodeModel> nodeModels = this.allNodes.stream().filter(
                    node->END.equals(node.nodeType().value())).collect(Collectors.toList());
            if(!ObjectHelper.isEmpty(nodeModels)) {
                this.lastTaskNode = nodeModels.get(0).preTask();
                return;
            }
        }
        buildLastTaskNodes(this.childNodeModel);
    }


    private void  buildLastTaskNodes(NodeModel nodeModel){
        List<NodeModel> nextNodes = nodeModel.next();

        if(!ObjectHelper.isEmpty(nextNodes)){
            for(NodeModel node : nextNodes){
                if(END.equals(node.nodeType().value())){
                    this.lastTaskNode = node.preTask();
                    return;
                }else{
                    buildLastTaskNodes(node);
                }
            }
        }
    }
}
