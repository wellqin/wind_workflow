package com.bcx.wind.workflow.core.constant;

/**
 * @author zhanglei
 */
public class Constant {

    public static final String AND = "and";

    public static final String OR = "or";

    public static final String JSON_NULL = "{}";

    public static final String  ARRAY_NULL = "[]";

    public static final String  WIND_ADMIN = "wind_admin";

    public static final String  ZERO = "0";

    public static final String  ONE = "1";

    public static final String  ROOT_MODULE = "root_module";

    public static final String  MODULE_CACHE = "moduleCache";


}
