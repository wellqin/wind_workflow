package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.core.constant.NodeType;
import com.bcx.wind.workflow.support.Assert;
import java.util.List;
import static com.bcx.wind.workflow.core.constant.NodeConfigConstant.SUBMIT_LINE;
import static com.bcx.wind.workflow.message.ErrorCode.SUBMIT_LINE_NOT_FOUND;

/**
 * 决策路由节点
 *
 * @author zhanglei
 */
public class DiscNode extends BaseNode {


    public DiscNode(String nodeId, String nodeName) {
        super(nodeId, nodeName);
        this.nodeType = NodeType.DISC;
    }

    @Override
    public void executor() {
        next(getPath());
    }

    private String  getPath(){
        List<String> paths = this.wind().getSubmitPath().get(this.task().getNodeId());
        if(paths == null) {
            String path = getConfigValue(SUBMIT_LINE);
            Assert.notEmptyError(SUBMIT_LINE_NOT_FOUND,path,this.task().getNodeId());
            return path;
        }
        return paths.get(0);
    }

    public void build(){
        //构建路线
        buildPaths(now);
        //构建节点指针
        createNodePointer();
    }
}
