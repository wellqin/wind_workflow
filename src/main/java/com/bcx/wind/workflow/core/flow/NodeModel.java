package com.bcx.wind.workflow.core.flow;

import com.bcx.wind.workflow.core.constant.NodeType;
import com.bcx.wind.workflow.core.flow.node.Path;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.pojo.Task;

import java.util.List;

/**
 * 工作流节点类型
 *
 * @author zhanglei
 */
public interface NodeModel extends Model{



    /**
     * 返回节点类型
     *
     * @return  节点类型
     */
    NodeType  nodeType();

    /**
     * 是否在子流程中
     *
     * @return  boolean
     */
    boolean  inRouter();


    /**
     * 设置是否在子流程中
     *
     * @param inRouter  boolean
     */
    void  inRouter(boolean inRouter);


    /**
     * 返回节点父节点
     *
     * @return  父节点
     */
    NodeModel parentNode();


    /**
     * 路线
     *
     * @return  路线集合
     */
    List<Path>  path();


    /**
     * 返回节点直属后续节点
     *
     * @return   子节点集合
     */
    List<NodeModel>  next();


    /**
     * 返回节点前面节点
     *
     * @return  前面节点集合
     */
    List<NodeModel>  pre();


    /**
     * 返回节点直属任务节点
     *
     * @return  任务节点集合
     */
    List<NodeModel>   nextTask();


    /**
     * 返回节点前面任务节点
     *
     * @return  前面节点集合
     */
    List<NodeModel>   preTask();


    /**
     * 返回指定路线后的任务节点
     *
     * @param line   路线
     * @return       任务节点集合
     */
    List<NodeModel>   submitLineTask(String line);


    /**
     * 获取后续所有可以提交的任务
     *
     * @return  任务节点集合
     */
    List<NodeModel>   nextAllTask();



    /**
     * 执行
     */
    void  execute();


    /**
     * 往后执行
     *
     * @param line  路线
     */
    void  next(String line);


    /**
     * 设置执行数据
     *
     * @param commandContext  执行数据
     */
    void  context(CommandContext commandContext, Task task);

}
