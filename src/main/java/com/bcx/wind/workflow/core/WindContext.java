package com.bcx.wind.workflow.core;

/**
 * 工作流工具容器
 *
 * @author zhanglei
 */
public interface WindContext {


    /**
     * 通过工具类 获取工具
     *
     * @param clazz  工具类
     * @param <T>    类型
     * @return      工具
     */
    <T>T  get(Class<T> clazz);


    /**
     * 通过工具名称获取工具
     *
     * @param name   工具名称
     * @return    工具
     */
    Object   get(String name);


    /**
     * 设置工具
     *
     * @param name   工具名称
     * @param value  工具
     */
    void   set(String name,Object value);


}
