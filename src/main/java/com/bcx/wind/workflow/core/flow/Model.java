package com.bcx.wind.workflow.core.flow;

/**
 * 节点模型
 *
 * @author zhanglei
 */
public interface Model {

    /**
     * 返回节点ID
     *
     * @return   节点ID名称
     */
    String  nodeId();

    /**
     * 返回节点名称
     *
     * @return  节点名称
     */
    String  nodeName();

}
