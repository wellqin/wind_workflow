package com.bcx.wind.workflow.core.flow.node;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.core.constant.NodeType;
import com.bcx.wind.workflow.core.constant.OrderStatus;
import com.bcx.wind.workflow.entity.WindActHistory;
import com.bcx.wind.workflow.entity.WindHistOrder;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 结束节点
 *
 * @author zhanglei
 */
public class EndNode extends BaseNode {


    public EndNode(String nodeId, String nodeName) {
        super(nodeId, nodeName);
        this.nodeType = NodeType.END;
    }

    @Override
    public void executor() {
        if(this.parentNode instanceof ProcessModel) {
            //添加历史流程实例 删除执行流程实例
            addHistoryOrder();
            //删除历史执行履历，添加历史完毕履历
            addCompleteHistory();
            //删除撤销历史数据
            removeRevokeData();
            //删除流程实例中无关的任务
            removeOtherTask();
        }else if(this.parentNode instanceof RouterNode){
            //如果是子流程，则跳出子流程，进入主流程
            this.parentNode.next(null);
        }
    }

    /**
     * 删除没用的任务任务
     */
    private void removeOtherTask(){
        List<WindTask> allTasks = this.wind().getAllTasks();
        //执行到后续任务，删除订阅任务
        if(!ObjectHelper.isEmpty(allTasks)){
            List<String> ids = allTasks.stream().map(WindTask::getId).collect(Collectors.toList());
            this.commandContext().access().removeTaskActorByTaskIds(ids);
            this.commandContext().access().removeTaskByTaskIds(ids);
        }
    }


    /**
     * 删除撤销历史数据
     */
    private void removeRevokeData(){
        this.commandContext().access().deleteWindRevokeDataByOrderId(this.wind().getWindOrder().getId());
    }

    private  void addCompleteHistory(){
        QueryFilter filter = new QueryFilter()
                .setOrderId(this.wind().getWindOrder().getId());
        List<WindActHistory> actHistories = this.commandContext().access().selectActiveHistoryList(filter);
        //添加完毕履历
        this.commandContext().access().insertCompleteHistoryList(actHistories);
        //删除执行履历
        this.commandContext().access().removeActiveHistoryByOrderId(this.wind().getWindOrder().getId());
    }

    /**
     * 添加历史流程实例 删除执行流程实例
     */
    private void addHistoryOrder(){
        WindHistOrder histOrder = histOrder(this.wind().getWindOrder());
        this.commandContext().access().removeOrderInstanceById(this.wind().getWindOrder().getId());
        //删除子流程，如果有的话
        this.commandContext().access().removeOrderInstanceByParentId(this.wind().getWindOrder().getId());
        this.commandContext().access().insertWindHistOrder(histOrder);
    }



    private WindHistOrder histOrder(WindOrder windOrder){
        return new WindHistOrder()
                .setId(windOrder.getId())
                .setProcessId(windOrder.getProcessId())
                .setStatus(OrderStatus.complete.name())
                .setCreateUser(windOrder.getCreateUser())
                .setCreateTime(windOrder.getCreateTime())
                .setExpireTime(windOrder.getExpireTime())
                .setParentId(windOrder.getParentId())
                .setVersion(windOrder.getVersion())
                .setData(windOrder.getData())
                .setBusinessId(windOrder.getBusinessId())
                .setSystem(windOrder.getSystem())
                .setFinishTime(TimeHelper.nowDate());
    }

    public void build(){
        //构建路线
        buildPaths(now);
        //构建节点指针
        createNodePointer();
    }
}
