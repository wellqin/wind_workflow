package com.bcx.wind.workflow.handler;

import com.bcx.wind.workflow.core.flow.NodeModel;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.pojo.*;

import java.util.Map;

import static com.bcx.wind.workflow.core.constant.Constant.WIND_ADMIN;
import static com.bcx.wind.workflow.core.constant.NodeConfigConstant.TASK_LINK;

/**
 * @author zhanglei
 */
public abstract class BaseHandler implements Handler {

    /**
     * 执行数据
     */
    protected CommandContext commandContext;

    /**
     * 当前节点
     */
    protected NodeModel curNode;

    /**
     * 上一个任务实例
     */
    protected Task  preTask;


    public BaseHandler(CommandContext commandContext,NodeModel curNode,Task preTask){
        this.commandContext = commandContext;
        this.curNode = curNode;
        this.preTask = preTask;
    }

    public abstract void  execute();

    @Override
    public void handler() {
        execute();
    }

    protected Wind  wind(){
        return this.commandContext.getWind();
    }

    protected Map<String,Object> nodeConfig(){
        return this.preTask.getConfig().nodeConfig();
    }


    protected void addOrderVariable(String key,Object value){
        WindOrder windOrder = wind().getWindOrder();
        if(windOrder != null){
            windOrder.addVariable(key,value);
        }
    }


    protected WindUser user(){
        WindUser user = wind().getUser();
        if(user == null){
            return new WindUser().setUserId(WIND_ADMIN)
                    .setUserName("管理员");
        }
        return user;
    }


    /**
     * 获取待办链接
     * @return  待办链接
     */
    protected   String  taskLink(){
        Object object = nodeConfig().get(TASK_LINK);
        if(object == null){
            return null;
        }
        return object.toString();
    }


}
