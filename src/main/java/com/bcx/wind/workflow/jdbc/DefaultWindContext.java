package com.bcx.wind.workflow.jdbc;

import com.bcx.wind.workflow.core.WindContext;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.script.ScriptEngineManager;
import java.util.HashMap;
import java.util.Map;

public class DefaultWindContext implements WindContext {

    /**
     * 容器
     */
    private Map<String,Object>  context = new HashMap<>();

    public DefaultWindContext(){
        //初始化工具
        init();
    }


    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(Class<T> clazz) {
        for(Map.Entry<String,Object> data : context.entrySet()){

            Object object = data.getValue();
            if(object.getClass().isAssignableFrom(clazz)){
                return (T) object;
            }
        }
        return null;
    }

    @Override
    public Object get(String name) {
        return context.get(name);
    }

    @Override
    public void set(String name, Object value) {
        this.context.put(name,value);
    }


    private void init(){
        //构建jackson
        this.context.put("objectMapper",new ObjectMapper());
        //脚本工具
        this.context.put("script",new ScriptEngineManager().getEngineByName("js"));
    }


}
