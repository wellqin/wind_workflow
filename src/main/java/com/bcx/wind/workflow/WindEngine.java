package com.bcx.wind.workflow;

import com.bcx.wind.workflow.imp.Configuration;
import com.bcx.wind.workflow.imp.service.*;

/**
 * <p>
 * 工作流执行引擎
 * </p>
 *
 * @author zhanglei
 * @since 2019-02-24
 */
public interface WindEngine {


    /**
     * 运行时 服务
     *
     * @return   runtimeService
     */
    RuntimeService runtimeService();

    /**
     * 流程管理监控服务
     *
     * @return   managerService
     */
    ManagerService managerService();

    /**
     * 静态资源管理服务
     *
     * @return  RepositoryService
     */
    RepositoryService repositoryService();

    /**
     * 历史模块服务
     *
     * @return  historyService
     */
    HistoryService historyService();


    /**
     * 工作流核心服务
     *
     * @return  windSession
     */
    WindCoreService windCoreService();


    /**
     * 获取全局配置数据
     *
     * @return  全局配置
     */
    Configuration  getConfiguration();

}
