package com.bcx.wind.workflow.imp;

import com.bcx.wind.workflow.imp.service.Service;
import com.bcx.wind.workflow.interceptor.CommandExecutor;

/**
 * 中间服务
 *
 * @author zhanglei
 */
public class ServiceImpl implements Service {

    protected Configuration configuration;

    protected CommandExecutor commandExecutor;


    public void setExecutor(CommandExecutor executor){
        this.commandExecutor = executor;
    }

    @Override
    public Configuration configuration() {
        return this.configuration;
    }

    @Override
    public void configuration(Configuration configuration) {
        this.configuration = configuration;
    }
}
