package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.Assert;
import com.bcx.wind.workflow.support.ObjectHelper;
import com.bcx.wind.workflow.support.TimeHelper;

import static com.bcx.wind.workflow.core.constant.Constant.JSON_NULL;
import static com.bcx.wind.workflow.message.ErrorCode.CREATE_TASK_FAIL;

/**
 * 创建新任务命令
 *
 * @author zhanglei
 */
public class CreateNewTaskCommand implements Command<WindTask> {

    /**
     * 添加的任务
     */
    private WindTask windTask;

    public CreateNewTaskCommand(WindTask windTask){
        this.windTask = windTask;
    }

    @Override
    public WindTask executor(CommandContext context) {
        //校验
        checkData();
        //新增
        context.access().insertTaskInstance(this.windTask);
        return this.windTask;
    }


    /**
     * 校验是否为空
     */
    private void checkData(){
        if(windTask == null){
            WindError.error(CREATE_TASK_FAIL,null,"windTask");
        }
        if(this.windTask.getId() == null){
            this.windTask.setId(ObjectHelper.primaryKey());
        }
        Assert.notEmptyError(CREATE_TASK_FAIL,windTask.getTaskName(),"taskName");
        Assert.notEmptyError(CREATE_TASK_FAIL,windTask.getTaskType(),"taskType");
        Assert.notEmptyError(CREATE_TASK_FAIL,windTask.getStatus(),"status");
        Assert.notEmptyError(CREATE_TASK_FAIL,windTask.getOrderId(),"orderId");
        Assert.notEmptyError(CREATE_TASK_FAIL,windTask.getProcessId(),"processId");

        if(this.windTask.getCreateTime() == null){
            this.windTask.setCreateTime(TimeHelper.nowDate());
        }
        this.windTask.setApproveCount(0);
        if(this.windTask.getVariable()==null){
            this.windTask.setVariable(JSON_NULL);
        }
        this.windTask.setVersion(1);
    }
}
