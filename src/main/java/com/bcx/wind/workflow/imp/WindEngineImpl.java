package com.bcx.wind.workflow.imp;

import com.bcx.wind.workflow.WindEngine;
import com.bcx.wind.workflow.imp.service.*;

/**
 * 工作流执行引擎实现
 *
 * @author zhanglei
 */
public class WindEngineImpl implements WindEngine {

    /**
     * 运行时服务
     */
    private RuntimeService runtimeService;

    /**
     * 资源服务
     */
    private RepositoryService repositoryService;

    /**
     * 历史服务
     */
    private HistoryService historyService;

    /**
     * 管理监控服务
     */
    private ManagerService managerService;

    /**
     * 工作流核心服务
     */
    private WindCoreService windCoreService;

    /**
     * 全局配置
     */
    private Configuration configuration;

    public WindEngineImpl(Configuration configuration){
         this.configuration = configuration;
         this.runtimeService = configuration.getRuntimeService();
         this.repositoryService = configuration.getRepositoryService();
         this.managerService = configuration.getManagerService();
         this.historyService = configuration.getHistoryService();
         this.windCoreService = configuration.getWindCoreService();
    }

    @Override
    public RuntimeService runtimeService() {
        return this.runtimeService;
    }

    @Override
    public ManagerService managerService() {
        return this.managerService;
    }

    @Override
    public RepositoryService repositoryService() {
        return this.repositoryService;
    }

    @Override
    public HistoryService historyService() {
        return this.historyService;
    }

    @Override
    public WindCoreService windCoreService() {
        return this.windCoreService;
    }

    @Override
    public Configuration getConfiguration() {
        return this.configuration;
    }

}
