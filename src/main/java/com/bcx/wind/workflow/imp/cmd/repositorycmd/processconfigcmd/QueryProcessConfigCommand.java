package com.bcx.wind.workflow.imp.cmd.repositorycmd.processconfigcmd;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.entity.WindProcessConfig;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;

import java.util.List;

import static com.bcx.wind.workflow.message.ErrorCode.QUERY_FILTER_NULL;

/**
 * 查询流程配置命令实现
 *
 * @author zhanglei
 */
public class QueryProcessConfigCommand implements Command<List<WindProcessConfig>> {

    /**
     * 过滤条件
     */
    private QueryFilter filter;

    /**
     * 执行数据
     */
    private CommandContext commandContext;

    public QueryProcessConfigCommand(QueryFilter filter){
        this.filter = filter;
    }

    @Override
    public List<WindProcessConfig> executor(CommandContext context) {
        try{
            this.commandContext = context;
            return execute();
        }catch (Exception e){
            commandContext.log(e);
        }
        return null;
    }

    private List<WindProcessConfig>  execute(){
        if(this.filter == null){
            WindError.error(QUERY_FILTER_NULL,null);
        }
        return this.commandContext.access().selectProcessConfigList(filter);
    }
}
