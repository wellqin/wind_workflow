package com.bcx.wind.workflow.imp.service;

import com.bcx.wind.workflow.imp.Configuration;

/**
 * 基接口
 *
 * @author zhanglei
 */
public interface Service {

    /**
     * 全局配置数据
     *
     * @return   configuration
     */
    Configuration  configuration();

    /**
     * 设置配置数据
     * @param configuration  配置
     */
    void configuration(Configuration configuration);
}
