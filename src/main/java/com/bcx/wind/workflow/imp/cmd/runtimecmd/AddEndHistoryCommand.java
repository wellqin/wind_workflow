package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.entity.WindActHistory;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;

import java.util.List;

/**
 * 添加完毕执行履历命令
 *
 * @author zhanglei
 */
public class AddEndHistoryCommand implements Command<Void> {

    /**
     * 执行履历
     */
    private List<WindActHistory> actHistoryList;

    public AddEndHistoryCommand(List<WindActHistory> windActHistories){
        this.actHistoryList = windActHistories;
    }


    @Override
    public Void executor(CommandContext context) {
        if(this.actHistoryList == null){
            return null;
        }
        context.access().insertCompleteHistoryList(this.actHistoryList);
        return null;
    }
}
