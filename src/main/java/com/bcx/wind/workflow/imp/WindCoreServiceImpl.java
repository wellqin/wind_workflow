package com.bcx.wind.workflow.imp;

import com.bcx.wind.workflow.imp.cmd.corecmd.*;
import com.bcx.wind.workflow.imp.service.WindCoreService;
import com.bcx.wind.workflow.pojo.*;
import com.bcx.wind.workflow.pojo.variable.*;

import java.util.List;

/**
 * 工作流核心服务实现
 *
 * @author zhanglei
 */
public class WindCoreServiceImpl extends ServiceImpl implements WindCoreService {


    @Override
    public Wind buildByProcessId(String processId, String businessId) {
        BuildVariable variable = new BuildVariable()
                .setProcessId(processId)
                .setBusinessId(businessId);
        return this.commandExecutor.executor(new BuildCommand(variable));
    }

    @Override
    public Wind buildByProcessId(String processId, String businessId, WindUser user) {
        BuildVariable variable = new BuildVariable()
                .setProcessId(processId)
                .setBusinessId(businessId)
                .setUser(user);
        return this.commandExecutor.executor(new BuildCommand(variable));
    }

    @Override
    public Wind buildByProcessId(String processId, String businessId, WindUser user, String system) {
        BuildVariable variable = new BuildVariable()
                .setProcessId(processId)
                .setBusinessId(businessId)
                .setUser(user).setSystem(system);
        return this.commandExecutor.executor(new BuildCommand(variable));
    }

    @Override
    public Wind buildByProcessName(String processName, String businessId) {
        BuildVariable variable = new BuildVariable()
                .setProcessId(processName)
                .setBusinessId(businessId);
        return this.commandExecutor.executor(new BuildCommand(variable));
    }

    @Override
    public Wind buildByProcessName(String processName, String businessId, WindUser user) {
        BuildVariable variable = new BuildVariable()
                .setProcessId(processName)
                .setBusinessId(businessId)
                .setUser(user);
        return this.commandExecutor.executor(new BuildCommand(variable));
    }

    @Override
    public Wind buildByProcessName(String processName, String businessId, WindUser user, String system) {
        BuildVariable variable = new BuildVariable()
                .setProcessId(processName)
                .setBusinessId(businessId)
                .setUser(user)
                .setSystem(system);
        return this.commandExecutor.executor(new BuildCommand(variable));
    }

    @Override
    public Wind buildByVariable(BuildVariable variable) {
        return this.commandExecutor.executor(new BuildCommand(variable));
    }

    @Override
    public Wind submit(String orderId, List<ApproveUser> approveUsers) {
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setApproveUsers(approveUsers);
        return this.commandExecutor.executor(new SubmitCommand(variable));
    }

    @Override
    public Wind submit(String orderId, List<ApproveUser> approveUsers, WindUser user) {
        SubmitVariable variable = new SubmitVariable()
                .setOrderId(orderId)
                .setApproveUsers(approveUsers)
                .setUser(user);
        return this.commandExecutor.executor(new SubmitCommand(variable));
    }

    @Override
    public Wind submit(SubmitVariable variable) {
        return this.commandExecutor.executor(new SubmitCommand(variable));
    }

    @Override
    public Wind reject(String orderId) {
        RejectVariable variable = new RejectVariable()
                .setOrderId(orderId);
        return commandExecutor.executor(new RejectCommand(variable));
    }

    @Override
    public Wind reject(String orderId, WindUser user) {
        RejectVariable variable = new RejectVariable()
                .setOrderId(orderId)
                .setUser(user);
        return commandExecutor.executor(new RejectCommand(variable));
    }

    @Override
    public Wind reject(RejectVariable variable) {
        return commandExecutor.executor(new RejectCommand(variable));
    }

    @Override
    public Wind complete(String orderId) {
        CompleteVariable completeVariable = new CompleteVariable()
                .setOrderId(orderId);
        return commandExecutor.executor(new CompleteCommand(completeVariable));
    }

    @Override
    public Wind complete(String orderId, WindUser user) {
        CompleteVariable completeVariable = new CompleteVariable()
                .setOrderId(orderId)
                .setUser(user);
        return commandExecutor.executor(new CompleteCommand(completeVariable));
    }

    @Override
    public Wind complete(CompleteVariable variable) {
        return commandExecutor.executor(new CompleteCommand(variable));
    }

    @Override
    public Wind withDraw(String orderId) {
        WithDrawVariable variable = new WithDrawVariable()
                .setOrderId(orderId);
        return this.commandExecutor.executor(new WithDrawCommand(variable));
    }

    @Override
    public Wind withDraw(String orderId, WindUser user) {
        WithDrawVariable variable = new WithDrawVariable()
                .setOrderId(orderId)
                .setUser(user);
        return this.commandExecutor.executor(new WithDrawCommand(variable));
    }

    @Override
    public Wind withDraw(WithDrawVariable variable) {
        return this.commandExecutor.executor(new WithDrawCommand(variable));
    }

    @Override
    public Wind revoke(String businessId) {
        RevokeVariable variable = new RevokeVariable()
                .setBusinessId(businessId);
        return this.commandExecutor.executor(new RevokeCommand(variable));
    }

    @Override
    public Wind revoke(String businessId, String histId, WindUser user) {
        RevokeVariable variable = new RevokeVariable()
                .setBusinessId(businessId)
                .setHistId(histId)
                .setUser(user);
        return this.commandExecutor.executor(new RevokeCommand(variable));
    }

    @Override
    public Wind revoke(RevokeVariable variable) {
        return this.commandExecutor.executor(new RevokeCommand(variable));
    }

}
