package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;

import static com.bcx.wind.workflow.message.ErrorCode.DATABASE_OPERATE_ERROR;

/**
 * 删除任务命令
 *
 * @author zhanglei
 */
public class RemoveTaskCommand implements Command<Void> {

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 流程实例ID
     */
    private String orderId;

    public RemoveTaskCommand(String taskId,String orderId){
        this.taskId = taskId;
        this.orderId = orderId;
    }

    @Override
    public Void executor(CommandContext context) {
        if(this.taskId != null && context.access().removeTaskInstanceById(this.taskId)!=1){
            WindError.error(DATABASE_OPERATE_ERROR,null);
        }

        if(this.orderId != null && context.access().removeTaskByOrderId(this.orderId) != 1){
            WindError.error(DATABASE_OPERATE_ERROR,null);
        }
        return null;
    }
}
