package com.bcx.wind.workflow.imp.cmd.managercmd;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.Assert;

import java.util.List;

import static com.bcx.wind.workflow.message.ErrorCode.QUERY_FILTER_NULL;

public class QueryOrderCommand implements Command<List<WindOrder>> {

    /**
     * 过滤条件
     */
    private QueryFilter queryFilter;

    /**
     * 分页数据
     */
    private WindPage windPage;

    public QueryOrderCommand(QueryFilter filter){
        this.queryFilter = filter;
    }


    public QueryOrderCommand(QueryFilter queryFilter , WindPage windPage){
        this.queryFilter = queryFilter;
        this.windPage = windPage;
    }


    @Override

    public List<WindOrder> executor(CommandContext context) {
        try{
            return execute(context);
        }catch (Exception e){
            context.log(e);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private List<WindOrder> execute(CommandContext context) {
        Assert.notEmptyError(QUERY_FILTER_NULL,this.queryFilter);

        if(this.windPage == null){
            return context.access().selectOrderInstanceList(this.queryFilter);
        }

        return context.access().selectOrderInstanceList(this.queryFilter,this.windPage);
    }
}
