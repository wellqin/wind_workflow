package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.entity.WindRevokeData;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.Assert;
import com.bcx.wind.workflow.support.ObjectHelper;

import java.util.LinkedList;
import java.util.List;

import static com.bcx.wind.workflow.message.ErrorCode.QUERY_FILTER_NULL;

/**
 * 查询撤销数据队列命令
 *
 * @author zhanglei
 */
public class QueryRevokeDataCommand  implements Command<List<WindRevokeData>> {

    /**
     * 流程实例号
     */
    private String orderId;

    /**
     * 业务ID
     */
    private String businessId;

    public QueryRevokeDataCommand(String orderId,String businessId){
        this.orderId = orderId;
        this.businessId = businessId;
    }

    @Override
    public List<WindRevokeData> executor(CommandContext context) {
        //校验参数
        if(!checkVariable(context)){
            return new LinkedList<>();
        }

        //查询
        QueryFilter filter = new QueryFilter()
                .setOrderId(orderId);
        return context.access().selectRevokeData(filter);
    }


    private boolean checkVariable(CommandContext commandContext){
        Assert.isTrueError(QUERY_FILTER_NULL,ObjectHelper.allEmpty(orderId,businessId));
        if(ObjectHelper.isEmpty(orderId)){
            QueryFilter filter = new QueryFilter()
                    .setBusinessId(businessId);
            List<WindOrder> windOrders = commandContext.access().selectOrderInstanceList(filter);
            if(ObjectHelper.isEmpty(windOrders)){
                return false;
            }

            this.orderId = windOrders.get(0).getId();
            return true;
        }

        WindOrder windOrder = commandContext.access().getOrderInstanceById(orderId);
        return !ObjectHelper.isEmpty(windOrder);
    }
}
