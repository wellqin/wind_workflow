package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;

import static com.bcx.wind.workflow.message.ErrorCode.DATABASE_OPERATE_ERROR;
import static com.bcx.wind.workflow.message.ErrorCode.OPERATE_FAIL;

/**
 * 删除流程实例命令
 *
 * @author zhanglei
 */
public class RemoveOrderCommand implements Command<Void> {

    /**
     * 流程实例ID
     */
    private String orderId;

    public RemoveOrderCommand(String orderId){
        this.orderId = orderId;
    }

    @Override
    public Void executor(CommandContext context) {
        if(orderId == null){
            WindError.error(OPERATE_FAIL,null,"order","remove","orderId");
        }
        if(context.access().removeOrderInstanceById(this.orderId) != 1){
            WindError.error(DATABASE_OPERATE_ERROR,null);
        }
        return null;
    }




}
