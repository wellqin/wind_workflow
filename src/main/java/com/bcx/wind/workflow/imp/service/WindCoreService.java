package com.bcx.wind.workflow.imp.service;

import com.bcx.wind.workflow.pojo.*;
import com.bcx.wind.workflow.pojo.variable.*;

import java.util.List;

/**
 * 工作流核心服务接口
 * 功能包含工作流  提交  退回  撤销  撤回   完结  创建
 *
 * @author zhanglei
 */
public interface WindCoreService {

    /**
     * 创建工作流实例
     * 创建工作流主要针对指定流程，生成指定人创建的流程实例，以及流程的第一个任务。流程执行历史记录。
     * 返回信息包含，流程实例信息，当前任务信息，后续任务信息，以及当前任务配置，后续任务配置信息，流程模型。
     * 使用者可以根据当前任务或者后续任务信息和配置信息结合着自己的需求业务进一步开发。
     *
     * 没有创建人，则默认使用工作流自定义用户
     *
     * @param processId    流程定义ID
     * @param businessId   业务数据ID
     * @return              工作流实例信息
     */
    Wind  buildByProcessId(String processId, String businessId);


    /**
     * 创建工作流实例
     *
     * @param processId   流程定义ID
     * @param businessId  业务数据ID
     * @param user         创建人
     * @return             工作流实例信息
     */
    Wind  buildByProcessId(String processId,String businessId,WindUser user);


    /**
     * 创建工作流实例
     *
     * @param processId   流程定义ID
     * @param businessId  业务数据ID
     * @param user         创建人
     * @param system      所属模块
     * @return             工作流实例信息
     */
    Wind  buildByProcessId(String processId,String businessId,WindUser user,String system);


    /**
     * 通过流程定义名称创建工作流实例
     * 通过名称启动，默认启动相同名称最高版本的工作流定义
     *
     *
     * @param processName   流程定义名称
     * @param businessId    业务数据ID
     * @return             工作流实例信息
     */
    Wind  buildByProcessName(String processName,String businessId);


    /**
     * 通过流程定义名称创建工作流实例，含创建人
     *
     * @param processName   模型定义名称
     * @param businessId    业务数据ID
     * @param user          创建人
     * @return             工作流实例信息
     */
    Wind  buildByProcessName(String processName,String businessId,WindUser user);


    /**
     * 通过流程定义名称创建工作流实例，含创建人
     *
     * @param processName   模型定义名称
     * @param businessId    业务数据ID
     * @param user          创建人
     * @param system     所属模块
     * @return             工作流实例信息
     */
    Wind  buildByProcessName(String processName,String businessId,WindUser user,String system);


    /**
     * 自定义创建参数
     * 创建流程时，如果processId  和 processName参数都有，则优先使用processId来启动
     *
     * @param variable  创建工作流实例参数
     * @return     工作流实例信息
     */
    Wind  buildByVariable(BuildVariable variable);


    /**
     * 提交操作
     * 提交操作，主要根据当前用户以及流程实例，将流程下针对当前用户的任务完成，保存历史履历，然后根据具体情况提交到下一个任务节点，
     * 创建新的任务。添加新的任务执行履历，如果没有用户，则默认使用系统用户。将流程实例信息返回。
     *
     * @param orderId          流程实例ID
     * @param approveUsers    提交后续审批人
     * @return                  工作流实例信息
     */
    Wind  submit(String orderId, List<ApproveUser> approveUsers);


    /**
     * 提交操作
     *
     * @param orderId       流程实例ID
     * @param approveUsers 审批人
     * @param user         当前用户
     * @return           工作流实例信息
     */
    Wind  submit(String orderId, List<ApproveUser> approveUsers, WindUser user);


    /**
     * 提交操作   推荐使用
     * @param variable  提交所需参数
     * @return       工作流实例信息
     */
    Wind  submit(SubmitVariable variable);



    /**
     * 退回操作  通过流程实例ID 查询当前执行的任务，退回到指定的任务节点，如果没有指定，则退回到上个节点，更新历史，新建退回任务节点任务以及历史信息
     *
     *
     * @param orderId  流程实例号
     * @return          工作流实例信息
     */
    Wind  reject(String orderId);


    /**
     * 退回操作
     *
     * @param orderId   流程实例号
     * @param user       当前用户
     * @return           工作流实例信息
     */
    Wind   reject(String orderId,WindUser user);


    /**
     * 退回操作   推荐使用
     *
     * @param variable   退回操作参数
     * @return         工作流实例信息
     */
    Wind   reject(RejectVariable variable);


    /**
     * 完结操作
     * 主要逻辑，完成当前流程，情况所有和当前流程相关的任务审批人信息，包含流程实例本身，添加历史履历。
     *
     * @param orderId  流程实例ID
     * @return         工作流实例信息
     */
    Wind   complete(String orderId);


    /**
     * 完结操作
     *
     * @param orderId   流程实例ID
     * @param user     当前用户
     * @return         工作流实例信息
     */
    Wind   complete(String orderId,WindUser  user);


    /**
     * 完结操作
     *
     * @param variable  完结操作参数
     * @return          工作流实例信息
     */
    Wind   complete(CompleteVariable variable);


    /**
     * 撤回操作
     * 主要逻辑，将当前流程实例相关信息全部清除，包含流程实例历史，添加该流程历史履历。流程实例历史和历史履历不同。
     *
     * @param orderId   流程实例号
     * @return        工作流实例信息
     */
    Wind   withDraw(String orderId);


    /**
     * 撤回操作
     *
     * @param orderId   流程实例ID
     * @param user      当前用户
     * @return          工作流实例信息
     */
    Wind   withDraw(String  orderId,WindUser user);


    /**
     * 撤回操作
     *
     * @param variable  撤回操作参数
     * @return      工作流实例信息
     */
    Wind   withDraw(WithDrawVariable variable);


    /**
     * 撤销操作
     * 功能： 撤销到上次操作的状态，流程数据回滚到上次数据状态
     * 主要逻辑：  根据撤销ID 查询需要撤销的数据，将撤销节点作为上个节点保存在工作流实例信息中，删除当前流程执行的所有数据。
     * 将撤销的数据保存到流程中。
     *
     * @param businessId   业务数据ID
     * @return        工作流实例信息
     */
    Wind   revoke(String businessId);


    /**
     * 撤销操作
     *
     * @param businessId  业务数据ID
     * @param histId     撤销ID
     * @param user    当前用户
     * @return        工作流实例信息
     */
    Wind   revoke(String businessId,String histId,WindUser user);


    /**
     * 撤销操作
     *
     * @param variable   撤销参数
     * @return            工作流实例信息
     */
    Wind   revoke(RevokeVariable variable);

}
