package com.bcx.wind.workflow.imp.cmd.repositorycmd.processconfigcmd;

import com.bcx.wind.workflow.entity.WindProcessConfig;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.pojo.Condition;
import com.bcx.wind.workflow.support.ObjectHelper;

import java.util.List;
import java.util.Map;

import static com.bcx.wind.workflow.message.ErrorCode.DATABASE_OPERATE_ERROR;
import static com.bcx.wind.workflow.message.ErrorCode.OPERATE_FAIL;
import static com.bcx.wind.workflow.message.ErrorCode.QUERY_EMPTY;

/**
 * 添加匹配规则配置命令
 *
 * @author zhanglei
 */
public class AddConditionConfigCommand implements Command<Integer> {

    /**
     * 配置ID
     */
    private String configId;

    /**
     * 匹配规则
     */
    private List<Condition> conditions;

    /**
     * 需要更新的配置数据
     */
    private WindProcessConfig windProcessConfig;

    /**
     * 执行数据
     */
    private CommandContext commandContext;

    public AddConditionConfigCommand(String configId,List<Condition> conditions){
        this.configId = configId;
        this.conditions = conditions;
    }


    @Override
    public Integer executor(CommandContext context) {
        try{
            this.commandContext = context;
            return this.execute();
        }catch (Exception e){
            commandContext.log(e);
        }
        return null;
    }


    private Integer execute(){
        //校验参数
        checkArgs();
        //添加
        int ret = addCondition();
        //更新缓存
        this.commandContext.getConfiguration().addProcessConfigCache(this.windProcessConfig.getProcessId(),this.windProcessConfig);
        return ret;
    }

    /**
     * 添加节点配置操作
     * @return   添加结果
     */
    private int  addCondition(){
        for(Condition condition : conditions){
            this.windProcessConfig.addCondition(condition);
        }
        int ret = this.commandContext.access().updateProcessConfig(this.windProcessConfig);
        if(ret != 1){
            WindError.error(DATABASE_OPERATE_ERROR,null);
        }
        return ret;
    }

    /**
     * 校验参数
     */
    private  void checkArgs(){
        if(this.configId == null){
            WindError.error(OPERATE_FAIL,null,"processConfig","addCondition","configId");
        }
        this.windProcessConfig = this.commandContext.access().getProcessConfigById(configId);
        if(this.windProcessConfig == null){
            WindError.error(QUERY_EMPTY,null,"processConfig",configId);
        }

        if(ObjectHelper.isEmpty(this.conditions)){
            WindError.error(OPERATE_FAIL,null,"processConfig","addCondition","businessConfig");
        }
    }
}
