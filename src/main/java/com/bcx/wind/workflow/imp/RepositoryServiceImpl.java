package com.bcx.wind.workflow.imp;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.entity.WindProcessConfig;
import com.bcx.wind.workflow.entity.WindProcessModule;
import com.bcx.wind.workflow.imp.cmd.repositorycmd.processconfigcmd.*;
import com.bcx.wind.workflow.imp.cmd.repositorycmd.processmodule.AddProcessModuleCommand;
import com.bcx.wind.workflow.imp.cmd.repositorycmd.processmodule.DeleteProcessModuleCommand;
import com.bcx.wind.workflow.imp.cmd.repositorycmd.processmodule.SelectWindProcessModuleCommand;
import com.bcx.wind.workflow.imp.cmd.repositorycmd.processmodule.UpdateProcessModuleCommand;
import com.bcx.wind.workflow.imp.cmd.repositorycmd.windprocesscmd.*;
import com.bcx.wind.workflow.imp.service.RepositoryService;
import com.bcx.wind.workflow.pojo.Condition;
import com.bcx.wind.workflow.pojo.ProcessModule;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 静态资源管理实现
 *
 * @author zhanglei
 */
public class RepositoryServiceImpl extends ServiceImpl  implements RepositoryService {

    @Override
    public WindProcess deploy(String processXml,String system) {
        return commandExecutor.executor(new AddProcessCommand(processXml,null));
    }

    @Override
    public WindProcess deploy(InputStream stream,String system) {
        return commandExecutor.executor(new AddProcessCommand(stream,system));
    }

    @Override
    public WindProcess queryById(String processId) {
        return commandExecutor.executor(new QueryProcessOneCommand(processId));
    }

    @Override
    public WindProcess queryMaxVersion(String processName) {
        return commandExecutor.executor(new QueryMaxVersionCommand(processName));
    }

    @Override
    public List<WindProcess> queryProcess(QueryFilter queryFilter) {
        return commandExecutor.executor(new QueryProcessCommand(queryFilter,null));
    }

    @Override
    public List<WindProcess> queryProcessPage(QueryFilter queryFilter, WindPage<WindProcess> page) {
        return commandExecutor.executor(new QueryProcessCommand(queryFilter,page));
    }

    @Override
    public int removeByProcessId(String processId) {
        return this.commandExecutor.executor(new RemoveProcessCommand(processId));
    }

    @Override
    public int release(String processId) {
        return this.commandExecutor.executor(new ReleaseProcessCommand(processId));
    }

    @Override
    public int recovery(String processId) {
        return this.commandExecutor.executor(new RecoveryProcessCommand(processId));
    }

    @Override
    public int addProcessModule(WindProcessModule windProcessModule) {
        return this.commandExecutor.executor(new AddProcessModuleCommand(windProcessModule));
    }

    @Override
    public int updateProcessModule(WindProcessModule windProcessModule) {
        return this.commandExecutor.executor(new UpdateProcessModuleCommand(windProcessModule));
    }

    @Override
    public int deleteProcessModule(String id) {
        return this.commandExecutor.executor(new DeleteProcessModuleCommand(id));
    }

    @Override
    public ProcessModule selectWindProcessModule(String system) {
        return this.commandExecutor.executor(new SelectWindProcessModuleCommand(system,false));
    }

    @Override
    public ProcessModule selectWindProcessModule() {
        return this.commandExecutor.executor(new SelectWindProcessModuleCommand(null,false));
    }

    @Override
    public ProcessModule selectDataWindProcessModule(String system) {
        return this.commandExecutor.executor(new SelectWindProcessModuleCommand(system,true));
    }

    @Override
    public ProcessModule selectDataWindProcessModule() {
        return this.commandExecutor.executor(new SelectWindProcessModuleCommand(null,true));
    }

    @Override
    public int addProcessConfig(WindProcessConfig config) {
        return this.commandExecutor.executor(new UpdateProcessConfigCommand(config,1));
    }

    @Override
    public int updateProcessConfig(WindProcessConfig config) {
        return this.commandExecutor.executor(new UpdateProcessConfigCommand(config,2));
    }

    @Override
    public List<WindProcessConfig> queryProcessConfig(QueryFilter filter) {
        return this.commandExecutor.executor(new QueryProcessConfigCommand(filter));
    }

    @Override
    public int removeProcessConfigById(String configId) {
        return this.commandExecutor.executor(new RemoveProcessConfigByIdCommand(configId));
    }

    @Override
    public int removeProcessConfigByProcessId(String processId) {
        return this.commandExecutor.executor(new RemoveProcessConfigByProcessIdCommand(processId));
    }

    @Override
    public int addNodeConfig(String configId, String key, Object value) {
        return this.commandExecutor.executor(new AddNodeConfigCommand(configId,Collections.singletonMap(key,value)));
    }

    @Override
    public int addNodeConfig(String configId, Map<String, Object> nodeConfigMap) {
        return this.commandExecutor.executor(new AddNodeConfigCommand(configId,nodeConfigMap));
    }

    @Override
    public int removeNodeConfig(String configId, String key) {
        return this.commandExecutor.executor(new RemoveNodeConfigCommand(configId,Collections.singletonList(key)));
    }

    @Override
    public int removeNodeConfig(String configId, List<String> keys) {
        return this.commandExecutor.executor(new RemoveNodeConfigCommand(configId,keys));
    }

    @Override
    public int addBusinessConfig(String configId, String key, String value) {
        return this.commandExecutor.executor(new AddBusinessConfigCommand(configId,Collections.singletonMap(key,value)));
    }

    @Override
    public int addBusinessConfig(String configId, Map<String, Object> businessConfigMap) {
        return this.commandExecutor.executor(new AddBusinessConfigCommand(configId,businessConfigMap));
    }

    @Override
    public int removeBusinessConfig(String configId, String key) {
        return this.commandExecutor.executor(new RemoveBusinessConfigCommand(configId,Collections.singletonList(key)));
    }

    @Override
    public int removeBusinessConfig(String configId, List<String> keys) {
        return this.commandExecutor.executor(new RemoveBusinessConfigCommand(configId,keys));
    }

    @Override
    public int addConditionConfig(String configId, Condition condition) {
        return this.commandExecutor.executor(new AddConditionConfigCommand(configId,Collections.singletonList(condition)));
    }

    @Override
    public int addConditionConfig(String configId, List<Condition> conditions) {
        return this.commandExecutor.executor(new AddConditionConfigCommand(configId,conditions));
    }

    @Override
    public int removeConditionConfig(String configId, String key) {
        return this.commandExecutor.executor(new RemoveConditionConfigCommand(configId,Collections.singletonList(key)));
    }

    @Override
    public int removeConditionConfig(String configId, List<String> keys) {
        return this.commandExecutor.executor(new RemoveConditionConfigCommand(configId,keys));
    }
}
