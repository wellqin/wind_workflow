package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.entity.WindHistOrder;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.Assert;

import static com.bcx.wind.workflow.message.ErrorCode.CREATE_HIST_ORDER_FAIL;
import static com.bcx.wind.workflow.message.ErrorCode.DATABASE_OPERATE_ERROR;

/**
 * 创建流程历史实例命令
 *
 * @author zhanglei
 */
public class CreateHistOrderCommand implements Command<Void> {

    /**
     * 流程历史实例
     */
    private WindHistOrder windHistOrder;

    public CreateHistOrderCommand(WindHistOrder windHistOrder){
        this.windHistOrder = windHistOrder;
    }

    @Override
    public Void executor(CommandContext context) {
        //校验
        checkArgs();
        if(context.access().insertWindHistOrder(this.windHistOrder) != 1){
            WindError.error(DATABASE_OPERATE_ERROR,null);
        }
        return null;
    }

    private void checkArgs(){
        if(this.windHistOrder == null){
            WindError.error(CREATE_HIST_ORDER_FAIL,null,"windHistOrder");
        }
        Assert.notEmptyError(CREATE_HIST_ORDER_FAIL,this.windHistOrder.getId(),"id");
        Assert.notEmptyError(CREATE_HIST_ORDER_FAIL,this.windHistOrder.getProcessId(),"processId");
        Assert.notEmptyError(CREATE_HIST_ORDER_FAIL,this.windHistOrder.getStatus(),"status");
        Assert.notEmptyError(CREATE_HIST_ORDER_FAIL,this.windHistOrder.getCreateUser(),"createUser");
        Assert.notEmptyError(CREATE_HIST_ORDER_FAIL,this.windHistOrder.getCreateTime(),"createTime");
        Assert.notEmptyError(CREATE_HIST_ORDER_FAIL,this.windHistOrder.getBusinessId(),"businessId");
    }
}
