package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;
import com.bcx.wind.workflow.support.Assert;

import static com.bcx.wind.workflow.core.constant.Constant.JSON_NULL;
import static com.bcx.wind.workflow.message.ErrorCode.CREATE_TASK_FAIL;
import static com.bcx.wind.workflow.message.ErrorCode.UPDATE_TASK_FAIL;

/**
 * 更新任务命令
 *
 * @author zhanglei
 */
public class UpdateTaskCommand implements Command<Void> {

    /**
     * 需要更新的任务
     */
    private WindTask windTask;

    public UpdateTaskCommand(WindTask windTask){
        this.windTask = windTask;
    }

    @Override
    public Void executor(CommandContext context) {
        //校验
        checkData();
        //更新
        context.access().updateTaskInstance(this.windTask);
        return null;
    }



    /**
     * 校验是否为空
     */
    private void checkData(){
        if(windTask == null){
            WindError.error(CREATE_TASK_FAIL,null,"windTask");
        }

        Assert.notEmptyError(UPDATE_TASK_FAIL,windTask.getId(),"taskId");
        Assert.notEmptyError(UPDATE_TASK_FAIL,windTask.getTaskName(),"taskName");
        Assert.notEmptyError(UPDATE_TASK_FAIL,windTask.getTaskType(),"taskType");
        Assert.notEmptyError(UPDATE_TASK_FAIL,windTask.getStatus(),"status");
        Assert.notEmptyError(UPDATE_TASK_FAIL,windTask.getOrderId(),"orderId");
        Assert.notEmptyError(UPDATE_TASK_FAIL,windTask.getProcessId(),"processId");

        this.windTask.setApproveCount(0);
        if(this.windTask.getVariable()==null){
            this.windTask.setVariable(JSON_NULL);
        }
        this.windTask.setVersion(1);
    }
}
