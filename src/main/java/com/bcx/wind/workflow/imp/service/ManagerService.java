package com.bcx.wind.workflow.imp.service;

import com.bcx.wind.workflow.access.QueryFilter;
import com.bcx.wind.workflow.access.TaskFilter;
import com.bcx.wind.workflow.access.WindPage;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.entity.WindTaskInstance;
import com.bcx.wind.workflow.pojo.ProcessModule;
import com.bcx.wind.workflow.pojo.WindUser;

import java.util.List;

/**
 * 工作流管理监控服务接口
 *
 * 主要功能  过滤查询正在执行的流程以及任务，包含延期的流程任务等
 * 转办 关闭任务  挂起恢复流程  加签减签
 * 转办：分为流程转办，任务转办，流程定义下转办，模块转办，所有转办
 * 关闭任务：只针对于任务关闭
 * 挂起恢复，只需改变流程实例状态即可
 * 加签减签，分为普通的加签  ， 会签加签，以及串签加签。
 *
 * 以及任务实例，审批人，流程实例查询功能
 *
 * @author zhanglei
 */
public interface ManagerService {

    /**
     * 设置当前国际化语言
     *
     * @param lang  语言 wind_msg/zh.properties  文件前缀   如 "zh"
     * @return   设置是否成功  1成功
     */
    int   language(String lang);


    /**
     * 通过指定系统模块以及用户 查询所有流程模块以及流程定义，以及流程定义下属于用户userId的任务实例数量
     *
     * 俗称查询某用户的待办事宜
     *
     * @param userId   用户ID
     * @param system   系统标识
     * @return         流程定义模块对象
     */
    ProcessModule  queryToDoProcess(String userId,String system);


    /**
     * 通过指定用户 查询所有流程模块以及流程定义，以及流程定义下属于用户userId的任务实例数量
     *
     * 俗称查询某用户的待办事宜
     *
     * @param userId  用户ID
     * @return   流程定义模块对象
     */
    ProcessModule  queryToDoProcess(String userId);


    /**
     * 过滤查询任务实例具体信息，包含任务所属流程定义，以及所属流程实例，任务审批人相关信息
     *
     * @param filter  过滤条件
     * @param page  分页条件
     * @return     任务实例信息
     */
    List<WindTaskInstance>  queryTaskInstance(TaskFilter filter,WindPage<WindTaskInstance> page);



    /**
     * 过滤查询流程实例数据
     *
     * @param filter  过滤条件
     * @return       查询结果
     */
    List<WindOrder>  queryOrder(QueryFilter filter);


    /**
     * 过滤分页查询流程实例
     *
     * @param filter     过滤条件
     * @param windPage   分页
     * @return           分页后查询结果，分页最终结果在windPage中
     */
    List<WindOrder>   queryOrder(QueryFilter filter, WindPage<WindOrder> windPage);


    /**
     * 过滤查询任务实例
     *
     * @param filter 过滤条件
     * @return  查询结果
     */
    List<WindTask>  queryTask(QueryFilter filter);


    /**
     * 分页过滤查询任务实例
     *
     * @param filter  过滤条件
     * @param windPage  分页条件
     * @return       查询结果
     */
    List<WindTask>  queryTask(QueryFilter filter,WindPage<WindTask> windPage);


    /**
     * 指定任务实例进行转办  任务实例存在唯一性，故返回单值
     *
     * 转办操作，为了方便保存转办操作的历史记录，以及后期可维护，故重新创建一个新的任务
     *
     * @param taskIds   指定的任务实例ID
     * @param oldUserId  任务原审批人
     * @param newUser   转办审批人
     * @return  转办产生的新任务
     */
    List<WindTask>  transferByTaskId(List<String> taskIds,String oldUserId,WindUser newUser,WindUser windUser);


    /**
     * 指定流程实例下的任务进行转办   流程实例下一个人可能存在多个任务，如果将这个人的多个任务全部转办给另一个人，则会产生多个新的任务
     *
     * @param orderIds    流程实例ID
     * @param oldUserId   任务原审批人
     * @param newUser   转办审批人
     * @return        转办产生的新任务集合
     */
    List<WindTask>  transferByOrderId(List<String> orderIds,String oldUserId,WindUser newUser,WindUser windUser);


    /**
     * 指定流程定义ID 进行转办，将该流程定义下所有的流程实例下，指定人的所有任务转办给另一个人
     *
     * @param processIds      流程定义ID
     * @param oldUserId     原审批人
     * @param newUser     新审批人
     * @return            转办产生的新任务集合
     */
    List<WindTask>  transferByProcessId(List<String> processIds,String oldUserId,WindUser newUser,WindUser windUser);


    /**
     * 指定流程定义名称，将该流程名下所有版本的流程定义下，所有流程实例下，指定人的所有任务转办给另一个人
     *
     * @param processNames  流程名称
     * @param oldUserId    原审批人
     * @param newUser    新审批人
     * @return           转办新任务集合
     */
    List<WindTask>  transferByProcessName(List<String> processNames,String oldUserId,WindUser newUser,WindUser windUser);




    /**
     * 指定流程定义模块下 所有流程定义下，所有流程实例下，指定人的所有任务转办给另一个人
     *
     * @param modules      流程定义模块
     * @param oldUserId    原审批人
     * @param newUser    新审批人
     * @return           转办新任务集合
     */
    List<WindTask>  transferByProcessModule(List<String> modules,String oldUserId,WindUser newUser,WindUser windUser);


    /**
     * 指定系统模块下，所有流程定义模块下 所有流程定义下，所有流程实例下，指定人的所有任务转办给另一个人
     *
     * @param systems       系统标识
     * @param oldUserId    原审批人
     * @param newUser   新审批人
     * @return             转办新任务集合
     */
    List<WindTask>  transferBySystem(List<String> systems,String oldUserId,WindUser newUser,WindUser windUser);


    /**
     * 通过任务实例ID  关闭任务
     *
     * @param taskId  任务ID
     * @param windUser  操作人
     * @return    关闭结果
     */
    int  closeTask(String taskId,WindUser windUser);


    /**
     * 通过流程定义ID  挂起该流程实例
     *
     * 挂起流程，将流程实例状态 status 更改为stop，包含该流程实例下的所有任务实例也将状态更新为stop，
     * 引擎执行流程时，只会筛选出status为run的流程实例进行执行
     *
     * @param orderId   流程实例ID
     * @return          ret
     */
    int  hangUpOrderByOrderId(String orderId);


    /**
     * 通过业务ID  挂起该业务ID对应的流程实例
     *
     * @param businessId   业务ID
     * @return    ret
     */
    int  hangUpOrderByBusinessId(String businessId);


    /**
     * 恢复流程实例   通过流程实例ID
     *
     * @param orderId  流程实例ID
     * @return   ret
     */
    int  recoveryOrderByOrderId(String orderId);


    /**
     * 恢复流程实例，通过业务ID
     *
     * @param businessId   业务ID
     * @return  ret
     */
    int recoveryOrderByBusinessId(String businessId);


    /**
     * 指定任务实例，对任务添加审批人，可添加多个审批人
     * 如果是普通任务，则返回当前任务数据
     * 如果是会签任务，则根据添加的审批人数量来新增任务数据  更新流程实例执行审批数据, 添加新的执行历史履历
     * 如果是串签，则返回当前任务，更新流程执行中的审批数据
     *
     * @param taskId           任务ID
     * @param windUsers        审批人数据
     * @param user           当前用户
     * @return              加签后产生的新的任务数据集合
     */
    List<WindTask>  addApproveUsers(String taskId , List<WindUser> windUsers,WindUser user);


    /**
     * 指定任务  除去任务下某些审批人
     *
     * @param taskId     任务ID
     * @param userIds   审批人ID
     * @param windUser  当前操作人
     * @return          除去审批人关联的任务
     */
    WindTask  reduceApproveUser(String taskId , List<String> userIds,WindUser windUser);


    /**
     * 根据主任务，添加子任务，一个子任务对应一个任务操作人
     *
     * @param taskId  主任务ID
     * @param users   子任务操作人集合
     * @return        创建的子任务集合
     */
    List<WindTask>  addChildTask(String taskId,List<WindUser>  users);


    /**
     * 根据主任务，添加子任务，一个子任务对应一个任务操作人
     *
     * @param taskId    主任务ID
     * @param users    子任务操作人集合
     * @param user     当前操作用户
     * @return        添加的子任务集合
     */
    List<WindTask>  addChildTask(String taskId,List<WindUser> users,WindUser user);


    /**
     * 根据任务ID  完成子任务  由于子任务无关流程的走向，完成子任务只需要判断完成人是否是操作人即可
     * 如果是操作人，则直接删除该条任务就可以了
     *
     * @param taskId    子任务ID
     * @param user      当前操作人
     * @return    操作结果
     */
    int  completeChildTask(String taskId,WindUser user,String suggest);
}




















