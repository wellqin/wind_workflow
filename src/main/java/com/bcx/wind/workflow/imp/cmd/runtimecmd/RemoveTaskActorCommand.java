package com.bcx.wind.workflow.imp.cmd.runtimecmd;

import com.bcx.wind.workflow.entity.WindTask;
import com.bcx.wind.workflow.errorcontext.WindError;
import com.bcx.wind.workflow.interceptor.Command;
import com.bcx.wind.workflow.interceptor.CommandContext;

import java.util.List;

import static com.bcx.wind.workflow.message.ErrorCode.OPERATE_FAIL;
import static com.bcx.wind.workflow.message.ErrorCode.QUERY_EMPTY;

/**
 * 删除任务审批人命令
 *
 * @author zhanglei
 */
public class RemoveTaskActorCommand implements Command<Void> {

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 审批人ID
     */
    private List<String> actorIds;

    /**
     * 执行数据
     */
    private CommandContext context;

    public RemoveTaskActorCommand(String taskId,List<String> actorIds){
        this.taskId = taskId;
        this.actorIds = actorIds;
    }

    @Override
    public Void executor(CommandContext context) {
        this.context = context;
        if(this.actorIds == null){
            return null;
        }
        //校验
        checkArgs();
        //删除审批人
        removeActor();
        return null;
    }

    private void removeActor(){
        for(String actorId : actorIds) {
            context.access().removeTaskActor(this.taskId,actorId);
        }
    }

    private void checkArgs(){
        if(taskId == null){
            WindError.error(OPERATE_FAIL,null,"task","addActor","taskId");
        }
        WindTask task = context.access().getTaskInstanceById(this.taskId);
        if(task == null){
            WindError.error(QUERY_EMPTY,null,"task",taskId);
        }
    }
}
