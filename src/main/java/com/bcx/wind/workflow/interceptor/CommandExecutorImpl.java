package com.bcx.wind.workflow.interceptor;

/**
 * 命令执行器实现
 *
 * @author zhanglei
 */
public class CommandExecutorImpl implements CommandExecutor {

    /**
     * 第一个拦截器
     */
    private CommandInterceptor first;

    public CommandExecutorImpl(CommandInterceptor interceptor){
        this.first = interceptor;
    }

    @Override
    public <T> T executor(Command<T> command) {
        return this.first.executor(command);
    }
}
