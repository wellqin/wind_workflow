package com.bcx.wind.workflow.interceptor;

import com.bcx.wind.workflow.WindEngine;
import com.bcx.wind.workflow.access.Access;
import com.bcx.wind.workflow.imp.Configuration;
import com.bcx.wind.workflow.imp.service.HistoryService;
import com.bcx.wind.workflow.imp.service.ManagerService;
import com.bcx.wind.workflow.imp.service.RepositoryService;
import com.bcx.wind.workflow.imp.service.RuntimeService;
import com.bcx.wind.workflow.pojo.Wind;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 命令所需数据
 *
 * @author zhanglei
 */
public class CommandContext {

    private static Logger logger = LoggerFactory.getLogger(CommandContext.class);

    /**
     * 配置数据
     */
    private Configuration configuration;

    /**
     * 工作流实例数据
     */
    private ThreadLocal<Wind>  windLocal = new ThreadLocal<>();

    public CommandContext(Configuration configuration){
        this.configuration = configuration;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public CommandContext setConfiguration(Configuration configuration) {
        this.configuration = configuration;
        return this;
    }

    public void log(Exception e){
        logger.error(e.getMessage(),e);
    }

    public RepositoryService repositoryService(){
        return configuration.getRepositoryService();
    }

    public RuntimeService runtimeService(){
        return configuration.getRuntimeService();
    }

    public HistoryService historyService(){
        return configuration.getHistoryService();
    }

    public ManagerService managerService(){
        return configuration.getManagerService();
    }

    public WindEngine   windEngine(){
        return configuration.getWindEngine();
    }

    public Access   access(){
        return configuration.getAccess();
    }

    public  void  setWind(Wind wind){
        this.windLocal.set(wind);
    }

    public  Wind  getWind(){
        return this.windLocal.get();
    }

    public void removeWind(){
        this.windLocal.remove();
    }

}
