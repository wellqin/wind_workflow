package com.bcx.wind.workflow.interceptor;

import com.bcx.wind.workflow.pojo.Task;

/**
 * 工作流本地线程数据容器
 *
 * @author zhanglei
 */
public class Context {

    /**
     * 当前线程执行数据
     */
    private static ThreadLocal<CommandContext>  localCommand = new ThreadLocal<>();

    /**
     * 当前线程任务
     */
    private static ThreadLocal<Task>  localTask = new ThreadLocal<>();

    private Context(){}

    public static void set(CommandContext commandContext,Task task){
        Context.localCommand.set(commandContext);
        Context.localTask.set(task);
    }

    public static void setLocalCommand(CommandContext commandContext){
        Context.localCommand.set(commandContext);
    }

    public static void setLocalTask(Task task){
        Context.localTask.set(task);
    }

    public static CommandContext getCommandContext(){
        return Context.localCommand.get();
    }

    public static Task getTask(){
        return Context.localTask.get();
    }

    public static void remove(){
        Context.localTask.remove();
        Context.localCommand.remove();
    }
}
