package com.bcx.wind.workflow.interceptor;

/**
 * 工作流责任链拦截器
 *
 * @author zhanglei
 */
public interface CommandInterceptor {

    /**
     * 拦截器执行
     *
     * @param command  命令
     * @param <T>      返回类型
     * @return         返回值
     */
    <T>T   executor(Command<T> command);

    /**
     * 获取下个拦截器
     *
     * @return  拦截器
     */
    CommandInterceptor  getNext();


    /**
     * 设置下一个拦截器
     *
     * @param next  下一个拦截器
     */
    void  setNext(CommandInterceptor next);

}
