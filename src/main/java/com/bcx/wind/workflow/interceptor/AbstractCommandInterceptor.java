package com.bcx.wind.workflow.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 基础实现
 *
 * @author zhanglei
 */
public abstract class AbstractCommandInterceptor implements CommandInterceptor{

    protected Logger logger = LoggerFactory.getLogger(AbstractCommandInterceptor.class);

    /**
     * 下一个拦截器
     */
    private CommandInterceptor next;

    @Override
    public CommandInterceptor getNext() {
        return this.next;
    }

    @Override
    public void setNext(CommandInterceptor next) {
        this.next = next;
    }
}
