package com.bcx.wind.workflow.interceptor;

/**
 * 命令接口
 * @param <T>  类型
 *
 * @author zhanglei
 */
public interface Command<T> {

    /**
     * 执行命令
     *
     * @param context  命令执行数据
     * @return          返回数据
     */
    T  executor(CommandContext context);

}
