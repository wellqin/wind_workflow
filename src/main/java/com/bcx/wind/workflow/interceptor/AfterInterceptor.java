package com.bcx.wind.workflow.interceptor;

import com.bcx.wind.workflow.pojo.Wind;

/**
 * 工作流自定义后置拦截器
 *
 * @author zhanglei
 */
public interface AfterInterceptor {


    /**
     * 后置拦截器
     *
     * @param wind              工作流实例信息数据
     * @param commandContext   工作流执行数据 包含全局配置
     */
    void   invoke(Wind wind,CommandContext commandContext);
}
