package com.bcx.wind.workflow.interceptor;

/**
 * 工作流命令执行器
 *
 * @author zhanglei
 */
public interface CommandExecutor {


    /**
     * 命令执行方法
     *
     * @param command 命令
     * @param <T>     返回类型
     * @return         返回值
     */
    <T>T  executor(Command<T> command);

}
