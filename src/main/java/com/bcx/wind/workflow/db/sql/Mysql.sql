/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : zflow

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2019-03-13 12:30:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wind_act_hist
-- ----------------------------

CREATE TABLE IF NOT EXISTS `wind_act_hist` (
  `id` char(64) NOT NULL,
  `task_id` char(64) NOT NULL,
  `task_name` char(64) NOT NULL,
  `task_display_name` char(255) DEFAULT NULL,
  `process_id` char(64) NOT NULL,
  `order_id` char(64) NOT NULL,
  `process_name` char(64) NOT NULL,
  `process_display_name` char(255) DEFAULT NULL,
  `operate` char(32) DEFAULT NULL,
  `suggest` longtext,
  `approve_time` timestamp NULL DEFAULT NULL,
  `actor_id` char(64) DEFAULT NULL,
  `actor_name` char(255) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `approve_user_variable` text,
  `task_type` char(32) DEFAULT NULL,
  `system` char(64) DEFAULT NULL,
  `submit_user_variable` text,
  `variable` text,
  `task_create_user` varchar(255),
  `task_level` varchar(16) not null,
  `task_parent_id` varchar(64),
  PRIMARY KEY (`id`),
  KEY `wind_act_hist_approve_time` (`approve_time`),
  KEY `wind_act_hist_order_id` (`order_id`),
  KEY `wind_act_hist_process_id` (`process_id`),
  KEY `wind_act_hist_task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for wind_end_hist
-- ----------------------------
CREATE TABLE  IF NOT EXISTS `wind_end_hist` (
  `id` char(64) NOT NULL,
  `task_id` char(64) NOT NULL,
  `task_name` char(64) NOT NULL,
  `task_display_name` char(255) DEFAULT NULL,
  `process_id` char(64) NOT NULL,
  `order_id` char(64) NOT NULL,
  `process_name` char(64) NOT NULL,
  `process_display_name` char(255) DEFAULT NULL,
  `operate` char(32) DEFAULT NULL,
  `suggest` longtext,
  `approve_time` timestamp NULL DEFAULT NULL,
  `actor_id` char(64) DEFAULT NULL,
  `actor_name` char(255) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `approve_user_variable` text,
  `task_type` char(32) DEFAULT NULL,
  `system` char(64) DEFAULT NULL,
  `submit_user_variable` text,
  `variable` text,
  `task_create_user` varchar(255),
  `task_level` varchar(16) not null,
  `task_parent_id` varchar(64),
  PRIMARY KEY (`id`),
  KEY `wind_end_hist_approve_time_copy` (`approve_time`),
  KEY `wind_end_hist_order_id_copy` (`order_id`),
  KEY `wind_end_hist_process_id_copy` (`process_id`),
  KEY `wind_end_hist_task_id_copy` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wind_hist_order
-- ----------------------------
CREATE TABLE  IF NOT EXISTS `wind_hist_order` (
  `id` char(64) NOT NULL,
  `process_id` char(64) NOT NULL,
  `status` char(32) NOT NULL,
  `create_user` char(64) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expire_time` timestamp NULL DEFAULT NULL,
  `parent_id` char(64) DEFAULT NULL,
  `version` smallint(16) NOT NULL,
  `variable` text,
  `data` longtext,
  `system` char(64) DEFAULT NULL,
  `finish_time` timestamp NULL DEFAULT NULL,
  `business_id` varchar(64) NOT NULL,
  `type` char(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wind_hist_order_create_time_copy` (`create_time`),
  KEY `wind_hist_order_create_user_copy` (`create_user`),
  KEY `wind_hist_order_business_id_copy` (`business_id`),
  KEY `wind_hist_order_process_id_copy` (`process_id`),
  KEY `wind_hist_order_id_copy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wind_order
-- ----------------------------
CREATE TABLE  IF NOT EXISTS `wind_order` (
  `id` char(64) NOT NULL,
  `process_id` char(64) NOT NULL,
  `status` char(32) NOT NULL,
  `create_user` char(64) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expire_time` timestamp NULL DEFAULT NULL,
  `parent_id` char(64) DEFAULT NULL,
  `version` smallint(16) NOT NULL,
  `variable` text,
  `data` longtext,
  `system` char(64) DEFAULT NULL,
  `finish_time` timestamp NULL DEFAULT NULL,
  `business_id` varchar(64) NOT NULL,
  `type` char(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wind_order_create_time` (`create_time`),
  KEY `wind_order_create_user` (`create_user`),
  KEY `wind_order_business_id` (`business_id`),
  KEY `wind_order_process_id` (`process_id`),
  KEY `wind_order_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wind_process_config
-- ----------------------------
CREATE TABLE  IF NOT EXISTS `wind_process_config` (
  `id` char(64) NOT NULL,
  `process_id` char(64) NOT NULL,
  `config_name` char(255) DEFAULT NULL,
  `process_name` char(255) NOT NULL,
  `node_id` char(64) NOT NULL,
  `condition` longtext,
  `node_config` text,
  `approve_user` text,
  `business_config` longtext,
  `sort` smallint(16) NOT NULL,
  `level` smallint(16) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logic` char(32) DEFAULT NULL,
  `script` text NULL,
  PRIMARY KEY (`id`),
  KEY `config_create_time` (`create_time`),
  KEY `config_process_id` (`process_id`),
  KEY `config_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wind_process_definition
-- ----------------------------
CREATE TABLE  IF NOT EXISTS `wind_process_definition` (
  `id` char(64) NOT NULL,
  `process_name` char(255) NOT NULL,
  `display_name` char(255) DEFAULT NULL,
  `status` char(32) NOT NULL,
  `version` smallint(16) NOT NULL,
  `parent_id` char(64) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `content` longblob NOT NULL,
  `system` char(64) DEFAULT NULL,
  `module` char(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `process_create_time` (`create_time`),
  KEY `process_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wind_revoke_data
-- ----------------------------
CREATE TABLE  IF NOT EXISTS `wind_revoke_data` (
  `id` char(64) NOT NULL,
  `order_id` char(64) NOT NULL,
  `process_id` char(64) NOT NULL,
  `process_display_name` char(255) DEFAULT NULL,
  `task_name` char(64) NOT NULL,
  `task_display_name` char(255) DEFAULT NULL,
  `actor_id` char(64) NOT NULL,
  `operate` char(32) NOT NULL,
  `suggest` longtext,
  `operate_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `wind_order` text NOT NULL,
  `wind_task` text NOT NULL,
  `wind_task_actor` text NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `wind_revoke_data_order_id_fkey` (`order_id`),
  KEY `wind_revoke_data_process_id_fkey` (`process_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wind_task
-- ----------------------------
CREATE TABLE  IF NOT EXISTS `wind_task` (
  `id` char(64) NOT NULL,
  `task_name` char(64) NOT NULL,
  `display_name` char(255) DEFAULT NULL,
  `task_type` char(32) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expire_time` timestamp NULL DEFAULT NULL,
  `approve_count` smallint(16) NOT NULL,
  `status` char(32) NOT NULL,
  `order_id` char(64) NOT NULL,
  `process_id` char(64) NOT NULL,
  `variable` text,
  `parent_id` char(64) DEFAULT NULL,
  `version` smallint(16) NOT NULL,
  `position` longtext,
  `create_user` varchar(255),
  `task_level` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wind_task_create_time` (`create_time`),
  KEY `wind_task_order_id` (`order_id`),
  KEY `wind_task_process_id` (`process_id`),
  KEY `wind_task_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `wind_process_module` (
  `id` varchar(64)   NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `parent_id` varchar(64),
  `system` varchar(64),
  PRIMARY KEY (`id`)
);


-- ----------------------------
-- Table structure for wind_task_actor
-- ----------------------------
CREATE TABLE  IF NOT EXISTS `wind_task_actor` (
  `task_id` char(64) NOT NULL,
  `actor_id` char(64) NOT NULL,
  KEY `wind_task_actor_task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
