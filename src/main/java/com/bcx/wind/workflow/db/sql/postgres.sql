/*
Navicat PGSQL Data Transfer

Source Server         : localhost
Source Server Version : 100100
Source Host           : localhost:5432
Source Database       : wind_workflow
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 100100
File Encoding         : 65001

Date: 2019-03-08 09:19:47
*/


-- ----------------------------
-- Table structure for wind_act_hist
-- ----------------------------

CREATE TABLE IF NOT EXISTS "public"."wind_act_hist" (
"id" varchar(64) COLLATE "default" NOT NULL primary  key ,
"task_id" varchar(64) COLLATE "default" NOT NULL,
"task_name" varchar(64) COLLATE "default" NOT NULL,
"task_display_name" varchar(255) COLLATE "default",
"process_id" varchar(64) COLLATE "default" NOT NULL,
"order_id" varchar(64) COLLATE "default" NOT NULL,
"process_name" varchar(64) COLLATE "default" NOT NULL,
"process_display_name" varchar(255) COLLATE "default",
"operate" varchar(32) COLLATE "default",
"suggest" varchar(1024) COLLATE "default",
"approve_time" timestamp(6),
"actor_id" varchar(64) COLLATE "default",
"actor_name" varchar(255) COLLATE "default",
"create_time" timestamp(6),
"approve_user_variable" text COLLATE "default",
"task_type" varchar(32) COLLATE "default",
"system" varchar(64) COLLATE "default",
"submit_user_variable" text COLLATE "default",
"variable" text COLLATE "default",
"task_create_user" varchar(255),
"task_level" varchar(16) COLLATE "default" NOT NULL,
"task_parent_id" varchar(64)
)
WITH (OIDS=FALSE)


;

-- ----------------------------
-- Table structure for wind_end_hist
-- ----------------------------
CREATE TABLE IF NOT EXISTS "public"."wind_end_hist" (
"id" varchar(64) COLLATE "default" NOT NULL primary  key ,
"task_id" varchar(64) COLLATE "default" NOT NULL,
"task_name" varchar(64) COLLATE "default" NOT NULL,
"task_display_name" varchar(255) COLLATE "default",
"process_id" varchar(64) COLLATE "default" NOT NULL,
"order_id" varchar(64) COLLATE "default" NOT NULL,
"process_name" varchar(64) COLLATE "default" NOT NULL,
"process_display_name" varchar(255) COLLATE "default",
"operate" varchar(32) COLLATE "default",
"suggest" varchar(1024) COLLATE "default",
"approve_time" timestamp(6),
"actor_id" varchar(64) COLLATE "default",
"actor_name" varchar(255) COLLATE "default",
"create_time" timestamp(6),
"approve_user_variable" text COLLATE "default",
"task_type" varchar(32) COLLATE "default",
"system" varchar(64) COLLATE "default",
"submit_user_variable" text COLLATE "default",
"variable" text COLLATE "default",
"task_create_user" varchar(255),
"task_level" varchar(16) COLLATE "default" NOT NULL,
"task_parent_id" varchar(64)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for wind_hist_order
-- ----------------------------
CREATE TABLE IF NOT EXISTS "public"."wind_hist_order" (
"id" varchar(64) COLLATE "default" NOT NULL primary  key ,
"process_id" varchar(64) COLLATE "default" NOT NULL,
"status" varchar(32) COLLATE "default" NOT NULL,
"create_user" varchar(64) COLLATE "default" NOT NULL,
"create_time" timestamp(6) NOT NULL,
"expire_time" timestamp(6),
"parent_id" varchar(64) COLLATE "default",
"version" int2 NOT NULL,
"variable" text COLLATE "default",
"data" varchar(4000) COLLATE "default",
"system" varchar(64) COLLATE "default",
"finish_time" timestamp(6),
"business_id" varchar(64) NOT NULL,
"type" varchar(32) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for wind_order
-- ----------------------------
CREATE TABLE IF NOT EXISTS "public"."wind_order" (
"id" varchar(64) COLLATE "default" NOT NULL primary key ,
"process_id" varchar(64) COLLATE "default" NOT NULL,
"status" varchar(32) COLLATE "default" NOT NULL,
"create_user" varchar(64) COLLATE "default" NOT NULL,
"create_time" timestamp(6) NOT NULL,
"expire_time" timestamp(6),
"parent_id" varchar(64) COLLATE "default",
"version" int2 NOT NULL,
"variable" text COLLATE "default",
"data" varchar(4000) COLLATE "default",
"system" varchar(64) COLLATE "default",
"finish_time" timestamp(6),
"business_id" varchar(64) NOT NULL,
"type" varchar(32) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for wind_process_config
-- ----------------------------
CREATE TABLE IF NOT EXISTS "public"."wind_process_config" (
"id" varchar(64) COLLATE "default" NOT NULL primary  key ,
"process_id" varchar(64) COLLATE "default" NOT NULL,
"config_name" varchar(255) COLLATE "default",
"process_name" varchar(255) COLLATE "default" NOT NULL,
"node_id" varchar(64) COLLATE "default" NOT NULL,
"condition" varchar(4000) COLLATE "default",
"node_config" text COLLATE "default",
"approve_user" text COLLATE "default",
"business_config" varchar(4000) COLLATE "default",
"sort" int2 NOT NULL,
"level" int2 NOT NULL,
"create_time" timestamp(6) NOT NULL,
"logic" varchar(32) COLLATE "default",
"script" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for wind_process_definition
-- ----------------------------
CREATE TABLE IF NOT EXISTS "public"."wind_process_definition" (
"id" varchar(64) COLLATE "default" NOT NULL primary  key ,
"process_name" varchar(255) COLLATE "default" NOT NULL,
"display_name" varchar(255) COLLATE "default",
"status" varchar(32) COLLATE "default" NOT NULL,
"version" int2 NOT NULL,
"parent_id" varchar(64) COLLATE "default",
"create_time" timestamp(6) NOT NULL,
"content" bytea NOT NULL,
"system" varchar(64) COLLATE "default",
"module" varchar(64) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for wind_revoke_data
-- ----------------------------
CREATE TABLE IF NOT EXISTS "public"."wind_revoke_data" (
"id" varchar(64) COLLATE "default" NOT NULL primary  key ,
"order_id" varchar(64) COLLATE "default" NOT NULL,
"process_id" varchar(64) COLLATE "default" NOT NULL,
"process_display_name" varchar(255) COLLATE "default",
"task_name" varchar(64) COLLATE "default" NOT NULL,
"task_display_name" varchar(255) COLLATE "default",
"actor_id" varchar(64) COLLATE "default" NOT NULL,
"operate" varchar(32) COLLATE "default" NOT NULL,
"suggest" varchar(1024) COLLATE "default",
"operate_time" timestamp(6) NOT NULL,
"wind_order" text COLLATE "default" NOT NULL,
"wind_task" text COLLATE "default" NOT NULL,
"wind_task_actor" text COLLATE "default" NOT NULL,
"create_time" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for wind_task
-- ----------------------------
CREATE TABLE IF NOT EXISTS "public"."wind_task" (
"id" varchar(64) COLLATE "default" NOT NULL primary  key ,
"task_name" varchar(64) COLLATE "default" NOT NULL,
"display_name" varchar(255) COLLATE "default",
"task_type" varchar(32) COLLATE "default" NOT NULL,
"create_time" timestamp(6) NOT NULL,
"expire_time" timestamp(6),
"approve_count" int2 NOT NULL,
"status" varchar(32) COLLATE "default" NOT NULL,
"order_id" varchar(64) COLLATE "default" NOT NULL,
"process_id" varchar(64) COLLATE "default" NOT NULL,
"variable" text COLLATE "default",
"parent_id" varchar(64) COLLATE "default",
"version" int2 NOT NULL,
"position" varchar(1024) COLLATE "default",
"create_user" varchar(255),
"task_level" varchar(32)  NOT NULL
)
WITH (OIDS=FALSE)

;

CREATE TABLE IF NOT EXISTS wind_process_module (
"id" varchar(64) COLLATE "default" NOT NULL primary  key ,
"module_name" varchar(255) COLLATE "default" NOT NULL,
"parent_id" varchar(64) COLLATE "default",
"system" varchar(64) COLLATE "default"
);

-- ----------------------------
-- Table structure for wind_task_actor
-- ----------------------------
CREATE TABLE IF NOT EXISTS "public"."wind_task_actor" (
"task_id" varchar(64) COLLATE "default" NOT NULL,
"actor_id" varchar(64) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Alter Sequences Owned By
-- ----------------------------

-- ----------------------------
-- Indexes structure for table wind_act_hist
-- ----------------------------
CREATE INDEX IF NOT EXISTS "wind_act_hist_approve_time" ON "public"."wind_act_hist" USING btree ("approve_time");
CREATE INDEX IF NOT EXISTS "wind_act_hist_order_id" ON "public"."wind_act_hist" USING btree ("order_id");
CREATE INDEX IF NOT EXISTS "wind_act_hist_process_id" ON "public"."wind_act_hist" USING btree ("process_id");
CREATE UNIQUE INDEX IF NOT EXISTS "wind_act_hist_task_id" ON "public"."wind_act_hist" USING btree ("task_id");

-- ----------------------------
-- Primary Key structure for table wind_act_hist
-- ----------------------------
-- ALTER TABLE "public"."wind_act_hist" ADD PRIMARY KEY ("id");



-- ----------------------------
-- Indexes structure for table wind_end_hist
-- ----------------------------
CREATE INDEX IF NOT EXISTS "wind_end_hist_approve_time_copy" ON "public"."wind_end_hist" USING btree ("approve_time");
CREATE INDEX IF NOT EXISTS "wind_end_hist_order_id_copy" ON "public"."wind_end_hist" USING btree ("order_id");
CREATE INDEX IF NOT EXISTS "wind_end_hist_process_id_copy" ON "public"."wind_end_hist" USING btree ("process_id");
CREATE UNIQUE INDEX IF NOT EXISTS "wind_end_hist_task_id_copy" ON "public"."wind_end_hist" USING btree ("task_id");

-- ----------------------------
-- Primary Key structure for table wind_end_hist
-- ----------------------------
-- ALTER TABLE "public"."wind_end_hist" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table wind_hist_order
-- ----------------------------
CREATE INDEX IF NOT EXISTS "wind_hist_order_create_time_copy" ON "public"."wind_hist_order" USING btree ("create_time");
CREATE INDEX IF NOT EXISTS "wind_hist_order_create_user_copy" ON "public"."wind_hist_order" USING btree ("create_user");
CREATE INDEX IF NOT EXISTS "wind_hist_order_business_id_copy" ON "public"."wind_hist_order" USING btree ("business_id");
CREATE UNIQUE INDEX IF NOT EXISTS "wind_hist_order_id_copy" ON "public"."wind_hist_order" USING btree ("id");
CREATE INDEX IF NOT EXISTS "wind_hist_order_process_id_copy" ON "public"."wind_hist_order" USING btree ("process_id");

-- ----------------------------
-- Primary Key structure for table wind_hist_order
-- ----------------------------
-- ALTER TABLE "public"."wind_hist_order" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table wind_order
-- ----------------------------
CREATE INDEX IF NOT EXISTS "wind_order_create_time" ON "public"."wind_order" USING btree ("create_time");
CREATE INDEX IF NOT EXISTS "wind_order_create_user" ON "public"."wind_order" USING btree ("create_user");
CREATE INDEX IF NOT EXISTS "wind_order_business_id" ON "public"."wind_order" USING btree ("business_id");
CREATE UNIQUE INDEX IF NOT EXISTS  "wind_order_id" ON "public"."wind_order" USING btree ("id");
CREATE INDEX IF NOT EXISTS "wind_order_process_id" ON "public"."wind_order" USING btree ("process_id");

-- ----------------------------
-- Primary Key structure for table wind_order
-- ----------------------------
-- ALTER TABLE "public"."wind_order" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table wind_process_config
-- ----------------------------
CREATE INDEX IF NOT EXISTS "config_create_time" ON "public"."wind_process_config" USING btree ("create_time");
CREATE UNIQUE INDEX IF NOT EXISTS "config_id" ON "public"."wind_process_config" USING btree ("id");
CREATE INDEX IF NOT EXISTS "config_process_id" ON "public"."wind_process_config" USING btree ("process_id");

-- ----------------------------
-- Primary Key structure for table wind_process_config
-- ----------------------------
-- ALTER TABLE "public"."wind_process_config" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table wind_process_definition
-- ----------------------------
CREATE INDEX IF NOT EXISTS "process_create_time" ON "public"."wind_process_definition" USING btree ("create_time");
CREATE UNIQUE INDEX IF NOT EXISTS "process_id" ON "public"."wind_process_definition" USING btree ("id");

-- ----------------------------
-- Primary Key structure for table wind_process_definition
-- ----------------------------
-- ALTER TABLE "public"."wind_process_definition" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table wind_revoke_data
-- ----------------------------
-- ALTER TABLE "public"."wind_revoke_data" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table wind_task
-- ----------------------------
CREATE INDEX IF NOT EXISTS "wind_task_create_time" ON "public"."wind_task" USING btree ("create_time");
CREATE UNIQUE INDEX IF NOT EXISTS "wind_task_id" ON "public"."wind_task" USING btree ("id");
CREATE INDEX IF NOT EXISTS "wind_task_order_id" ON "public"."wind_task" USING btree ("order_id");
CREATE INDEX IF NOT EXISTS "wind_task_process_id" ON "public"."wind_task" USING btree ("process_id");

