package com.bcx.wind.workflow.db.util.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *  数据库返回值处理拦截器
 *
 * @author zhanglei
 */
public interface ResultSetHandler {

    /**
     * 拦截处理函数
     *
     * @param resultSet     执行语句后返回值
     * @return                处理后的返回值
     * @throws SQLException   异常
     */
    Object handle(ResultSet resultSet) throws SQLException;
}
