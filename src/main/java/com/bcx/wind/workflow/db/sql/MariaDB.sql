/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 100001
 Source Host           : localhost:5432
 Source Catalog        : wind_workflow
 Source Schema         : public

 Target Server Type    : MariaDB
 Target Server Version : 100099
 File Encoding         : 65001

 Date: 14/03/2019 13:56:26
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wind_act_hist
-- ----------------------------
CREATE TABLE IF NOT EXISTS  `wind_act_hist`  (
  `id` varchar(64) NOT NULL,
  `task_id` varchar(64) NOT NULL,
  `task_name` varchar(64) NOT NULL,
  `task_display_name` varchar(255) NULL,
  `process_id` varchar(64) NOT NULL,
  `order_id` varchar(64) NOT NULL,
  `process_name` varchar(64) NOT NULL,
  `process_display_name` varchar(255) NULL,
  `operate` varchar(32) NULL,
  `suggest` text NULL,
  `approve_time` datetime NULL,
  `actor_id` varchar(64) NULL,
  `actor_name` varchar(255) NULL,
  `create_time` datetime NULL,
  `approve_user_variable` longtext NULL,
  `task_type` varchar(32) NULL,
  `system` varchar(64) NULL,
  `submit_user_variable` longtext NULL,
  `variable` longtext NULL,
  `task_create_user` varchar(255),
  `task_level` varchar(16) not null,
  `task_parent_id` varchar(64),
  PRIMARY KEY (`id`),
  INDEX `wind_act_hist_approve_time`(`approve_time` ASC) USING BTREE,
  INDEX `wind_act_hist_order_id`(`order_id` ASC) USING BTREE,
  INDEX `wind_act_hist_process_id`(`process_id` ASC) USING BTREE,
  INDEX `wind_act_hist_task_id`(`task_id` ASC) USING BTREE
);


-- ----------------------------
-- Table structure for wind_end_hist
-- ----------------------------
CREATE TABLE IF NOT EXISTS  `wind_end_hist`  (
  `id` varchar(64) NOT NULL,
  `task_id` varchar(64) NOT NULL,
  `task_name` varchar(64) NOT NULL,
  `task_display_name` varchar(255) NULL,
  `process_id` varchar(64) NOT NULL,
  `order_id` varchar(64) NOT NULL,
  `process_name` varchar(64) NOT NULL,
  `process_display_name` varchar(255) NULL,
  `operate` varchar(32) NULL,
  `suggest` text NULL,
  `approve_time` datetime NULL,
  `actor_id` varchar(64) NULL,
  `actor_name` varchar(255) NULL,
  `create_time` datetime NULL,
  `approve_user_variable` longtext NULL,
  `task_type` varchar(32) NULL,
  `system` varchar(64) NULL,
  `submit_user_variable` longtext NULL,
  `variable` longtext NULL,
  `task_create_user` varchar(255),
  `task_level` varchar(16) not null,
  `task_parent_id` varchar(64),
  PRIMARY KEY (`id`),
  INDEX `wind_end_hist_approve_time_copy`(`approve_time` ASC) USING BTREE,
  INDEX `wind_end_hist_order_id_copy`(`order_id` ASC) USING BTREE,
  INDEX `wind_end_hist_process_id_copy`(`process_id` ASC) USING BTREE,
  INDEX `wind_end_hist_task_id_copy`(`task_id` ASC) USING BTREE
);

-- ----------------------------
-- Table structure for wind_hist_order
-- ----------------------------
CREATE TABLE IF NOT EXISTS  `wind_hist_order`  (
  `id` varchar(64) NOT NULL,
  `process_id` varchar(64) NOT NULL,
  `status` varchar(32) NOT NULL,
  `create_user` varchar(64) NOT NULL,
  `create_time` datetime NOT NULL,
  `expire_time` datetime NULL,
  `parent_id` varchar(64) NULL,
  `version` smallint NOT NULL,
  `variable` longtext NULL,
  `data` text NULL,
  `system` varchar(64) NULL,
  `finish_time` datetime NULL,
  `business_id` varchar(64) NOT NULL,
  `type` varchar(32) NULL,
  PRIMARY KEY (`id`),
  INDEX `wind_hist_order_create_time_copy`(`create_time` ASC) USING BTREE,
  INDEX `wind_hist_order_create_user_copy`(`create_user` ASC) USING BTREE,
  INDEX `wind_hist_order_business_id_copy`(`business_id` ASC) USING BTREE,
  INDEX `wind_hist_order_id_copy`(`id` ASC) USING BTREE,
  INDEX `wind_hist_order_process_id_copy`(`process_id` ASC) USING BTREE
);

-- ----------------------------
-- Table structure for wind_order
-- ----------------------------
CREATE TABLE IF NOT EXISTS  `wind_order`  (
  `id` varchar(64) NOT NULL,
  `process_id` varchar(64) NOT NULL,
  `status` varchar(32) NOT NULL,
  `create_user` varchar(64) NOT NULL,
  `create_time` datetime NOT NULL,
  `expire_time` datetime NULL,
  `parent_id` varchar(64) NULL,
  `version` smallint NOT NULL,
  `variable` longtext NULL,
  `data` text NULL,
  `system` varchar(64) NULL,
  `finish_time` datetime NULL,
  `business_id` varchar(64) NOT NULL,
  `type` varchar(32) NULL,
  PRIMARY KEY (`id`),
  INDEX `wind_order_create_time`(`create_time` ASC) USING BTREE,
  INDEX `wind_order_create_user`(`create_user` ASC) USING BTREE,
  INDEX `wind_order_business_id`(`business_id` ASC) USING BTREE,
  INDEX `wind_order_id`(`id` ASC) USING BTREE,
  INDEX `wind_order_process_id`(`process_id` ASC) USING BTREE
);

-- ----------------------------
-- Records of wind_order
-- ----------------------------

-- ----------------------------
-- Table structure for wind_process_config
-- ----------------------------
CREATE TABLE IF NOT EXISTS  `wind_process_config`  (
  `id` varchar(64) NOT NULL,
  `process_id` varchar(64) NOT NULL,
  `config_name` varchar(255) NULL,
  `process_name` varchar(255) NOT NULL,
  `node_id` varchar(64) NOT NULL,
  `condition` text NULL,
  `node_config` longtext NULL,
  `approve_user` longtext NULL,
  `business_config` text NULL,
  `sort` smallint NOT NULL,
  `level` smallint NOT NULL,
  `create_time` datetime NOT NULL,
  `logic` varchar(32) NULL,
  `script` text NULL,
  PRIMARY KEY (`id`),
  INDEX `config_create_time`(`create_time` ASC) USING BTREE,
  INDEX `config_id`(`id` ASC) USING BTREE,
  INDEX `config_process_id`(`process_id` ASC) USING BTREE
);

-- ----------------------------
-- Table structure for wind_process_definition
-- ----------------------------
CREATE TABLE  IF NOT EXISTS `wind_process_definition`  (
  `id` varchar(64) NOT NULL,
  `process_name` varchar(255) NOT NULL,
  `display_name` varchar(255) NULL,
  `status` varchar(32) NOT NULL,
  `version` smallint NOT NULL,
  `parent_id` varchar(64) NULL,
  `create_time` datetime NOT NULL,
  `content` longblob NOT NULL,
  `system` varchar(64) NULL,
  `module` varchar(64) NULL,
  PRIMARY KEY (`id`),
  INDEX `process_create_time`(`create_time` ASC) USING BTREE,
  INDEX `process_id`(`id` ASC) USING BTREE
);


-- ----------------------------
-- Table structure for wind_revoke_data
-- ----------------------------
CREATE TABLE IF NOT EXISTS  `wind_revoke_data`  (
  `id` varchar(64) NOT NULL,
  `order_id` varchar(64) NOT NULL,
  `process_id` varchar(64) NOT NULL,
  `process_display_name` varchar(255) NULL,
  `task_name` varchar(64) NOT NULL,
  `task_display_name` varchar(255) NULL,
  `actor_id` varchar(64) NOT NULL,
  `operate` varchar(32) NOT NULL,
  `suggest` text NULL,
  `operate_time` datetime NOT NULL,
  `wind_order` longtext NOT NULL,
  `wind_task` longtext NOT NULL,
  `wind_task_actor` longtext NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
);

-- ----------------------------
-- Table structure for wind_task
-- ----------------------------
CREATE TABLE  IF NOT EXISTS  `wind_task`  (
  `id` varchar(64) NOT NULL,
  `task_name` varchar(64) NOT NULL,
  `display_name` varchar(255) NULL,
  `task_type` varchar(32) NOT NULL,
  `create_time` datetime NOT NULL,
  `expire_time` datetime NULL,
  `approve_count` smallint NOT NULL,
  `status` varchar(32) NOT NULL,
  `order_id` varchar(64) NOT NULL,
  `process_id` varchar(64) NOT NULL,
  `variable` longtext NULL,
  `parent_id` varchar(64) NULL,
  `version` smallint NOT NULL,
  `position` text NULL,
  `create_user` varchar(255),
  `task_level` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `wind_task_create_time`(`create_time` ASC) USING BTREE,
  INDEX `wind_task_id`(`id` ASC) USING BTREE,
  INDEX `wind_task_order_id`(`order_id` ASC) USING BTREE,
  INDEX `wind_task_process_id`(`process_id` ASC) USING BTREE
);


CREATE TABLE IF NOT EXISTS `wind_process_module` (
  `id` varchar(64)   NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `parent_id` varchar(64),
  `system` varchar(64),
  PRIMARY KEY (`id`)
);

-- ----------------------------
-- Table structure for wind_task_actor
-- ----------------------------
CREATE TABLE IF NOT EXISTS `wind_task_actor`  (
  `task_id` varchar(64) NOT NULL,
  `actor_id` varchar(64) NOT NULL,
  INDEX `wind_task_actor_task_id`(`task_id` ASC) USING BTREE
);

SET FOREIGN_KEY_CHECKS = 1;
