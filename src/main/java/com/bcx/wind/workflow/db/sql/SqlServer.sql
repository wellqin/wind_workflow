

-- ----------------------------
-- Table structure for wind_act_hist
-- ----------------------------
IF NOT EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[wind_act_hist]') AND type IN ('U'))
CREATE TABLE [dbo].[wind_act_hist] (
  [id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [task_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [task_name] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [task_display_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [process_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [order_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [process_name] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [process_display_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [operate] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NULL,
  [suggest] nvarchar(1024) COLLATE Chinese_PRC_CI_AS  NULL,
  [approve_time] datetime2(7)  NULL,
  [actor_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [actor_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_time] datetime2(7)  NULL,
  [approve_user_variable] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [task_type] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NULL,
  [system] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [submit_user_variable] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [variable] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [task_create_user] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [task_level] nvarchar(16) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [task_parent_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL
)




-- ----------------------------
-- Table structure for wind_end_hist
-- ----------------------------
IF NOT EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[wind_end_hist]') AND type IN ('U'))
CREATE TABLE [dbo].[wind_end_hist] (
  [id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [task_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [task_name] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [task_display_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [process_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [order_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [process_name] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [process_display_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [operate] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NULL,
  [suggest] nvarchar(1024) COLLATE Chinese_PRC_CI_AS  NULL,
  [approve_time] datetime2(7)  NULL,
  [actor_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [actor_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_time] datetime2(7)  NULL,
  [approve_user_variable] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [task_type] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NULL,
  [system] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [submit_user_variable] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [variable] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [task_create_user] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [task_level] nvarchar(16) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [task_parent_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL
)

-- ----------------------------
-- Table structure for wind_hist_order
-- ----------------------------
IF NOT EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[wind_hist_order]') AND type IN ('U'))
CREATE TABLE [dbo].[wind_hist_order] (
  [id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [process_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [status] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_user] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_time] datetime2(7)  NOT NULL,
  [expire_time] datetime2(7)  NULL,
  [parent_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [version] smallint  NOT NULL,
  [variable] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [data] nvarchar(4000) COLLATE Chinese_PRC_CI_AS  NULL,
  [system] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [finish_time] datetime2(7)  NULL,
  [business_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [type] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NULL
)

-- ----------------------------
-- Table structure for wind_order
-- ----------------------------
IF NOT EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[wind_order]') AND type IN ('U'))
CREATE TABLE [dbo].[wind_order] (
  [id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [process_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [status] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_user] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_time] datetime2(7)  NOT NULL,
  [expire_time] datetime2(7)  NULL,
  [parent_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [version] smallint  NOT NULL,
  [variable] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [data] nvarchar(4000) COLLATE Chinese_PRC_CI_AS  NULL,
  [system] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [finish_time] datetime2(7)  NOT NULL,
  [business_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [type] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NULL
)

-- ----------------------------
-- Table structure for wind_process_config
-- ----------------------------
IF NOT EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[wind_process_config]') AND type IN ('U'))
CREATE TABLE [dbo].[wind_process_config] (
  [id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [process_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [config_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [process_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [node_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [condition] nvarchar(4000) COLLATE Chinese_PRC_CI_AS  NULL,
  [node_config] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [approve_user] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [business_config] nvarchar(4000) COLLATE Chinese_PRC_CI_AS  NULL,
  [sort] smallint  NOT NULL,
  [level] smallint  NOT NULL,
  [create_time] datetime2(7)  NOT NULL,
  [logic] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NULL,
  [script] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
)
-- ----------------------------
-- Table structure for wind_process_definition
-- ----------------------------
IF NOT EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[wind_process_definition]') AND type IN ('U'))
CREATE TABLE [dbo].[wind_process_definition] (
  [id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [process_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [display_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [status] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [version] smallint  NOT NULL,
  [parent_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_time] datetime2(7)  NOT NULL,
  [content] varbinary(max)  NOT NULL,
  [system] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [module] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL
)


-- ----------------------------
-- Table structure for wind_revoke_data
-- ----------------------------
IF NOT EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[wind_revoke_data]') AND type IN ('U'))
CREATE TABLE [dbo].[wind_revoke_data] (
  [id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [order_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [process_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [process_display_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [task_name] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [task_display_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [actor_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [operate] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [suggest] nvarchar(1024) COLLATE Chinese_PRC_CI_AS  NULL,
  [operate_time] datetime2(7)  NOT NULL,
  [wind_order] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [wind_task] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [wind_task_actor] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_time] datetime2(7)  NOT NULL
)

-- ----------------------------
-- Table structure for wind_task
-- ----------------------------
IF NOT EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[wind_task]') AND type IN ('U'))
CREATE TABLE [dbo].[wind_task] (
  [id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [task_name] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [display_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [task_type] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_time] datetime2(7)  NOT NULL,
  [expire_time] datetime2(7)  NULL,
  [approve_count] smallint  NOT NULL,
  [status] nvarchar(32) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [order_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [process_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [variable] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [parent_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [version] smallint  NOT NULL,
  [position] nvarchar(1024) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_user] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [task_level] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL
)


IF NOT EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[wind_process_module]') AND type IN ('U'))
CREATE TABLE  wind_process_module (
[[id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
[module_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
[parentId] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
[system] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NULL
);

-- ----------------------------
-- Table structure for wind_task_actor
-- ----------------------------
CREATE TABLE [dbo].[wind_task_actor] (
  [task_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [actor_id] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL
)


-- ----------------------------
-- Indexes structure for table wind_act_hist
-- ----------------------------
CREATE NONCLUSTERED INDEX [wind_act_hist_approve_time]
ON [dbo].[wind_act_hist] (
  [approve_time] ASC
)

CREATE NONCLUSTERED INDEX [wind_act_hist_order_id]
ON [dbo].[wind_act_hist] (
  [order_id] ASC
)


CREATE NONCLUSTERED INDEX [wind_act_hist_process_id]
ON [dbo].[wind_act_hist] (
  [process_id] ASC
)

CREATE UNIQUE NONCLUSTERED INDEX [wind_act_hist_task_id]
ON [dbo].[wind_act_hist] (
  [task_id] ASC
)








-- ----------------------------
-- Indexes structure for table wind_end_hist
-- ----------------------------
CREATE NONCLUSTERED INDEX [wind_end_hist_approve_time_copy]
ON [dbo].[wind_end_hist] (
  [approve_time] ASC
)


CREATE NONCLUSTERED INDEX [wind_end_hist_order_id_copy]
ON [dbo].[wind_end_hist] (
  [order_id] ASC
)


CREATE NONCLUSTERED INDEX [wind_end_hist_process_id_copy]
ON [dbo].[wind_end_hist] (
  [process_id] ASC
)


CREATE UNIQUE NONCLUSTERED INDEX [wind_end_hist_task_id_copy]
ON [dbo].[wind_end_hist] (
  [task_id] ASC
)







-- ----------------------------
-- Indexes structure for table wind_hist_order
-- ----------------------------
CREATE NONCLUSTERED INDEX [wind_hist_order_create_time_copy]
ON [dbo].[wind_hist_order] (
  [create_time] ASC
)

-- ----------------------------
-- Indexes structure for table wind_hist_order
-- ----------------------------
CREATE NONCLUSTERED INDEX [wind_hist_order_business_id_copy]
ON [dbo].[wind_hist_order] (
  [business_id] ASC
)

CREATE NONCLUSTERED INDEX [wind_hist_order_create_user_copy]
ON [dbo].[wind_hist_order] (
  [create_user] ASC
)


CREATE UNIQUE NONCLUSTERED INDEX [wind_hist_order_id_copy]
ON [dbo].[wind_hist_order] (
  [id] ASC
)


CREATE NONCLUSTERED INDEX [wind_hist_order_process_id_copy]
ON [dbo].[wind_hist_order] (
  [process_id] ASC
)






-- ----------------------------
-- Indexes structure for table wind_order
-- ----------------------------
CREATE NONCLUSTERED INDEX [wind_order_create_time]
ON [dbo].[wind_order] (
  [create_time] ASC
)


CREATE NONCLUSTERED INDEX [wind_order_business_id]
ON [dbo].[wind_order] (
  [business_id] ASC
)


CREATE NONCLUSTERED INDEX [wind_order_create_user]
ON [dbo].[wind_order] (
  [create_user] ASC
)


CREATE UNIQUE NONCLUSTERED INDEX [wind_order_id]
ON [dbo].[wind_order] (
  [id] ASC
)


CREATE NONCLUSTERED INDEX [wind_order_process_id]
ON [dbo].[wind_order] (
  [process_id] ASC
)






-- ----------------------------
-- Indexes structure for table wind_process_config
-- ----------------------------
CREATE NONCLUSTERED INDEX [config_create_time]
ON [dbo].[wind_process_config] (
  [create_time] ASC
)


CREATE UNIQUE NONCLUSTERED INDEX [config_id]
ON [dbo].[wind_process_config] (
  [id] ASC
)


CREATE NONCLUSTERED INDEX [config_process_id]
ON [dbo].[wind_process_config] (
  [process_id] ASC
)






-- ----------------------------
-- Indexes structure for table wind_process_definition
-- ----------------------------
CREATE NONCLUSTERED INDEX [process_create_time]
ON [dbo].[wind_process_definition] (
  [create_time] ASC
)


CREATE UNIQUE NONCLUSTERED INDEX [process_id]
ON [dbo].[wind_process_definition] (
  [id] ASC
)








-- ----------------------------
-- Indexes structure for table wind_task
-- ----------------------------
CREATE NONCLUSTERED INDEX [wind_task_create_time]
ON [dbo].[wind_task] (
  [create_time] ASC
)


CREATE UNIQUE NONCLUSTERED INDEX [wind_task_id]
ON [dbo].[wind_task] (
  [id] ASC
)


CREATE NONCLUSTERED INDEX [wind_task_order_id]
ON [dbo].[wind_task] (
  [order_id] ASC
)


CREATE NONCLUSTERED INDEX [wind_task_process_id]
ON [dbo].[wind_task] (
  [process_id] ASC
)






-- ----------------------------
-- Indexes structure for table wind_task_actor
-- ----------------------------
CREATE NONCLUSTERED INDEX [wind_task_actor_task_id]
ON [dbo].[wind_task_actor] (
  [task_id] ASC
)


