package com.bcx.wind.workflow.errorcontext;

/**
 * 默认错误信息
 *
 * @author zhanglei
 */
public class DefaultErrorContext implements ErrorContext {

    /**
     * 错误信息
     */
    private String errorMsg;

    /**
     * 错误
     */
    private Throwable throwable;

    /**
     * 错误代码
     */
    private int code;

    /**
     * 工作流创建异常信息
     */
    private ProcessBuilderError processBuilderError = new ProcessBuilderError();

    public DefaultErrorContext(){

    }

    public DefaultErrorContext(String errorMsg,int code,Throwable throwable){
        this.errorMsg = errorMsg;
        this.code = code;
        this.throwable = throwable;
    }

    @Override
    public String errorMsg() {
        return this.errorMsg;
    }

    @Override
    public ErrorContext errorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
        return this;
    }

    @Override
    public int code() {
        return this.code;
    }

    @Override
    public ErrorContext code(int code) {
        this.code = code;
        return this;
    }

    @Override
    public Throwable throwable() {
        return this.throwable;
    }

    @Override
    public ErrorContext throwable(Throwable throwable) {
        this.throwable = throwable;
        return this;
    }

    @Override
    public boolean error() {
        return this.code != 0;
    }

    @Override
    public ProcessBuilderError processBuilderError() {
        return this.processBuilderError;
    }

    @Override
    public ErrorContext processBuilderError(ProcessBuilderError processBuilderError) {
        this.processBuilderError = processBuilderError;
        return this;
    }

    public ProcessBuilderError getProcessBuilderError() {
        return processBuilderError;
    }

    public void setProcessBuilderError(ProcessBuilderError processBuilderError) {
        this.processBuilderError = processBuilderError;
    }
}
