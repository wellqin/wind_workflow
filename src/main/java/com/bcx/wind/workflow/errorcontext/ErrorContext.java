package com.bcx.wind.workflow.errorcontext;

import com.bcx.wind.workflow.parser.ProcessBuilder;

/**
 * 工作流错误信息
 *
 * @author zhanglei
 */
public interface ErrorContext {

    /**
     * 获取错误基础信息
     *
     * @return  错误信息
     */
    String  errorMsg();


    /**
     * 设置错误信息
     *
     * @param errorMsg  错误信息
     * @return    错误
     */
    ErrorContext  errorMsg(String errorMsg);


    /**
     * 获取错误代码
     *
     * @return  错误代码
     */
    int  code();


    /**
     * 设置错误代码
     *
     * @param code  错误代码
     * @return    错误
     */
    ErrorContext  code(int code);


    /**
     * 错误异常信息
     *
     * @return 错误异常信息
     */
    Throwable  throwable();


    /**
     * 设置错误
     *
     * @param throwable  错误
     * @return    错误
     */
    ErrorContext  throwable(Throwable throwable);


    /**
     * 是否错误
     *
     * @return  是否错误
     */
    boolean  error();


    /**
     * 工作流构建错误信息
     *
     * @return 工作流构建错误信息
     */
    ProcessBuilderError  processBuilderError();

    /**
     * 设置流程定义构建失败信息
     *
     * @param processBuilderError   流程定义构建错误
     * @return        错误
     */
    ErrorContext  processBuilderError(ProcessBuilderError processBuilderError);
}
