package com.bcx.wind.workflow.errorcontext;

/**
 * 工作流当前线程错误信息
 *
 * @author zhanglei
 */
public class WindLocalError {

    /**
     * 错误信息
     */
    private static ThreadLocal<ErrorContext>  error = new ThreadLocal<>();

    private WindLocalError(){}

    public static ErrorContext  get(){
        ErrorContext context =  error.get();
        error.remove();
        if(context == null){
            context = new DefaultErrorContext();
        }
        return context;
    }

    public static ErrorContext  getLocal(){
        ErrorContext context =  error.get();
        if(context == null){
            context = new DefaultErrorContext();
            error.set(context);
        }
        return context;
    }

    public static void remove(){
        error.remove();
    }

    public static void  set(ErrorContext context){
        error.set(context);
    }

}
