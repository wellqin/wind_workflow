package com.bcx.wind.workflow.pojo;

import com.bcx.wind.workflow.core.constant.NodeType;

/**
 * 任务节点相关属性 主要作序列化使用
 *
 * @author zhanglei
 */
public class TaskNodeAttr {

    /**
     * 任务节点类型
     */
    private NodeType nodeType;


    /**
     * 是否在子节点中
     */
    private boolean inRouter;


    /**
     * 是否为会签
     */
    private boolean jointly;


    /**
     * 是否为串签
     */
    private boolean strand;


    /**
     * 是否在分支中
     */
    private boolean inForkJoin;

    /**
     * 是否在并且分支中
     */
    private boolean inAnd;


    /**
     * 是否在或者分支中
     */
    private boolean inOr;


    /**
     * 是否在动态分支中   动态分支默认为并且状态
     */
    private boolean inDyna;


    /**
     * 是否是第一个任务
     */
    private boolean isFirstTask;

    /**
     * 是否是在最后一个任务
     */
    private boolean isLastTask;

    public NodeType getNodeType() {
        return nodeType;
    }

    public TaskNodeAttr setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
        return this;
    }

    public boolean isInRouter() {
        return inRouter;
    }

    public TaskNodeAttr setInRouter(boolean inRouter) {
        this.inRouter = inRouter;
        return this;
    }

    public boolean isJointly() {
        return jointly;
    }

    public TaskNodeAttr setJointly(boolean jointly) {
        this.jointly = jointly;
        return this;
    }

    public boolean isStrand() {
        return strand;
    }

    public TaskNodeAttr setStrand(boolean strand) {
        this.strand = strand;
        return this;
    }

    public boolean isInForkJoin() {
        return inForkJoin;
    }

    public TaskNodeAttr setInForkJoin(boolean inForkJoin) {
        this.inForkJoin = inForkJoin;
        return this;
    }

    public boolean isInAnd() {
        return inAnd;
    }

    public TaskNodeAttr setInAnd(boolean inAnd) {
        this.inAnd = inAnd;
        return this;
    }

    public boolean isInOr() {
        return inOr;
    }

    public TaskNodeAttr setInOr(boolean inOr) {
        this.inOr = inOr;
        return this;
    }

    public boolean isInDyna() {
        return inDyna;
    }

    public TaskNodeAttr setInDyna(boolean inDyna) {
        this.inDyna = inDyna;
        return this;
    }

    public boolean isFirstTask() {
        return isFirstTask;
    }

    public TaskNodeAttr setFirstTask(boolean firstTask) {
        isFirstTask = firstTask;
        return this;
    }

    public boolean isLastTask() {
        return isLastTask;
    }

    public TaskNodeAttr setLastTask(boolean lastTask) {
        isLastTask = lastTask;
        return this;
    }
}

















