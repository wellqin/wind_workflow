package com.bcx.wind.workflow.pojo;

/**
 * 工作流使用用户 仅提供用户ID  和用户名
 *
 * @author zhanglei
 */
public class WindUser{


    /**
     * 用户ID
     */
    private String userId;


    /**
     * 用户名称
     */
    private String userName;

    /**
     * 代理用户
     *
     * 当某个人的任务需要交给另一个人代理时，将代理人的用户信息赋值给proxyUser属性。操作时还是使用这个任务的实际审批人进行操作。
     * 引擎会将代理人用户信息保存到操作历史中，以达到代理的功能，如果代理用户为空，则默认使用当前对象用户
     */
    private WindUser  proxyUser;


    public String getUserId() {
        return userId;
    }

    public WindUser setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public WindUser setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public WindUser getProxyUser() {
        return proxyUser;
    }

    public WindUser setProxyUser(WindUser proxyUser) {
        this.proxyUser = proxyUser;
        return this;
    }

    public String proxyUserId(){
        if(this.proxyUser != null && this.proxyUser.getUserId() != null){
            return this.proxyUser.getUserId();
        }
        return userId;
    }


    public String proxyUserName(){
        if(this.proxyUser != null && this.proxyUser.getUserId() != null){
            return this.proxyUser.getUserName();
        }
        return this.userName;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof WindUser){
            return this.getUserId().equals(((WindUser) obj).getUserId());
        }
        return super.equals(obj);
    }
}
