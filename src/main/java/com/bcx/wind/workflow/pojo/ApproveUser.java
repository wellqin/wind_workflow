package com.bcx.wind.workflow.pojo;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

/**
 * 工作流提交审批人数据实体
 *
 * @author zhanglei
 */
public class ApproveUser {

    /**
     * 提交的任务节点ID
     */
    private String nodeId;


    /**
     * 期望完成时间
     */
    private Timestamp expireTime;


    /**
     * 任务节点的审批人
     */
    private List<WindUser> users = new LinkedList<>();

    public String getNodeId() {
        return nodeId;
    }

    public ApproveUser setNodeId(String nodeId) {
        this.nodeId = nodeId;
        return this;
    }

    public List<WindUser> getUsers() {
        return users;
    }

    public ApproveUser setUsers(List<WindUser> users) {
        this.users = users;
        return this;
    }

    public Timestamp getExpireTime() {
        return expireTime;
    }

    public ApproveUser setExpireTime(Timestamp expireTime) {
        this.expireTime = expireTime;
        return this;
    }

    public ApproveUser  addUser(List<WindUser> user){
        this.users.addAll(user);
        return this;
    }
}
