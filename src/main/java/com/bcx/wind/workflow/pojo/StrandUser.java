package com.bcx.wind.workflow.pojo;

/**
 * 串签用户实体
 *
 * @author zhanglei
 */
public class StrandUser {

    /**
     * 排序
     */
    private int sort;


    /**
     * 审批人
     */
    private WindUser user;

    public int getSort() {
        return sort;
    }

    public StrandUser setSort(int sort) {
        this.sort = sort;
        return this;
    }

    public WindUser getUser() {
        return user;
    }

    public StrandUser setUser(WindUser user) {
        this.user = user;
        return this;
    }


}
