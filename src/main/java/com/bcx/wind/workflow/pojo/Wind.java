package com.bcx.wind.workflow.pojo;

import com.bcx.wind.workflow.core.constant.WorkflowOperate;
import com.bcx.wind.workflow.entity.WindOrder;
import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.entity.WindTask;

import java.util.*;

/**
 * 工作流核心执行数据
 *
 * @author zhanglei
 */
public class Wind {

    /**
     * 业务ID
     */
    private String businessId;

    /**
     * 流程实例
     */
    private WindOrder windOrder;

    /**
     * 流程定义
     */
    private WindProcess windProcess;

    /**
     * 流程配置
     */
    private WindConfig windConfig;

    /**
     * 当前用户
     */
    private WindUser user;

    /**
     * 匹配参数
     */
    private Map<String,Object> args = new LinkedHashMap<>();


    /**
     * 操作参数，会根据提交为build  或是submit  reject 放在 order variable 和 task variable字段中
     */
    private Map<String,Object>  vars = new LinkedHashMap<>();

    /**
     * 当前任务
     */
    private List<Task> curNode = new LinkedList<>();


    /**
     * 当前流程下所有正在执行的任务
     */
    private List<WindTask> allTasks = new LinkedList<>();

    /**
     * 意见建议
     */
    private String suggest;

    /**
     * 审批人数据
     */
    private List<ApproveUser> approveUsers = new LinkedList<>();

    /**
     * 实际操作人
     */
    private String operator;

    /**
     * 操作类型
     */
    private WorkflowOperate operate;

    /**
     * 模块
     */
    private String system;

    /**
     * 提交到指定节点  这里的提交包含退回，也可以表示退回到指定的节点
     */
    private String submitNode;


    /**
     * 提交路线
     */
    private Map<String,List<String>>  submitPath = new HashMap<>();

    /**
     * 是否向下执行
     */
    private  boolean next = false;


    public String getBusinessId() {
        return businessId;
    }

    public Wind setBusinessId(String businessId) {
        this.businessId = businessId;
        return this;
    }

    public WindOrder getWindOrder() {
        return windOrder;
    }

    public Wind setWindOrder(WindOrder windOrder) {
        this.windOrder = windOrder;
        return this;
    }

    public WindProcess getWindProcess() {
        return windProcess;
    }

    public Wind setWindProcess(WindProcess windProcess) {
        this.windProcess = windProcess;
        return this;
    }

    public WindConfig getWindConfig() {
        return windConfig;
    }

    public Wind setWindConfig(WindConfig windConfig) {
        this.windConfig = windConfig;
        return this;
    }

    public WindUser getUser() {
        return user;
    }

    public Wind setUser(WindUser user) {
        this.user = user;
        return this;
    }

    public Map<String, Object> getArgs() {
        return args;
    }

    public Wind setArgs(Map<String, Object> args) {
        this.args = args;
        return this;
    }

    public List<Task> getCurNode() {
        return curNode;
    }

    public Wind setCurNode(List<Task> curNode) {
        this.curNode = curNode;
        return this;
    }

    public String getSuggest() {
        return suggest;
    }

    public Wind setSuggest(String suggest) {
        this.suggest = suggest;
        return this;
    }

    public List<ApproveUser> getApproveUsers() {
        return approveUsers;
    }

    public Wind setApproveUsers(List<ApproveUser> approveUsers) {
        this.approveUsers = approveUsers;
        return this;
    }

    public String getOperator() {
        return operator;
    }

    public Wind setOperator(String operator) {
        this.operator = operator;
        return this;
    }

    public WorkflowOperate getOperate() {
        return operate;
    }

    public Wind setOperate(WorkflowOperate operate) {
        this.operate = operate;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public Wind setSystem(String system) {
        this.system = system;
        return this;
    }

    public Map<String, List<String>> getSubmitPath() {
        return submitPath;
    }

    public Wind setSubmitPath(Map<String, List<String>> submitPath) {
        this.submitPath = submitPath;
        return this;
    }

    public List<WindTask> getAllTasks() {
        return allTasks;
    }

    public Wind setAllTasks(List<WindTask> allTasks) {
        this.allTasks = allTasks;
        return this;
    }

    public String getSubmitNode() {
        return submitNode;
    }

    public Wind setSubmitNode(String submitNode) {
        this.submitNode = submitNode;
        return this;
    }

    public boolean isNext() {
        return next;
    }

    public Wind setNext(boolean next) {
        this.next = next;
        return this;
    }

    public Map<String, Object> getVars() {
        return vars;
    }

    public Wind setVars(Map<String, Object> vars) {
        this.vars = vars;
        return this;
    }
}
