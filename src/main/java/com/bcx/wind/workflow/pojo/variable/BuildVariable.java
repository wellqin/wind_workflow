package com.bcx.wind.workflow.pojo.variable;

import com.bcx.wind.workflow.pojo.WindUser;

import java.sql.Timestamp;
import java.util.Map;

/**
 * 创建工作流参数实体
 *
 * @author zhanglei
 */
public class BuildVariable {

    /**
     * 当前用户
     */
    private WindUser user;


    /**
     * 业务数据ID
     */
    private String businessId;

    /**
     * 流程ID   ,创建流程时，如果processId  和 processName参数都有，则优先使用processId来启动
     */
    private String processId;


    /**
     * 流程定义名称
     */
    private String processName;

    /**
     * 规则参数,主要用来匹配节点配置
     */
    private Map<String,Object> args;

    /**
     * 任务扩展变量字段数据， 该参数不参与引擎业务，会被自动保存到流程实例变量variable字段中，供二次开发使用
     */
    private Map<String,Object> vars;

    /**
     * 流程所属模块
     */
    private String system;

    /**
     * 自定义创建的流程信息
     */
    private  String   orderMsg;

    /**
     * 流程期望完成时间
     */
    private Timestamp  expireTime;


    public WindUser getUser() {
        return user;
    }

    public BuildVariable setUser(WindUser user) {
        this.user = user;
        return this;
    }

    public String getBusinessId() {
        return businessId;
    }

    public BuildVariable setBusinessId(String businessId) {
        this.businessId = businessId;
        return this;
    }

    public String getProcessId() {
        return processId;
    }

    public BuildVariable setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public String getProcessName() {
        return processName;
    }

    public BuildVariable setProcessName(String processName) {
        this.processName = processName;
        return this;
    }

    public Map<String, Object> getArgs() {
        return args;
    }

    public BuildVariable setArgs(Map<String, Object> args) {
        this.args = args;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public BuildVariable setSystem(String system) {
        this.system = system;
        return this;
    }

    public String getOrderMsg() {
        return orderMsg;
    }

    public BuildVariable setOrderMsg(String orderMsg) {
        this.orderMsg = orderMsg;
        return this;
    }

    public Timestamp getExpireTime() {
        return expireTime;
    }

    public BuildVariable setExpireTime(Timestamp expireTime) {
        this.expireTime = expireTime;
        return this;
    }

    public Map<String, Object> getVars() {
        return vars;
    }

    public BuildVariable setVars(Map<String, Object> vars) {
        this.vars = vars;
        return this;
    }
}
