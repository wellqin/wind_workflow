package com.bcx.wind.workflow.pojo;

import com.bcx.wind.workflow.entity.WindProcess;
import com.bcx.wind.workflow.entity.WindProcessModule;

import java.util.LinkedList;
import java.util.List;

/**
 * 流程定义模块组合
 *
 * @author zhanglei
 */
public class ProcessModule {

    /**
     * 当前模块
     */
    private WindProcessModule windProcessModule;


    /**
     * 当前模块摸下流程定义
     */
    private List<WindProcess> windProcesses = new LinkedList<>();


    /**
     * 子模块
     */
    private List<ProcessModule>  child = new LinkedList<>();

    public WindProcessModule getWindProcessModule() {
        return windProcessModule;
    }

    public ProcessModule setWindProcessModule(WindProcessModule windProcessModule) {
        this.windProcessModule = windProcessModule;
        return this;
    }

    public List<ProcessModule> getChild() {
        return child;
    }

    public ProcessModule setChild(List<ProcessModule> child) {
        this.child = child;
        return this;
    }

    public List<WindProcess> getWindProcesses() {
        return windProcesses;
    }

    public ProcessModule setWindProcesses(List<WindProcess> windProcesses) {
        this.windProcesses = windProcesses;
        return this;
    }
}
