package com.bcx.wind.workflow.pojo;

import com.bcx.wind.workflow.support.JsonHelper;

import java.util.List;
import java.util.Map;

/**
 * 工作流任务配置详情
 *
 * @author zhanglei
 */
public class WindConfig {

    /**
     * 流程定义ID
     */
    private String processId;

    /**
     * 配置名称
     */
    private String configName;

    /**
     * 流程定义名称
     */
    private String processName;

    /**
     * 任务节点ID
     */
    private String nodeId;

    /**
     * 任务节点配置
     */
    private String nodeConfig;

    /**
     * 业务参数
     */
    private String businessConfig;


    /**
     * 预审批人
     */
    private String approveUser;


    public String getProcessId() {
        return processId;
    }

    public WindConfig setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public String getConfigName() {
        return configName;
    }

    public WindConfig setConfigName(String configName) {
        this.configName = configName;
        return this;
    }

    public String getProcessName() {
        return processName;
    }

    public WindConfig setProcessName(String processName) {
        this.processName = processName;
        return this;
    }

    public String getNodeId() {
        return nodeId;
    }

    public WindConfig setNodeId(String nodeId) {
        this.nodeId = nodeId;
        return this;
    }

    public String getNodeConfig() {
        return nodeConfig;
    }

    public WindConfig setNodeConfig(String nodeConfig) {
        this.nodeConfig = nodeConfig;
        return this;
    }

    public String getBusinessConfig() {
        return businessConfig;
    }

    public WindConfig setBusinessConfig(String businessConfig) {
        this.businessConfig = businessConfig;
        return this;
    }

    public String getApproveUser() {
        return approveUser;
    }

    public WindConfig setApproveUser(String approveUser) {
        this.approveUser = approveUser;
        return this;
    }

    public Map<String,Object> nodeConfig(){
        return JsonHelper.jsonToMap(this.getNodeConfig());
    }

    public Map<String,Object> businessConfig(){
        return JsonHelper.jsonToMap(this.getBusinessConfig());
    }

    @SuppressWarnings("unchecked")
    public List<WindUser>  approveUser(){
        return JsonHelper.parseJson(this.approveUser,List.class,WindUser.class);
    }

}
