package com.bcx.wind.workflow.access;

/**
 * 查询任务实例过滤器
 *
 * @author zhanglei
 */
public class TaskFilter {

    /**
     * 流程定义ID
     */
    private String processId;

    /**
     * 流程定义名称
     */
    private String processName;

    /**
     * 任务审批人
     */
    private String actor;

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 任务级别
     */
    private String taskLevel;

    /**
     * 流程实例ID
     */
    private String orderId;


    /**
     * 任务创建时间排序
     */
    private boolean orderBy;




    public String getProcessId() {
        return processId;
    }

    public TaskFilter setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public String getProcessName() {
        return processName;
    }

    public TaskFilter setProcessName(String processName) {
        this.processName = processName;
        return this;
    }

    public String getActor() {
        return actor;
    }

    public TaskFilter setActor(String actor) {
        this.actor = actor;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public TaskFilter setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public TaskFilter setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public boolean isOrderBy() {
        return orderBy;
    }

    public TaskFilter setOrderBy(boolean orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public String getTaskLevel() {
        return taskLevel;
    }

    public TaskFilter setTaskLevel(String taskLevel) {
        this.taskLevel = taskLevel;
        return this;
    }
}
