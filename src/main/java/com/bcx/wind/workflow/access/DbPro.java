package com.bcx.wind.workflow.access;

/**
 * db数据库名称
 *
 * @author zhanglei
 */
public final class DbPro {

    public static final String POSTGRESQL = "postgres";

    public static final String H2 = "h2";

    public static final String ORACLE = "oracle";

    public static final String DB2 = "db2";

    public static final String MYSQL = "mysql";

    public static final String SQLSERVER = "sqlserver";

    public static final String MARIADB = "maria";
}
