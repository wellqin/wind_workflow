package com.bcx.wind.workflow.access.paging;

import com.bcx.wind.workflow.access.WindPage;

/**
 * 分页插件
 *
 * @author zhanglei
 */
public interface Paging {


    /**
     * 通过指定sql语句 和 分页参数 拼接sql语句
     *
     * @param sql   sql语句
     * @param page  分页信息
     *
     * @return  拼接后的sql语句
     */
    StringBuilder  buildSql(StringBuilder sql, WindPage page);


    /**
     * 通过指定sql语句 和 分页参数 拼接sql语句
     *
     * @param sql        sql语句
     * @param page       分页参数
     * @return            拼接后的sql语句
     */
    String  buildSql(String sql,WindPage page);


    /**
     * 从工作流分页参数中获取pageSize
     *
     * @param windPage   分页参数
     * @return            页面长度
     */
    default int pageSize(WindPage windPage){
        return windPage.getPageSize();
    }


    /**
     * 从工作流分页参数中获取pageNum
     *
     * @param windPage  分页参数
     * @return           页面号
     */
    default int  pageNum(WindPage windPage){
        return windPage.getPageNum();
    }
}
