package com.bcx.wind.workflow.access.paging;

import com.bcx.wind.workflow.access.WindPage;

/**
 * SqlServer 分页
 *
 * @author zhanglei
 */
public class SqlServerPaging implements Paging{

    public static final String ORDER_BY = "order by";

    @Override
    public StringBuilder buildSql(StringBuilder sql, WindPage page) {
        if(!sql.toString().contains(ORDER_BY)){
            sql.append(" order by id ");
        }
        return sql.append(" OFFSET ")
                .append(page.getPageSize()*(page.getPageNum()-1))
                .append(" ROW FETCH NEXT ")
                .append(page.getPageSize())
                .append(" ROWS only ");
    }

    @Override
    public String buildSql(String sql, WindPage page) {
        StringBuilder builder = new StringBuilder(sql);
        return buildSql(builder,page).toString();
    }
}
