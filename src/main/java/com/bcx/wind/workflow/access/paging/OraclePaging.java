package com.bcx.wind.workflow.access.paging;

import com.bcx.wind.workflow.access.WindPage;

/**
 * oracle数据库分页
 *
 * @author zhanglei
 */
public class OraclePaging implements Paging {

    @Override
    public StringBuilder buildSql(StringBuilder sql, WindPage page) {
        sql.append("select * from ( select row_.*, rownum rownum_ from ( ");
        sql.append(sql);
        long start = (long)((page.getPageNum() - 1) * page.getPageSize() + 1);
        sql.append(" ) row_ where rownum < ");
        sql.append(start + (long)page.getPageSize());
        sql.append(" ) where rownum_ >= ");
        sql.append(start);
        return sql;
    }

    @Override
    public String buildSql(String sql, WindPage page) {
        StringBuilder builder = new StringBuilder(sql);
        builder = buildSql(builder,page);
        return builder.toString();
    }
}
