package com.bcx.wind.workflow.access.paging;

import com.bcx.wind.workflow.access.WindPage;

/**
 * Maria 数据库分页
 *
 * @author zhanglei
 */
public class MariaPaging implements Paging {

    @Override
    public StringBuilder buildSql(StringBuilder sql, WindPage page) {
        return sql.append(" limit ")
                .append((pageNum(page)-1) * pageSize(page))
                .append(" , ")
                .append(pageSize(page));
    }

    @Override
    public String buildSql(String sql, WindPage page) {
        StringBuilder builder = new StringBuilder(sql);
        builder = buildSql(builder,page);
        return builder.toString();
    }
}
