package com.bcx.wind.workflow.access;


import com.bcx.wind.workflow.db.util.handler.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 返回结果为数字类型的拦截器
 *
 * @author zhanglei
 */
public class WindResultObjectHandler implements ResultSetHandler {


    @Override
    public Object handle(ResultSet resultSet) throws SQLException {
        Object result = null;
        while(resultSet.next()){
            Object ret = resultSet.getObject(1);
            if(ret != null) {
                result =  resultSet.getObject(1);
            }
            break;
        }
        return result;
    }
}
