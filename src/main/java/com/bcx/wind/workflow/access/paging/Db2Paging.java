package com.bcx.wind.workflow.access.paging;

import com.bcx.wind.workflow.access.WindPage;

/**
 * DB2数据库分页
 *
 * @author zhanglei
 */
public class Db2Paging implements Paging {

    @Override
    public StringBuilder buildSql(StringBuilder sql, WindPage page) {
        sql.append("SELECT * FROM  ( SELECT B.*, ROWNUMBER() OVER() AS RN FROM ( ");
        sql.append(sql);
        long start = (long)((page.getPageNum() - 1) * page.getPageSize() + 1);
        sql.append(" ) AS B )AS A WHERE A.RN BETWEEN ");
        sql.append(start);
        sql.append(" AND ");
        sql.append(start + (long)page.getPageSize());
        return sql;
    }

    @Override
    public String buildSql(String sql, WindPage page) {
        StringBuilder builder = new StringBuilder(sql);
        builder = buildSql(builder,page);
        return builder.toString();
    }
}
