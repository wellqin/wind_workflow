package com.bcx.wind.workflow.cache.imp;

import com.bcx.wind.workflow.cache.Cache;

import java.util.LinkedList;

/**
 * 固定缓存大小，超过则删除最先设置的缓存值
 *
 * @author zhanglei
 */
public class DelFirstCache implements Cache {

    /**
     * 实际缓存
     */
    private Cache cache;

    /**
     * 保存缓存的保存顺序
     */
    private LinkedList<Object> listCache;

    /**
     * 缓存最大长度
     */
    private final int maxSize;


    public DelFirstCache(Cache cache){
        this.cache = cache;
        this.listCache = new LinkedList<>();
        this.maxSize = 1024;
    }

    public DelFirstCache(Cache cache, int maxSize){
        this.cache = cache;
        this.listCache = new LinkedList<>();
        this.maxSize = maxSize;
    }

    private void changeCache(){
        if(this.listCache.size() > this.maxSize){
            Object key = this.listCache.removeFirst();
            this.cache.removeKey(key);
        }
    }

    @Override
    public Object put(Object key, Object value) {
        this.listCache.addLast(key);
        changeCache();
        return this.cache.put(key,value);
    }

    @Override
    public Object get(Object key) {
        return this.cache.get(key);
    }

    @Override
    public boolean contain(Object key) {
        return this.cache.contain(key);
    }

    @Override
    public int size() {
        return this.cache.size();
    }

    @Override
    public Object removeKey(Object key) {
        return this.cache.removeKey(key);
    }

    @Override
    public void clear() {
        this.cache.clear();
    }
}
