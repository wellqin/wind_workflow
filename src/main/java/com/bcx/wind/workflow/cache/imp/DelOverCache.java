package com.bcx.wind.workflow.cache.imp;

import com.bcx.wind.workflow.cache.Cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 指定缓存大小，超过限制，删除最不常用的值
 *
 * @author zhanglei
 */
public class DelOverCache implements Cache {

    /**
     * 实际缓存
     */
    private Cache cache;

    /**
     * 缓存大小
     */
    private final int maxSize;


    /**
     * 暂存缓存
     */
    private Map<Object,Object> cacheList;

    /**
     * 最不常用的键
     */
    private Object eldestKey;


    public DelOverCache(Cache cache){
        this.maxSize = 1024;
        this.cache = cache;
        setSize(this.maxSize);
    }

    public DelOverCache(Cache cache , int maxSize){
        this.cache = cache;
        this.maxSize = maxSize;
        setSize(this.maxSize);
    }


    private void setSize(int size){
        this.cacheList = new LinkedHashMap<Object, Object>(size,0.75F,true){

            /**
             * 重写removeEldestEntry 方法，将最不常用的键 去除来，然后将实际缓存中的值删除
             * @param eldest    不常用的数据
             * @return          是否超出容器大小
             */
            @Override
            protected boolean removeEldestEntry(Map.Entry<Object,Object> eldest){
                boolean ret = size() > maxSize;
                if(ret){
                    eldestKey = eldest.getKey();
                }
                return ret;
            }
        };
    }


    @Override
    public Object put(Object key, Object value) {
        this.cacheList.put(key,value);
        if(this.eldestKey != null) {
            this.cache.removeKey(eldestKey);
            //置空，等待下次设置
            this.eldestKey = null;
        }
        return this.cache.put(key,value);
    }

    @Override
    public Object get(Object key) {
        //模拟使用
        this.cacheList.get(key);
        return this.cache.get(key);
    }

    @Override
    public boolean contain(Object key) {
        //模拟使用
        this.cacheList.containsKey(key);
        return this.cache.contain(key);
    }

    @Override
    public int size() {
        this.cacheList.size();
        return this.cache.size();
    }

    @Override
    public Object removeKey(Object key) {
        this.cacheList.remove(key);
        return this.cache.removeKey(key);
    }

    @Override
    public void clear() {
        this.cacheList.clear();
    }
}
