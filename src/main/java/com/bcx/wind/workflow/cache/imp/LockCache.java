package com.bcx.wind.workflow.cache.imp;

import com.bcx.wind.workflow.cache.Cache;

/**
 * 同步阻塞缓存实现
 *
 * @author zhanglei
 */
public class LockCache implements Cache {

    /**
     * 实际缓存数据
     */
    private Cache cache;

    public LockCache(Cache cache){
        this.cache = cache;
    }

    @Override
    public synchronized Object put(Object key, Object value) {
        return this.cache.put(key,value);
    }

    @Override
    public synchronized Object get(Object key) {
        return this.cache.get(key);
    }

    @Override
    public synchronized boolean contain(Object key) {
        return this.cache.contain(key);
    }

    @Override
    public synchronized int size() {
        return this.cache.size();
    }

    @Override
    public synchronized Object removeKey(Object key) {
        return this.cache.removeKey(key);
    }

    @Override
    public void clear() {
        this.cache.clear();
    }
}
