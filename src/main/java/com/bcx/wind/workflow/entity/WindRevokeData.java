package com.bcx.wind.workflow.entity;

import java.sql.Timestamp;

/**
 * 撤销操作，临时暂存信息实体
 *
 * @author zhanglei
 */
public class WindRevokeData {

    /**
     * 主键
     */
    private String id;

    /**
     * 流程实例ID
     */
    private String orderId;

    /**
     * 流程定义ID
     */
    private String processId;

    /**
     * 流程定义显示名称
     */
    private String processDisplayName;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务显示名称
     */
    private String taskDisplayName;

    /**
     * 任务操作人
     */
    private String actorId;

    /**
     * 操作
     */
    private String operate;

    /**
     * 建议
     */
    private String suggest;

    /**
     * 操作时间
     */
    private Timestamp operateTime;

    /**
     * 操作时，相关流程信息数据
     */
    private String windOrder;

    /**
     * 操作时，相关任务信息数据
     */
    private String windTask;

    /**
     * 操作时，相关任务审批人数据
     */
    private String windTaskActor;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    public String getId() {
        return id;
    }

    public WindRevokeData setId(String id) {
        this.id = id;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public WindRevokeData setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getProcessId() {
        return processId;
    }

    public WindRevokeData setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public String getProcessDisplayName() {
        return processDisplayName;
    }

    public WindRevokeData setProcessDisplayName(String processDisplayName) {
        this.processDisplayName = processDisplayName;
        return this;
    }

    public String getTaskName() {
        return taskName;
    }

    public WindRevokeData setTaskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    public String getTaskDisplayName() {
        return taskDisplayName;
    }

    public WindRevokeData setTaskDisplayName(String taskDisplayName) {
        this.taskDisplayName = taskDisplayName;
        return this;
    }

    public String getActorId() {
        return actorId;
    }

    public WindRevokeData setActorId(String actorId) {
        this.actorId = actorId;
        return this;
    }

    public String getOperate() {
        return operate;
    }

    public WindRevokeData setOperate(String operate) {
        this.operate = operate;
        return this;
    }

    public String getSuggest() {
        return suggest;
    }

    public WindRevokeData setSuggest(String suggest) {
        this.suggest = suggest;
        return this;
    }

    public Timestamp getOperateTime() {
        return operateTime;
    }

    public WindRevokeData setOperateTime(Timestamp operateTime) {
        this.operateTime = operateTime;
        return this;
    }

    public String getWindOrder() {
        return windOrder;
    }

    public WindRevokeData setWindOrder(String windOrder) {
        this.windOrder = windOrder;
        return this;
    }

    public String getWindTask() {
        return windTask;
    }

    public WindRevokeData setWindTask(String windTask) {
        this.windTask = windTask;
        return this;
    }

    public String getWindTaskActor() {
        return windTaskActor;
    }

    public WindRevokeData setWindTaskActor(String windTaskActor) {
        this.windTaskActor = windTaskActor;
        return this;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public WindRevokeData setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
        return this;
    }
}
