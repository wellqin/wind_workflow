package com.bcx.wind.workflow.entity;

import com.bcx.wind.workflow.support.JsonHelper;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * 流程实例实体
 *
 * @author zhanglei
 */
public class WindOrder {

    /**
     * 流程实例ID
     */
    private String id;

    /**
     * 流程定义ID
     */
    private String processId;

    /**
     * 流程状态   暂停，运行
     */
    private String status;

    /**
     * 流程创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 期望完成时间
     */
    private Timestamp expireTime;

    /**
     * 父流程ID
     */
    private String parentId;

    /**
     * 版本
     */
    private int version;

    /**
     * 流程变量
     */
    private String variable;

    /**
     * 流程信息消息
     */
    private String data;

    /**
     * 所属模块
     */
    private String system;

    /**
     * 流程结束时间
     */
    private Timestamp finishTime;

    /**
     * 业务ID
     */
    private String businessId;


    /**
     * 流程实例类型
     */
    private String type;


    public Object  gotVariable(String key){
        if(this.variable != null){
            Map<String,Object> args = JsonHelper.jsonToMap(this.variable);
            return args.get(key);
        }
        return null;
    }

    public Map<String,Object> variable(){
        if(this.variable != null){
            return JsonHelper.jsonToMap(this.variable);
        }
        return new HashMap<>(4);
    }

    public WindOrder addVariable(String key,Object value){
        Map<String,Object> args = variable();
        args.put(key,value);
        this.setVariable(JsonHelper.toJson(args));
        return this;
    }

    public WindOrder addVariable(Map<String,Object> args){
        Map<String,Object> variable = variable();
        variable.putAll(args);
        this.setVariable(JsonHelper.toJson(args));
        return this;
    }

    public String getId() {
        return id;
    }

    public WindOrder setId(String id) {
        this.id = id;
        return this;
    }

    public String getProcessId() {
        return processId;
    }

    public WindOrder setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public WindOrder setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getCreateUser() {
        return createUser;
    }

    public WindOrder setCreateUser(String createUser) {
        this.createUser = createUser;
        return this;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public WindOrder setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
        return this;
    }

    public Timestamp getExpireTime() {
        return expireTime;
    }

    public WindOrder setExpireTime(Timestamp expireTime) {
        this.expireTime = expireTime;
        return this;
    }

    public String getParentId() {
        return parentId;
    }

    public WindOrder setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    public int getVersion() {
        return version;
    }

    public WindOrder setVersion(int version) {
        this.version = version;
        return this;
    }

    public String getVariable() {
        return variable;
    }

    public WindOrder setVariable(String variable) {
        this.variable = variable;
        return this;
    }

    public String getData() {
        return data;
    }

    public WindOrder setData(String data) {
        this.data = data;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public WindOrder setSystem(String system) {
        this.system = system;
        return this;
    }

    public Timestamp getFinishTime() {
        return finishTime;
    }

    public WindOrder setFinishTime(Timestamp finishTime) {
        this.finishTime = finishTime;
        return this;
    }

    public String getBusinessId() {
        return businessId;
    }

    public WindOrder setBusinessId(String businessId) {
        this.businessId = businessId;
        return this;
    }

    public String getType() {
        return type;
    }

    public WindOrder setType(String type) {
        this.type = type;
        return this;
    }
}
