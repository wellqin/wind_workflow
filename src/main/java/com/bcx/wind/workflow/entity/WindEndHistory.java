package com.bcx.wind.workflow.entity;

import java.sql.Timestamp;

/**
 * 结束流程历史信息实体
 *
 * @author zhanglei
 */
public class WindEndHistory {

    /**
     * 主键
     */
    private String id;

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 任务名称  （节点名称）
     */
    private String taskName;

    /**
     * 任务显示名称  （节点显示名称）
     */
    private String taskDisplayName;

    /**
     * 流程定义ID
     */
    private String processId;

    /**
     * 流程实例ID
     */
    private String orderId;

    /**
     * 流程定义名称
     */
    private String processName;

    /**
     * 流程定义显示名称
     */
    private String processDisplayName;

    /**
     * 操作
     */
    private String operate;

    /**
     * 意见
     */
    private String suggest;

    /**
     * 审批时间
     */
    private Timestamp approveTime;

    /**
     * 操作人ID
     */
    private String actorId;

    /**
     * 操作人名称
     */
    private String actorName;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 审批人数据集合
     */
    private String approveUserVariable;

    /**
     * 任务类型
     */
    private String taskType;

    /**
     * 所属模块
     */
    private String system;

    /**
     * 操作人信息
     */
    private String submitUserVariable;

    /**
     * 附加信息
     */
    private String variable;

    /**
     * 任务创建人
     */
    private String taskCreateUser;

    /**
     * 任务等级
     */
    private String taskLevel;

    /**
     * 父任务ID
     */
    private String taskParentId;

    public String getId() {
        return id;
    }

    public WindEndHistory setId(String id) {
        this.id = id;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public WindEndHistory setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getTaskName() {
        return taskName;
    }

    public WindEndHistory setTaskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    public String getTaskDisplayName() {
        return taskDisplayName;
    }

    public WindEndHistory setTaskDisplayName(String taskDisplayName) {
        this.taskDisplayName = taskDisplayName;
        return this;
    }

    public String getProcessId() {
        return processId;
    }

    public WindEndHistory setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public WindEndHistory setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getProcessName() {
        return processName;
    }

    public WindEndHistory setProcessName(String processName) {
        this.processName = processName;
        return this;
    }

    public String getProcessDisplayName() {
        return processDisplayName;
    }

    public WindEndHistory setProcessDisplayName(String processDisplayName) {
        this.processDisplayName = processDisplayName;
        return this;
    }

    public String getOperate() {
        return operate;
    }

    public WindEndHistory setOperate(String operate) {
        this.operate = operate;
        return this;
    }

    public String getSuggest() {
        return suggest;
    }

    public WindEndHistory setSuggest(String suggest) {
        this.suggest = suggest;
        return this;
    }

    public Timestamp getApproveTime() {
        return approveTime;
    }

    public WindEndHistory setApproveTime(Timestamp approveTime) {
        this.approveTime = approveTime;
        return this;
    }

    public String getActorId() {
        return actorId;
    }

    public WindEndHistory setActorId(String actorId) {
        this.actorId = actorId;
        return this;
    }

    public String getActorName() {
        return actorName;
    }

    public WindEndHistory setActorName(String actorName) {
        this.actorName = actorName;
        return this;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public WindEndHistory setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getApproveUserVariable() {
        return approveUserVariable;
    }

    public WindEndHistory setApproveUserVariable(String approveUserVariable) {
        this.approveUserVariable = approveUserVariable;
        return this;
    }

    public String getTaskType() {
        return taskType;
    }

    public WindEndHistory setTaskType(String taskType) {
        this.taskType = taskType;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public WindEndHistory setSystem(String system) {
        this.system = system;
        return this;
    }

    public String getSubmitUserVariable() {
        return submitUserVariable;
    }

    public WindEndHistory setSubmitUserVariable(String submitUserVariable) {
        this.submitUserVariable = submitUserVariable;
        return this;
    }

    public String getVariable() {
        return variable;
    }

    public WindEndHistory setVariable(String variable) {
        this.variable = variable;
        return this;
    }

    public String getTaskCreateUser() {
        return taskCreateUser;
    }

    public WindEndHistory setTaskCreateUser(String taskCreateUser) {
        this.taskCreateUser = taskCreateUser;
        return this;
    }

    public String getTaskLevel() {
        return taskLevel;
    }

    public WindEndHistory setTaskLevel(String taskLevel) {
        this.taskLevel = taskLevel;
        return this;
    }

    public String getTaskParentId() {
        return taskParentId;
    }

    public WindEndHistory setTaskParentId(String taskParentId) {
        this.taskParentId = taskParentId;
        return this;
    }
}
