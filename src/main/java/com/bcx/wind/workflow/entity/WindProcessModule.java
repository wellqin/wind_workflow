package com.bcx.wind.workflow.entity;

/**
 * 流程定义模块实体
 *
 * @author zhanglei
 */
public class WindProcessModule {

    /**
     * 主键
     */
    private String id;

    /**
     * 模块名称
     */
    private String moduleName;

    /**
     * 父模块主键
     */
    private String parentId;

    /**
     * 所属系统  附加字段，按需使用
     */
    private String system;

    public String getId() {
        return id;
    }

    public WindProcessModule setId(String id) {
        this.id = id;
        return this;
    }

    public String getModuleName() {
        return moduleName;
    }

    public WindProcessModule setModuleName(String moduleName) {
        this.moduleName = moduleName;
        return this;
    }

    public String getParentId() {
        return parentId;
    }

    public WindProcessModule setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    public String getSystem() {
        return system;
    }

    public WindProcessModule setSystem(String system) {
        this.system = system;
        return this;
    }
}
