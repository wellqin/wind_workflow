package com.bcx.wind.workflow.entity;

import com.bcx.wind.workflow.core.constant.ConditionType;
import com.bcx.wind.workflow.core.constant.Logic;
import com.bcx.wind.workflow.pojo.Condition;
import com.bcx.wind.workflow.support.JsonHelper;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * 流程定义配置实体
 *
 * @author zhanglei
 */
public class WindProcessConfig {

    /**
     * 流程定义配置ID
     */
    private String id;

    /**
     * 流程定义ID
     */
    private String processId;

    /**
     * 配置名称
     */
    private String configName;

    /**
     * 流程定义名称
     */
    private String processName;

    /**
     * 节点名称
     */
    private String nodeId;

    /**
     * 匹配条件
     */
    private String condition;

    /**
     * 任务节点配置信息
     */
    private String nodeConfig;

    /**
     * 节点预选审批人信息
     */
    private String approveUser;

    /**
     * 业务配置信息      {"docType":"submit"}
     */
    private String businessConfig;

    /**
     * 排序，优先级
     */
    private int sort;

    /**
     * 配置登记  1流程配置  2任务节点配置
     */
    private int level;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 匹配逻辑符
     */
    private String logic;

    /**
     * 匹配脚本
     */
    private String script;

    public WindProcessConfig  addCondition(Condition condition) {
        if(condition == null){
            return this;
        }
        List<Condition> conditions = this.conditions();
        for (Condition con : conditions) {
            String key = condition.getKey();
            ConditionType type = condition.getCondition();
            Object value = condition.getValue();
            if (key == null || type == null || value == null || key.equals(con.getKey())) {
                return this;
            }
        }
        conditions.add(condition);
        this.setCondition(JsonHelper.toJson(conditions));
        return this;
    }

    public WindProcessConfig  addNodeConfig(String key,Object value){
        if(key == null || value == null){
            return this;
        }
        Map<String,Object> nodeConfigMap = this.nodeConfig();
        nodeConfigMap.put(key,value);
        this.setNodeConfig(JsonHelper.toJson(nodeConfigMap));
        return this;
    }

    public WindProcessConfig addBusinessConfig(String key,Object value){
        if(key == null || value == null){
            return this;
        }
        Map<String,Object> businessConfigMap = this.businessConfig();
        businessConfigMap.put(key,value);
        this.setBusinessConfig(JsonHelper.toJson(businessConfigMap));
        return this;
    }

    public String getId() {
        return id;
    }

    public WindProcessConfig setId(String id) {
        this.id = id;
        return this;
    }

    public String getProcessId() {
        return processId;
    }

    public WindProcessConfig setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public String getConfigName() {
        return configName;
    }

    public WindProcessConfig setConfigName(String configName) {
        this.configName = configName;
        return this;
    }

    public String getProcessName() {
        return processName;
    }

    public WindProcessConfig setProcessName(String processName) {
        this.processName = processName;
        return this;
    }

    public String getNodeId() {
        return nodeId;
    }

    public WindProcessConfig setNodeId(String nodeId) {
        this.nodeId = nodeId;
        return this;
    }

    public String getCondition() {
        return condition;
    }

    public WindProcessConfig setCondition(String condition) {
        this.condition = condition;
        return this;
    }

    public String getNodeConfig() {
        return nodeConfig;
    }

    public WindProcessConfig setNodeConfig(String nodeConfig) {
        this.nodeConfig = nodeConfig;
        return this;
    }

    public String getApproveUser() {
        return approveUser;
    }

    public WindProcessConfig setApproveUser(String approveUser) {
        this.approveUser = approveUser;
        return this;
    }

    public String getBusinessConfig() {
        return businessConfig;
    }

    public WindProcessConfig setBusinessConfig(String businessConfig) {
        this.businessConfig = businessConfig;
        return this;
    }

    public int getSort() {
        return sort;
    }

    public WindProcessConfig setSort(int sort) {
        this.sort = sort;
        return this;
    }

    public int getLevel() {
        return level;
    }

    public WindProcessConfig setLevel(int level) {
        this.level = level;
        return this;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public WindProcessConfig setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getLogic() {
        return logic;
    }

    public WindProcessConfig setLogic(String logic) {
        this.logic = logic;
        return this;
    }

    public String getScript() {
        return script;
    }

    public WindProcessConfig setScript(String script) {
        this.script = script;
        return this;
    }

    @SuppressWarnings("unchecked")
    public List<Condition> conditions(){
        return JsonHelper.parseJson(this.getCondition(),List.class,Condition.class);
    }

    public Map<String,Object> nodeConfig(){
        return JsonHelper.jsonToMap(this.getNodeConfig());
    }

    public Map<String,Object> businessConfig(){
        return JsonHelper.jsonToMap(this.getBusinessConfig());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof WindProcessConfig){
            WindProcessConfig config = (WindProcessConfig) obj;
            if(this.id.equals(config.getId())){
                return true;
            }
        }
        return super.equals(obj);
    }
}
